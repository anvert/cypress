module.exports = {

    /**
     * Magyar nyelvre vált.
     * @param cy
     */
    hungarian(cy) {
        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.get('a.dropdown-item.dusk-profile').click({force: true});
        cy.wait(5000);
        cy.get('div.content-body span.primary.pointer').eq(1).click();
        cy.wait(2000);
        cy.get('div.vs__actions svg path').eq(3).click({force: true});
        cy.get('#vs2__option-1').click({force: true});
        cy.get('div.card-footer button.btn-normal-primary-with-shadow').eq(1).click({force: true});
        cy.wait('@update').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.get('li.nav-item.mobile-menu.m-auto.pl-0').click();
        cy.wait(6000);
        cy.wait('@post', {timeout: 10000}).should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        });
        cy.url().should('include', '');
        cy.get('h1.content-header-title').contains('Áttekintés').should('exist');
    },

    /**
     * Angol nyelvre vált.
     * @param cy
     */
    english(cy) {
        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.get('a.dropdown-item.dusk-profile').click({force: true});
        cy.wait(5000);
        cy.get('div.content-body span.primary.pointer').eq(1).click();
        cy.wait(2000);
        cy.get('div.vs__actions svg path').eq(3).click({force: true});
        cy.get('#vs2__option-0').click({force: true});
        cy.get('div.card-footer button.btn-normal-primary-with-shadow').eq(1).click({force: true});
        cy.wait('@update').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.get('li.nav-item.mobile-menu.m-auto.pl-0').click();
        cy.wait(6000);
        cy.wait('@post', {timeout: 10000}).should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        });
        cy.url().should('include', '');
        cy.get('h1.content-header-title').contains('Dashboard').should('exist');
    },

    /**
     * Fiók adatok tesztelése
     * @param cy
     * @param name
     * @param vat
     * @param comp
     * @param zip
     * @param city
     * @param address
     * @param country
     */
    profile(cy, name, vat, comp, zip, city, address, country) {
        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.wait(3000);
        cy.get('a.dropdown-item.dusk-profile').click();
        cy.wait(3000);
        cy.get('div.content-body span.primary.pointer').eq(0).click();
        cy.wait(3000);
        cy.get('#name').clear().type(name);
        cy.get('#vat_number').clear().type(vat);
        cy.get('div.card-footer button.btn-normal-primary-with-shadow').eq(0).click();
        cy.wait('@updateProf').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.get('li.nav-item.mobile-menu.m-auto.pl-0').click();
        cy.wait(3000);
        cy.get('.subhead-3.mt-auto.mb-auto.p-b-4').contains(name).should('exist');
        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.wait(3000);
        cy.get('a.dropdown-item.dusk-profile').click();
        cy.wait(3000);
        cy.get('div.content-body span.primary.pointer').eq(0).click();
        cy.wait(3000);
        cy.get('[type="radio"]').eq(1).click({force: true});
        cy.wait(3000);
        cy.get('div.card-footer button.btn-normal-primary-with-shadow').eq(0).click();
        cy.wait('@updateProf').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200);
        })
        cy.get('#company_name').clear().type(comp);
        cy.get('div.card-footer button.btn-normal-primary-with-shadow').eq(0).click();
        cy.wait('@updateProf').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200);
        })
        cy.get('li.nav-item.mobile-menu.m-auto.pl-0').click();
        cy.wait(3000);
        cy.get('div.multiselect__select').eq(0).click({force: true});
        cy.get('ul.multiselect__content li.multiselect__element span span').eq(0).contains(comp).should('exist');
        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.wait(3000);
        cy.get('a.dropdown-item.dusk-profile').click();
        cy.wait(3000);
        cy.get('div.content-body span.primary.pointer').eq(0).click();
        cy.wait(3000);
        cy.get('div.vs__actions svg path').eq(1).click({force: true});
        cy.wait(3000);
        cy.get('input.vs__search').eq(0).type(country);
        cy.get('ul#vs1__listbox li#vs1__option-0').click({force: true});
        cy.get('#company_zip').clear().type(zip);
        cy.get('div.card-footer button.btn-normal-primary-with-shadow').eq(0).click();
        cy.wait('@updateProf').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200);
        })
        cy.get('#company_city').clear().type(city);
        cy.get('div.card-footer button.btn-normal-primary-with-shadow').eq(0).click();
        cy.wait('@updateProf').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200);
        })
        cy.get('#company_address').clear().type(address);
        cy.get('div.card-footer button.btn-normal-primary-with-shadow').eq(0).click();
        cy.wait('@updateProf').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200);
        })
    },

    /**
     * Profil beállítások módosítása.
     * @param cy
     * @param num1
     * @param num2
     * @param num3
     * @param num4
     * @param num5
     */
    settings(cy, num1, num2, num3, num4, num5) {
        // TODO: a gombokból a data azonosítás helyett mást kell pl.: .btn-normal-primary-with-shadow -> eq(1)
        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.get('a.dropdown-item.dusk-profile').click();
        cy.wait(2000);
        cy.get('div.content-body span.primary.pointer').eq(1).click();

        cy.get('.control_indicator-check').each(el => {
            el.click();
            cy.wait(4000);
            cy.get('[data-v-677be0eb=""][style=""] > .card-footer > .row > .col-12 > .btn-normal-primary-with-shadow').click();
            cy.wait('@update').should((xhr) => {
                expect(xhr.status, 'successful POST').to.equal(200)
            })
        });

        cy.get('.control_indicator-check').each(el => {
            el.click();
        });
        cy.get('[data-v-677be0eb=""][style=""] > .card-footer > .row > .col-12 > .btn-normal-primary-with-shadow').click();
        cy.wait('@update').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(2000);

        for (let i = 0; i <= 1; i++) {
            cy.get('.control_indicator-check').eq(i).click();
            cy.wait(2000);
        }

        cy.get(':nth-child(1) > .wizard-card-row > .unit > .form-control').scrollIntoView().clear().type(num1);
        cy.get('[data-v-677be0eb=""][style=""] > .card-footer > .row > .col-12 > .btn-normal-primary-with-shadow').click({force:true});
        cy.wait('@update').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(2000);

        cy.get(':nth-child(2) > .wizard-card-row > .unit > .form-control').scrollIntoView().clear().type(num2);
        cy.get('[data-v-677be0eb=""][style=""] > .card-footer > .row > .col-12 > .btn-normal-primary-with-shadow').click({force:true});
        cy.wait('@update').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(2000);

        cy.get(':nth-child(3) > .wizard-card-row > .unit > .form-control').scrollIntoView().clear().type(num3);
        cy.get('[data-v-677be0eb=""][style=""] > .card-footer > .row > .col-12 > .btn-normal-primary-with-shadow').click({force:true});
        cy.wait('@update').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(2000);

        cy.get(':nth-child(6) > .wizard-card-row > .unit > .form-control').scrollIntoView().clear().type(num4);
        cy.get('[data-v-677be0eb=""][style=""] > .card-footer > .row > .col-12 > .btn-normal-primary-with-shadow').click({force:true});
        cy.wait('@update').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(2000);

        cy.get(':nth-child(5) > .unit > .form-control').scrollIntoView().clear().type(num5);
        cy.get('[data-v-677be0eb=""][style=""] > .card-footer > .row > .col-12 > .btn-normal-primary-with-shadow').click({force:true});
        cy.wait('@update').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(2000);

        for (let i = 0; i <= 1; i++) {
            cy.get('.control_indicator-check').eq(i).click();
            cy.wait(2000);
        }
    },

    /**
     * Bejelentkezés
     * @param cy
     * @param psw
     * @param email
     * @param url
     */
    login2(cy, psw, email, url) {
        cy.visit(url);
        cy.url().should('include', '/login');
        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span').click();
        cy.get('a.dropdown-item.dusk-profile').next().click();
        cy.get('#email').clear();
        cy.get('#email').type(email);
        cy.get('#password').clear();
        cy.get('#password').type(psw);
        cy.get('form').eq(0).submit();
        cy.wait(4000);
        cy.wait('@post').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
    },

    /**
     * Egyszerű bejelentkezés bármi post bevárása nélkül
     * @param cy
     * @param psw
     * @param email
     */
    login(cy, psw, email) {
        cy.get('#email').clear();
        cy.get('#email').type(email);
        cy.get('#password').clear();
        cy.get('#password').type(psw);
        cy.get('form').eq(0).submit();
    },

    /**
     * Új jelszó megadása
     * @param cy
     * @param old
     * @param fresh
     */
    password(cy, old, fresh) {
        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.wait(3000);
        cy.get('a.dropdown-item.dusk-profile').click();
        cy.wait(3000);
        cy.get('div.content-body span.primary.pointer').eq(2).click();
        cy.wait(3000);
        cy.get('#current_password').clear().type(old);
        cy.get('#new_password').clear().type(fresh);
        cy.get('#retype_password').clear().type(fresh);
        cy.get('div.card-footer button.btn-normal-primary-with-shadow').eq(2).click({force: true});
        cy.wait(3000);
        cy.wait('@updatePass').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.get('li.nav-item.mobile-menu.m-auto.pl-0').click();
        cy.wait(3000);
        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.wait(3000);
        cy.get('a.dropdown-item').eq(9).click();
        cy.wait('@post').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
    },

    /**
     * Paraméterben átadott nevű szövegek létezésének ellenőrzése
     */
    dash2(cy, text, text2, text3, text4, text5, text6, text7, text8, text9, text10, text11, text12, text13, text14, text15, text16, text17, text18, text19, text20, text21) {
        cy.get('#opinionIndex div span svg g g g g text').contains(text21).should('exist');
        cy.get('#allTrafficChart div span svg g g g g text').contains(text).should('exist');
        cy.get('#sentEmails div span svg g g g g text').contains(text).should('exist');
        cy.get('#messengerTraffic div span svg g g g g text').contains(text17).should('exist');
        cy.get('#replyTimeChart div span svg g g g g text').contains(text2).should('exist');
        cy.get('#messengerReplyTimeChart div svg g g g g text').contains(text2).should('exist');
        cy.get('#all-email-traffic div span svg g g g g text').first().contains(text3).should('exist');
        cy.get('#all-email-traffic div span svg g g g g text').eq(1).contains(text4).should('exist');
        cy.get('#all-email-traffic div span svg g g g g text').eq(2).contains(text5).should('exist');
        cy.get('div.card-content span.main-number').nextAll().should('not.have.value', '~');
        cy.get('div.card-content.p-1.pt-0 table.top-list.body-2').nextAll().should('not.eq', '');
        cy.get('ul#main-menu-navigation li a').eq(1).contains(text6).should('exist');
        cy.get('ul#main-menu-navigation li a').eq(2).contains(text7).should('exist');
        cy.get('ul#main-menu-navigation li a').eq(3).contains(text8).should('exist');
        cy.get('ul#main-menu-navigation li a').eq(4).contains(text18).should('exist');
        cy.get('ul#main-menu-navigation li ul li a').eq(0).contains(text19).should('exist');
        cy.get('ul#main-menu-navigation li ul li a').eq(1).contains(text20).should('exist');

        cy.get('ul li a.menu-item div span.menu-item').eq(0).contains(text9).should('exist');
        cy.get('ul li a.menu-item div span.menu-item').eq(1).contains(text10).should('exist');
        cy.get('ul li a.menu-item div span.menu-item').eq(2).contains(text11).should('exist');
        cy.get('ul li a.menu-item div span.menu-item').eq(3).contains(text5).should('exist');
        cy.get('ul li a.menu-item div span.menu-item').eq(4).contains(text12).should('exist');
        cy.get('ul li a.menu-item div span.menu-item').eq(5).contains(text13).should('exist');
        cy.get('ul li a.menu-item div span.menu-item').eq(6).contains(text14).should('exist');
        cy.get('ul li a.menu-item div span.menu-item').eq(7).contains(text15).should('exist');
        cy.get('ul li a.menu-item div span.menu-item').eq(8).contains(text16).should('exist');
    },

    /**
     * Paraméterben átadott nevű szövegek létezésének ellenőrzése
     */
    dashWoMess(cy, text, text2, text3, text4, text5, text6, text7, text8, text9, text10, text11, text12, text13, text14, text15, text16, text17, text18, text19, text20) {
        cy.get('#opinionIndex div span svg g g g g text').contains(text17).should('exist');
        cy.get('#allTrafficChart div span svg g g g g text').contains(text).should('exist');
        cy.get('#sentEmails div span svg g g g g text').contains(text).should('exist');
        cy.get('#replyTimeChart div span svg g g g g text').contains(text2).should('exist');
        cy.get('#all-email-traffic div span svg g g g g text').first().contains(text3).should('exist');
        cy.get('#all-email-traffic div span svg g g g g text').eq(1).contains(text4).should('exist');
        cy.get('#all-email-traffic div span svg g g g g text').eq(2).contains(text5).should('exist');
        cy.get('div.card-content span.main-number').nextAll().should('not.have.value', '~');
        cy.get('div.card-content.p-1.pt-0 table.top-list.body-2').nextAll().should('not.eq', '');
        cy.get('ul#main-menu-navigation li a').eq(1).contains(text6).should('exist');
        cy.get('ul#main-menu-navigation li a').eq(2).contains(text7).should('exist');
        cy.get('ul#main-menu-navigation li a').eq(3).contains(text8).should('exist');
        cy.get('ul#main-menu-navigation li a').eq(4).contains(text18).should('exist');
        cy.get('ul#main-menu-navigation li ul li a').eq(0).contains(text19).should('exist');
        cy.get('ul#main-menu-navigation li ul li a').eq(1).contains(text20).should('exist');

        cy.get('ul li a.menu-item div span.menu-item').eq(0).contains(text9).should('exist');
        cy.get('ul li a.menu-item div span.menu-item').eq(1).contains(text10).should('exist');
        cy.get('ul li a.menu-item div span.menu-item').eq(2).contains(text11).should('exist');
        cy.get('ul li a.menu-item div span.menu-item').eq(3).contains(text5).should('exist');
        cy.get('ul li a.menu-item div span.menu-item').eq(4).contains(text12).should('exist');
        cy.get('ul li a.menu-item div span.menu-item').eq(5).contains(text13).should('exist');
        cy.get('ul li a.menu-item div span.menu-item').eq(6).contains(text14).should('exist');
        cy.get('ul li a.menu-item div span.menu-item').eq(7).contains(text15).should('exist');
        cy.get('ul li a.menu-item div span.menu-item').eq(8).contains(text16).should('exist');
    },

    /**
     * Paraméterben átadott nevű szövegek létezésének ellenőrzése
     */
    dashWoNe(cy, text, text2, text3, text4, text5, text6, text7, text8, text9, text10, text11, text12, text13, text14, text15, text16, text17) {
        cy.get('#opinionIndex div span svg g g g g text').contains(text17).should('exist');
        cy.get('#allTrafficChart div span svg g g g g text').contains(text).should('exist');
        cy.get('#sentEmails div span svg g g g g text').contains(text).should('exist');
        cy.get('#replyTimeChart div span svg g g g g text').contains(text2).should('exist');
        cy.get('#all-email-traffic div span svg g g g g text').first().contains(text3).should('exist');
        cy.get('#all-email-traffic div span svg g g g g text').eq(1).contains(text4).should('exist');
        cy.get('#all-email-traffic div span svg g g g g text').eq(2).contains(text5).should('exist');
        cy.get('div.card-content span.main-number').nextAll().should('not.have.value', '~');
        cy.get('div.card-content.p-1.pt-0 table.top-list.body-2').nextAll().should('not.eq', '');
        cy.get('ul#main-menu-navigation li a').eq(1).contains(text6).should('exist');
        cy.get('ul#main-menu-navigation li a').eq(2).contains(text7).should('exist');
        cy.get('ul#main-menu-navigation li a').eq(3).contains(text8).should('exist');

        cy.get('ul li a.menu-item div span.menu-item').eq(0).contains(text9).should('exist');
        cy.get('ul li a.menu-item div span.menu-item').eq(1).contains(text10).should('exist');
        cy.get('ul li a.menu-item div span.menu-item').eq(2).contains(text11).should('exist');
        cy.get('ul li a.menu-item div span.menu-item').eq(3).contains(text5).should('exist');
        cy.get('ul li a.menu-item div span.menu-item').eq(4).contains(text12).should('exist');
        cy.get('ul li a.menu-item div span.menu-item').eq(5).contains(text13).should('exist');
        cy.get('ul li a.menu-item div span.menu-item').eq(6).contains(text14).should('exist');
        cy.get('ul li a.menu-item div span.menu-item').eq(7).contains(text15).should('exist');
        cy.get('ul li a.menu-item div span.menu-item').eq(8).contains(text16).should('exist');
    },

    /**
     *
     * @param cy
     * @param date
     */
    dashFilter(cy, date) {
        cy.get('div.multiselect__select').eq(0).click({force: true});
        cy.get('ul.multiselect__content li.multiselect__element span span').eq(1).click({force: true});
        cy.get('div.text-right button.btn-normal-primary-with-shadow').eq(1).click({force: true});
        cy.wait('@post').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.get('div.card-content span.main-number').nextAll().should('not.have.value', '~');

        cy.get('div.multiselect__select').eq(0).click({force: true});
        cy.get('ul.multiselect__content li.multiselect__element span span').eq(1).click({force: true});
        cy.get('ul.multiselect__content li.multiselect__element span span').eq(2).click({force: true});
        cy.get('div.text-right button.btn-normal-primary-with-shadow').eq(1).click({force: true});
        cy.wait('@post').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.get('div.card-content span.main-number').nextAll().should('not.have.value', '~');

        cy.get('div.multiselect__select').eq(0).click({force: true});
        cy.get('ul.multiselect__content li.multiselect__element span span').eq(1).click({force: true});
        cy.get('div.text-right button.btn-normal-primary-with-shadow').eq(1).click({force: true});
        cy.wait('@post').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.get('div.card-content span.main-number').nextAll().should('not.have.value', '~');

        cy.get('div.multiselect__select').eq(0).click({force: true});
        cy.get('ul.multiselect__content li.multiselect__element span span').eq(1).click({force: true});
        cy.get('ul.multiselect__content li.multiselect__element span span').eq(2).click({force: true});
        cy.get('div.text-right button.btn-normal-primary-with-shadow').eq(1).click({force: true});
        cy.wait('@post').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.get('div.card-content span.main-number').nextAll().should('not.have.value', '~');

        cy.get('div.multiselect__select').eq(0).click({force: true});
        cy.get('ul.multiselect__content li.multiselect__element span span').eq(0).click({force: true});
        cy.get('div.text-right button.btn-normal-primary-with-shadow').eq(1).click({force: true});
        cy.wait('@post').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.get('div.card-content span.main-number').nextAll().should('not.have.value', '~');

        cy.wait(3000);
        cy.get('#vs2__combobox > .vs__actions > .vs__open-indicator > path').click({force: true});
        cy.get('ul#vs2__listbox li#vs2__option-1').click({force: true});
        cy.get('div.text-right button.btn-normal-primary-with-shadow').eq(1).click({force: true});
        cy.wait('@post').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.get('div.card-content span.main-number').nextAll().should('not.have.value', '~');
        cy.wait(3000);

        cy.get('#vs2__combobox > .vs__actions > .vs__open-indicator > path').click({force: true});
        cy.get('ul#vs2__listbox li#vs2__option-2').click({force: true});
        cy.get('div.text-right button.btn-normal-primary-with-shadow').eq(1).click({force: true});
        cy.wait('@post').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.get('div.card-content span.main-number').nextAll().should('not.have.value', '~');
        cy.wait(3000);

        cy.get('#vs2__combobox > .vs__actions > .vs__open-indicator > path').click({force: true});
        cy.get('ul#vs2__listbox li#vs2__option-3').click({force: true});
        cy.get('div.text-right button.btn-normal-primary-with-shadow').eq(1).click({force: true});
        cy.wait('@post').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.get('div.card-content span.main-number').nextAll().should('not.have.value', '~');
        cy.wait(3000);

        cy.get('#vs2__combobox > .vs__actions > .vs__open-indicator > path').click({force: true});
        cy.get('ul#vs2__listbox li#vs2__option-4').click({force: true});
        cy.get('div.text-right button.btn-normal-primary-with-shadow').eq(1).click();
        cy.wait('@post').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.get('div.card-content span.main-number').nextAll().should('not.have.value', '~');
        cy.wait(3000);

        cy.get('input.mx-input').clear().type(date);
        cy.get('div.text-right button.btn-normal-primary-with-shadow').eq(1).click();
        cy.wait('@post').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.get('div.card-content span.main-number').nextAll().should('not.have.value', '~');
        cy.wait(3000);

        cy.get('#vs2__combobox > .vs__actions > .vs__open-indicator > path').click({force: true});
        cy.get('ul#vs2__listbox li#vs2__option-0').click({force: true});
        cy.wait('@post').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.get('div.card-content span.main-number').nextAll().should('not.have.value', '~');
    },

    /**
     * Szűrő mentése
     * @param cy
     * @param text
     */
    save(cy, text) {
        cy.get('button.btn-link').eq(1).click({force: true});
        cy.get('input.form-control.form-control-sm').first().type(text);
        cy.get('svg.vs__open-indicator').eq(0).click();
        cy.get('#vs1__listbox li').eq(0).click();
        cy.get('button.btn-normal-primary-with-shadow').eq(0).click();
    },

    /**
     * Szűrési beállítások ellenőrzése
     * @param cy
     * @param num
     * @param email szűrésnél ezt írja be feladóhoz
     * @param text beírandó szöveg
     * @param number1 "Keresés helye"-nél eq(?) gomb (ki kell keresni)
     * @param number2 "Keresés helye"-nél eq(?) gomb (ki kell keresni)
     * @param number3 "Keresés helye"-nél eq(?) gomb (ki kell keresni)
     * @param waitText előre definiált alias megadása
     */
    extended(cy, num, email, text, number1, number2, number3, waitText) {
        cy.get('.pointer > .ico-chewron-down').eq(0).click({force: true});

        cy.get('input.form-control').eq(2).clear({force: true}).type(email);
        cy.get('div.multiselect__select').eq(1).click({force: true});
        cy.get('div.col-md-4 div.form-group ul.multiselect__content li span span').eq(number2).click({force: true});
        cy.get('div.text-right button.btn-normal-primary-with-shadow').eq(1).click({force: true});
        cy.wait(waitText).should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(2000);

        cy.get('input.form-control').eq(2).clear({force: true}).type(text);
        cy.get('div.multiselect__select').eq(1).click({force: true});
        cy.get('div.col-md-4 div.form-group ul.multiselect__content li span span').eq(number2).click({force: true});
        cy.get('div.col-md-4 div.form-group ul.multiselect__content li span span').eq(number1).click({force: true});
        cy.get('div.text-right button.btn-normal-primary-with-shadow').eq(1).click({force: true});
        cy.wait(waitText).should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(2000);

        cy.get('input.form-control').eq(2).clear({force: true}).type(text);
        cy.get('div.multiselect__select').eq(1).click({force: true});
        cy.get('div.col-md-4 div.form-group ul.multiselect__content li span span').eq(number1).click({force: true});
        cy.get('div.col-md-4 div.form-group ul.multiselect__content li span span').eq(number3).click({force: true});
        cy.get('div.text-right button.btn-normal-primary-with-shadow').eq(1).click({force: true});
        cy.wait(waitText).should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(2000);

        cy.get('div.multiselect__select').eq(1).click({force: true});
        cy.get('div.col-md-4 div.form-group ul.multiselect__content li span span').eq(number1).click({force: true});
        cy.get('div.text-right button.btn-normal-primary-with-shadow').eq(1).click({force: true});
        cy.wait(waitText).should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(2000);

        cy.get('input.form-control').eq(2).clear({force: true}).type(email);
        cy.get('div.multiselect__select').eq(1).click({force: true});
        cy.get('div.col-md-4 div.form-group ul.multiselect__content li span span').eq(number1).click({force: true});
        cy.get('div.col-md-4 div.form-group ul.multiselect__content li span span').eq(number2).click({force: true});
        cy.get('div.text-right button.btn-normal-primary-with-shadow').eq(1).click({force: true});
        cy.wait(waitText).should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(2000);

        cy.get('[type="checkbox"]').check({force: true});
        cy.get('div.text-right button.btn-normal-primary-with-shadow').eq(1).click({force: true});
        cy.wait(waitText).should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
    },

    /**
     * Kiterjesztett szűrés az Aktivitás oldalon
     * @param cy
     * @param num
     * @param email
     * @param text
     * @param number1
     * @param number2
     * @param number3
     * @param waitText
     */
    extendedForStatistic(cy, num, email, text, number1, number2, number3, waitText) {
        cy.get('.mt-1 > .col-sm-12 > .input-label > .pointer > .ico-chewron-down').click({force: true});
        cy.get('input.form-control').eq(2).clear({force: true}).type(email);
        cy.get('div.multiselect__select').eq(1).click({force: true});
        cy.get('div.col-md-4 div.form-group ul.multiselect__content li span span').eq(number2).click({force: true});
        cy.get('div.text-right button.btn-normal-primary-with-shadow').eq(1).click({force: true});
        cy.wait(waitText).should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })

        cy.get('input.form-control').eq(2).clear({force: true}).type(text);
        cy.get('div.multiselect__select').eq(1).click({force: true});
        cy.get('div.col-md-4 div.form-group ul.multiselect__content li span span').eq(number2).click({force: true});
        cy.get('div.col-md-4 div.form-group ul.multiselect__content li span span').eq(number1).click({force: true});
        cy.get('div.text-right button.btn-normal-primary-with-shadow').eq(1).click({force: true});
        cy.wait(waitText).should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })

        cy.get('input.form-control').eq(2).clear({force: true}).type(text);
        cy.get('div.multiselect__select').eq(1).click({force: true});
        cy.get('div.col-md-4 div.form-group ul.multiselect__content li span span').eq(number1).click({force: true});
        cy.get('div.col-md-4 div.form-group ul.multiselect__content li span span').eq(number3).click({force: true});
        cy.get('div.text-right button.btn-normal-primary-with-shadow').eq(1).click({force: true});
        cy.wait(waitText).should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })

        cy.get('div.multiselect__select').eq(1).click({force: true});
        cy.get('div.col-md-4 div.form-group ul.multiselect__content li span span').eq(number1).click({force: true});
        cy.get('div.text-right button.btn-normal-primary-with-shadow').eq(1).click({force: true});
        cy.wait(waitText).should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })

        cy.get('input.form-control').eq(2).clear({force: true}).type(email);
        cy.get('div.multiselect__select').eq(1).click({force: true});
        cy.get('div.col-md-4 div.form-group ul.multiselect__content li span span').eq(number1).click({force: true});
        cy.get('div.col-md-4 div.form-group ul.multiselect__content li span span').eq(number2).click({force: true});
        cy.get('div.text-right button.btn-normal-primary-with-shadow').eq(1).click({force: true});
        cy.wait(waitText).should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })

        cy.get('[type="checkbox"]').check({force: true});
        cy.get('div.text-right button.btn-normal-primary-with-shadow').eq(1).click({force: true});
        cy.wait(waitText).should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
    },

    /**
     *
     * @param cy
     */
    filter(cy) {
        let tmp = "";
        cy.wait(3000);
        cy.get('#thread-list-scroll > :nth-child(3)').find('li.list-bg').eq(0).click();
        cy.wait('@email').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(2000);
        cy.get('button.btn-rounded-with-icon-2').eq(4).click();
        cy.get('div.vs__actions svg path').eq(1).click({force: true});
        cy.get('ul.vs__dropdown-menu.vs__fade-enter-active.vs__fade-enter-to li').eq(1).click();
        cy.get('div.vs__actions svg path').eq(3).click({force: true});
        cy.get('ul.vs__dropdown-menu.vs__fade-enter-active.vs__fade-enter-to li').eq(2).click();
        cy.get('input.form-control.form-control-sm').then(($text) => {
            tmp = $text.val();
            cy.log(tmp);
            cy.get('button.btn-normal-primary-with-shadow').eq(0).click({force: true});

            cy.wait('@filter').should((xhr) => {
                expect(xhr.status, 'successful POST').to.equal(200)
            })
            cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
            cy.get('.dusk-filter').click({force: true});
            cy.wait(5000);
            cy.wait('@filters').should((xhr) => {
                expect(xhr.status, 'successful POST').to.equal(200)
            })
            cy.get('table.table tbody tr td').eq(2).contains(tmp).should('exist');
            cy.get('span.danger.pointer').eq(0).click();
            cy.get('button.btn-warning-2').click();
        })
    },

    /**
     * Levél törlése
     * @param cy
     * @param which hanyadik levelet törölje (0 [összes mai], 1, 2, 3, ... stb.)
     */
    deleteLetter(cy, which) {
        cy.get('ul li a.menu-item div span.menu-item').eq(1).click({force: true});
        cy.url().should('include', '/threads');
        cy.get('#thread-list-scroll > :nth-child(3)').find('[type="checkbox"]').eq(which).check({force: true});
        cy.get('.ico-delete-warning').click({force: true});
        cy.wait('@trash').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
    },

    /**
     * 6 levél lezárása
     * @param cy
     */
    moveLetterToClosed(cy) {
        cy.get('ul li a.menu-item div span.menu-item').eq(3).click({force: true});
        cy.url().should('include', '/threads');
        cy.wait('@threads').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200);
        });
        cy.wait(2000);
        for (let i = 1; i <= 6; i++) {
            cy.get('#thread-list-scroll > :nth-child(3)').find('[type="checkbox"]').eq(i).check({force: true});
        }
        cy.get('.ico-close').click({force: true});
        cy.wait('@closed').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200);
        });
        cy.wait(2000);
    },

    /**
     * 6 levél törlése
     * @param cy
     */
    moveLetterToDeleted(cy) {
        cy.get('ul li a.menu-item div span.menu-item').eq(3).click({force: true});
        cy.url().should('include', '/threads');
        cy.wait(2000);
        for (let i = 1; i <= 6; i++) {
            cy.get('#thread-list-scroll > :nth-child(3)').find('[type="checkbox"]').eq(i).check({force: true});
        }
        cy.get('.ico-delete-warning').click({force: true});
        cy.wait('@trash').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        });
        cy.wait(2000);
    },

    /**
     * Level(ek) visszaállítása
     * @param cy
     * @param from melyik mappából (név)
     * @param alias a művelet elvégezte alatt történő post request aliasa
     */
    restoreThreads(cy, from, alias) {
        cy.get('ul li a.menu-item div span.menu-item').each(el => {
            if (el.text().includes(from)) {
                el.click();
            }
        });
        cy.wait(1000).then(() => {
            cy.url().should('include', '/threads');
        }).then(() => {
            cy.wait(2000);
            cy.get('#thread-list-scroll > :nth-child(3)').each((el, ind) => {
                cy.get('#thread-list-scroll > :nth-child(3)').eq(ind).find('[type="checkbox"]').check({force: true});
            });
            cy.get('.ico-follow-light').click({force: true});
            cy.wait(alias).should((xhr) => {
                expect(xhr.status, 'successful POST').to.equal(200)
            });
            cy.wait(2000);
        });
    },

    /**
     * Törölt vagy lezártba rakja a levelet.
     * @param cy
     * @param num
     * @param num2
     */
    closed(cy, num, num2) {
        cy.get('ul li a.menu-item div span.menu-item').eq(1).click({force: true});
        cy.url().should('include', '/threads');
        cy.wait(6000).then(() => {
            cy.get('#thread-list-scroll > :nth-child(3)').find('span.thread-list-name').eq(0).then(function ($elem) {
                cy.log($elem.text().trim())
                cy.get('#thread-list-scroll > :nth-child(3)').find('[type="checkbox"]').eq(1).check({force: true});
                if (num2 === 0) {
                    // lezártba rakja
                    cy.get('.ico-close').click({force: true});
                    cy.wait('@closed').should((xhr) => {
                        expect(xhr.status, 'successful POST').to.equal(200)
                    })
                } else if (num2 === 1) {
                    // töröltek közé rakja
                    cy.get('.ico-delete-warning').click({force: true});
                    cy.wait('@trash').should((xhr) => {
                        expect(xhr.status, 'successful POST').to.equal(200)
                    })
                }

                cy.get('span.thread-list-name').eq(0).should('not.contain', $elem.text().trim());

                // törölt mappába lép bele
                cy.get('ul li a.menu-item div span.menu-item').eq(num).click({force: true});

                cy.url().should('include', '/threads');
                cy.wait(6000);
                cy.get('.hidden-xs > :nth-child(1) > :nth-child(1) > :nth-child(1) > .form-group > .input-group-append').click();
                cy.get('input.form-control.borderless-input').eq(2).type($elem.text().trim());
                cy.get('div.advanced-search-options div div button.btn-normal-primary-with-shadow').eq(1).click();
                cy.wait('@search').should((xhr) => {
                    expect(xhr.status, 'successful POST').to.equal(200)
                })
                cy.get('#thread-list-scroll > :nth-child(3)').find('span.thread-list-name').eq(0);
                cy.wait(2000);

                cy.get('#thread-list-scroll > :nth-child(3)').find('.flex-shrink-1.w-100.ml-0.mr-auto').eq(0).click({force: true});
                cy.get('.email-from-name').contains($elem.text().trim()).should('exist');
                cy.get('.ico-back').click();

                cy.get('#thread-list-scroll > :nth-child(3)').find('[type="checkbox"]').eq(1).check({force: true});
                cy.get('.ico-delete-warning').click();
                if (num2 === 1) {
                    cy.wait('@deleted').should((xhr) => {
                        expect(xhr.status, 'successful POST').to.equal(200)
                    })
                } else {
                    cy.wait('@trash').should((xhr) => {
                        expect(xhr.status, 'successful POST').to.equal(200)
                    })
                }
                cy.wait(2000);
                cy.get('.hidden-xs > :nth-child(1) > .pt-1 > .col-sm-12 > div > .filter > .anv-close').click();
                cy.wait(2000);

                cy.get('#thread-list-scroll > :nth-child(3)').find('[type="checkbox"]').eq(1).check({force: true});
                cy.get('.ico-follow-light').click({force: true});
                cy.wait('@closedRemove').should((xhr) => {
                    expect(xhr.status, 'successful POST').to.equal(200)
                })

                cy.wait(3000);
                cy.get('#thread-list-scroll > :nth-child(3)').find('li.list-bg').eq(0).click();
                cy.wait('@email').should((xhr) => {
                    expect(xhr.status, 'successful POST').to.equal(200)
                })
                cy.wait(2000);
                cy.get('.Delete').eq(1).click({force: true});
                cy.wait('@trash').should((xhr) => {
                    expect(xhr.status, 'successful POST').to.equal(200)
                })

                cy.wait(3000);
                cy.get('#thread-list-scroll > :nth-child(3)').find('li.list-bg').eq(0).click();
                cy.wait('@email').should((xhr) => {
                    expect(xhr.status, 'successful POST').to.equal(200)
                })
                cy.wait(2000);
                cy.get('.Follow').click({force: true});
                cy.wait('@closedRemove').should((xhr) => {
                    expect(xhr.status, 'successful POST').to.equal(200)
                })
                cy.wait(3000);
            })
        });
    },


    /**
     * Szűrés Válaszidőre
     * @param cy
     * @param date
     */
    prod(cy, date) {
        cy.get('div.multiselect__select').eq(0).click({force: true});
        cy.get('ul.multiselect__content li.multiselect__element span span').eq(1).click({force: true});
        cy.get('div.text-right button.btn-normal-primary-with-shadow').eq(1).click({force: true});
        cy.wait('@filt').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })

        cy.get('div.multiselect__select').eq(0).click({force: true});
        cy.get('ul.multiselect__content li.multiselect__element span span').eq(1).click({force: true});
        cy.get('ul.multiselect__content li.multiselect__element span span').eq(2).click({force: true});
        cy.get('div.text-right button.btn-normal-primary-with-shadow').eq(1).click({force: true});
        cy.wait('@filt').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })

        cy.get('div.multiselect__select').eq(0).click({force: true});
        cy.get('ul.multiselect__content li.multiselect__element span span').eq(1).click({force: true});
        cy.get('div.text-right button.btn-normal-primary-with-shadow').eq(1).click({force: true});
        cy.wait('@filt').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })

        cy.get('div.multiselect__select').eq(0).click({force: true});
        cy.get('ul.multiselect__content li.multiselect__element span span').eq(0).click({force: true});
        cy.get('div.text-right button.btn-normal-primary-with-shadow').eq(1).click({force: true});
        cy.wait('@filt').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(3000);

        cy.get('input.mx-input').clear().type(date);
        cy.get('div.text-right button.btn-normal-primary-with-shadow').eq(1).click({force: true});
        cy.wait('@filt').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })

        cy.get('div.col-md-12 div span.pointer span').eq(1).click({force: true});
        cy.get('label.control.control-radio').eq(1).click({force: true});
        cy.get('div.text-right button.btn-normal-primary-with-shadow').eq(1).click({force: true});
        cy.wait('@filt').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(3000);
    },


    /**
     * Levél törlés/ lezárás utáni tesztek a megválaszolt mappában
     * @param cy
     * @param num
     */
    repl(cy, num) {
        cy.get('ul li a.menu-item div span.menu-item').eq(num).click({force: true});
        cy.url().should('include', '/threads');
        cy.wait(5000);
        cy.get('span.thread-list-name').eq(0).then(function ($elem) {
            cy.log($elem.text().trim())
            cy.get('#thread-list-scroll > :nth-child(3)').find('.control_indicator-check.mobil').eq(1).click();

            cy.get('div.thread-icons button.btn-rounded-with-icon').eq(0).click({force: true});
            cy.wait('@closed').should((xhr) => {
                expect(xhr.status, 'successful POST').to.equal(200)
            })
            cy.get('#thread-list-scroll > :nth-child(3)').find('span.thread-list-name').eq(0).should('not.contain', $elem.text().trim());
            // cy.get('a').invoke('attr', 'id');

            cy.get('#thread-list-scroll > :nth-child(3)').find('.control_indicator-check.mobil').eq(1).click();
            cy.get('.ico-delete-warning').click({force: true});
            cy.wait('@trash').should((xhr) => {
                expect(xhr.status, 'successful POST').to.equal(200)
            })
            cy.get('#thread-list-scroll > :nth-child(3)').find('span.thread-list-name').eq(0).should('not.contain', $elem.text().trim());
            cy.wait(3000);
            cy.get('#thread-list-scroll > :nth-child(3)').find('li.list-bg').eq(0).click();
            cy.wait('@email').should((xhr) => {
                expect(xhr.status, 'successful POST').to.equal(200)
            })
            cy.wait(2000);
            cy.get('button.btn-rounded-with-icon-2').eq(1).click();
            cy.wait('@closed').should((xhr) => {
                expect(xhr.status, 'successful POST').to.equal(200)
            })
            cy.wait(3000);
            cy.get('#thread-list-scroll > :nth-child(3)').find('li.list-bg').eq(0).click();
            cy.wait('@email').should((xhr) => {
                expect(xhr.status, 'successful POST').to.equal(200)
            })
            cy.wait(2000);
            cy.get('button.btn-rounded-with-icon-2').eq(2).click();
            cy.wait('@trash').should((xhr) => {
                expect(xhr.status, 'successful POST').to.equal(200)
            })
            cy.wait(3000);

        })
    },

    /**
     * Levél törlés/ lezárás utáni tesztek a spam mappában
     * @param cy
     * @param num
     */
    spam(cy, num) {
        cy.get('ul li a.menu-item div span.menu-item').eq(num).click({force: true});
        cy.url().should('include', '/threads');
        cy.wait(5000);

        cy.get('#thread-list-scroll > :nth-child(3)').find('.control_indicator-check.mobil').eq(1).click();
        cy.get('.ico-delete-warning').click({force: true});
        cy.wait('@trash').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(3000);
        cy.get('#thread-list-scroll > :nth-child(3)').find('li.list-bg').eq(0).click();
        cy.wait('@email').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(2000);
        cy.get('button.btn-rounded-with-icon-2').eq(2).click();
        cy.wait('@trash').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(3000);
    },

    /**
     * Keresési szűrés e-mail cím alapján
     * @param cy
     * @param name
     */
    searchE(cy, name) {
        cy.wait(2000);
        cy.get('.hidden-xs > :nth-child(1) > :nth-child(1) > :nth-child(1) > .form-group > .input-group-append > .pointer').click({force: true});
        cy.get('input.form-control.borderless-input').eq(2).clear().type(name);
        cy.get('div.advanced-search-options div div button.btn-normal-primary-with-shadow').eq(1).click();
        cy.wait('@search').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(3000);
        cy.get('span.thread-list-name').then(($text) => {
            if ($text.text().includes(name)) {
                cy.get('span.thread-list-name').contains(name).should('exist');
            } else {
                cy.log('Search result is not ok!');
            }
        })
    },

    /**
     * Létezik-e az adott e-mail cím
     * @param cy
     * @param email
     */
    getEmail(cy, email) {
        cy.wait(3000);
        cy.get('#thread-list-scroll > :nth-child(3)').find('.flex-shrink-1.w-100.ml-0.mr-auto').eq(0).click({force: true});
        cy.wait(1000);
        cy.get('p.email-from-name').then(($text) => {
            if ($text.text().includes(email)) {
                cy.get('p.email-from-name').contains(email).should('exist');
            } else {
                cy.log('Search result is not ok!');
            }
        })
        cy.get('span.ico-back.pull-left').eq(0).click();

        cy.get('span.anv-close').eq(1).click();
    },

    /**
     * Keresési szűrés adott kifejezés tartalmazása alapján
     * @param cy
     * @param str
     */
    searchT(cy, str) {
        cy.get('.hidden-xs > :nth-child(1) > :nth-child(1) > :nth-child(1) > .form-group > .input-group-append > .pointer').click({force: true});
        cy.get('input.form-control').eq(3).clear().type(str);
        cy.get('div.advanced-search-options div div button.btn-normal-primary-with-shadow').eq(1).click();
        cy.wait('@search').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.get('span.thread-list-subject').then(($text) => {
            if ($text.text().includes(str)) {
                cy.get('span.thread-list-subject').contains(str).should('exist');
            } else {
                cy.log('Search result is not ok!');
            }
        })
    },

    /**
     * Levél tartalmazza-e a szöveget
     * @param cy
     * @param str
     */
    getText(cy, str) {
        cy.log('___INNEN INDUL___');
        cy.wait(3000);
        cy.get('#thread-list-scroll > :nth-child(3)').find('.flex-shrink-1.w-100.ml-0.mr-auto').eq(0).click({force: true});
        cy.wait(3000);
        cy.get('.media-list').then(($text) => {
            if ($text.text().includes(str)) {
                cy.get('.media-list').contains(str, {matchCase: false}).should('exist');
            } else {
                cy.get('h3.email-thread-heading').contains(str, {matchCase: false}).should('exist');
            }
        })
        cy.get('span.ico-back.pull-left').eq(0).click();

        cy.get('input.form-control.form-control-pr').eq(1).clear();
    },

    /**
     * Dátum alapú keresési szűrés
     * @param cy
     * @param date
     * @param date1
     * @param date2
     * @param date3
     */
    searchD(cy, date, date1, date2, date3) {
        cy.get('.hidden-xs > :nth-child(1) > :nth-child(1) > :nth-child(1) > .form-group > .input-group-append > .pointer').click({force: true});
        cy.get('input.mx-input').eq(1).clear().type(date);
        cy.get('div.advanced-search-options div div button.btn-normal-primary-with-shadow').eq(1).click();
        cy.wait('@search').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.get('.date-divider').then(($text) => {
            if ($text.text().includes(date1)) {
                cy.get('li.media.date-divider').contains(date1).should('exist');
            } else if ($text.text().includes(date2)) {
                cy.get('li.media.date-divider').contains(date2).should('exist');
            } else if ($text.text().includes(date3)) {
                cy.get('li.media.date-divider').contains(date3).should('exist');
            } else {
                cy.log('Search result is not ok!');
            }
        })
    },

    /**
     * Dátum helyességének ellenőrzése
     * @param cy
     * @param date1
     * @param date2
     * @param date3
     */
    getDate(cy, date1, date2, date3) {
        cy.wait(3000);
        cy.get('#thread-list-scroll > :nth-child(3)').find('.flex-shrink-1.w-100.ml-0.mr-auto').eq(0).click({force: true});
        cy.wait(1000);
        cy.get('span.mail-date.email-thread-time').then(($text) => {
            if ($text.text().includes(date1)) {
                cy.get('span.mail-date.email-thread-time').contains(date1).should('exist');
            } else if ($text.text().includes(date2)) {
                cy.get('span.mail-date.email-thread-time').contains(date2).should('exist');
            } else if ($text.text().includes(date3)) {
                cy.get('span.mail-date.email-thread-time').contains(date3).should('exist');
            } else {
                cy.log('Search result is not ok!');
            }
        })
        cy.get('span.ico-back.pull-left').eq(0).click();

        cy.get('span.anv-close').eq(1).click();
    },

    /**
     * Szűrésnél a csoportokba felveszi a szűrni kívánt felhasználókat, majd a dátumot.
     * @param cy
     * @param date
     */
    analysis(cy, date) {
        cy.get('div.multiselect__select').eq(0).click({force: true});
        cy.get('ul.multiselect__content li.multiselect__element span span').eq(1).click({force: true});
        cy.get('div.text-right button.btn-normal-primary-with-shadow').eq(1).click({force: true});
        cy.wait('@post').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(1500);

        cy.get('div.multiselect__select').eq(0).click({force: true});
        cy.get('ul.multiselect__content li.multiselect__element span span').eq(1).click({force: true});
        cy.get('ul.multiselect__content li.multiselect__element span span').eq(2).click({force: true});
        cy.get('div.text-right button.btn-normal-primary-with-shadow').eq(1).click({force: true});
        cy.wait('@post').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(1500);

        cy.get('div.multiselect__select').eq(0).click({force: true});
        cy.get('ul.multiselect__content li.multiselect__element span span').eq(2).click({force: true});
        cy.get('ul.multiselect__content li.multiselect__element span span').eq(3).click({force: true});
        cy.get('div.text-right button.btn-normal-primary-with-shadow').eq(1).click({force: true});
        cy.wait('@post').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(1500);

        cy.get('div.multiselect__select').eq(0).click({force: true});
        cy.get('ul.multiselect__content li.multiselect__element span span').eq(2).click({force: true});
        cy.get('div.text-right button.btn-normal-primary-with-shadow').eq(1).click({force: true});
        cy.wait('@post').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(1500);

        cy.get('div.multiselect__select').eq(0).click({force: true});
        cy.get('ul.multiselect__content li.multiselect__element span span').eq(1).click({force: true});
        cy.get('div.text-right button.btn-normal-primary-with-shadow').eq(1).click({force: true});
        cy.wait('@post').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(1500);

        cy.get('div.multiselect__select').eq(0).click({force: true});
        cy.get('ul.multiselect__content li.multiselect__element span span').eq(2).click({force: true});
        cy.get('div.text-right button.btn-normal-primary-with-shadow').eq(1).click({force: true});
        cy.wait('@post').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(1500);

        cy.get('div.multiselect__select').eq(0).click({force: true});
        cy.get('ul.multiselect__content li.multiselect__element span span').eq(0).click({force: true});
        cy.get('div.text-right button.btn-normal-primary-with-shadow').eq(1).click({force: true});
        cy.wait('@post').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(1500);

        cy.get('div.vs__actions svg.vs__open-indicator').eq(1).click({force: true});
        cy.get('ul#vs2__listbox li#vs2__option-1').click({force: true});
        cy.get('div.text-right button.btn-normal-primary-with-shadow').eq(1).click({force: true});
        cy.wait('@post').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(1500);

        cy.get('div.vs__actions svg.vs__open-indicator').eq(1).click({force: true});
        cy.get('ul#vs2__listbox li#vs2__option-2').click({force: true});
        cy.get('div.text-right button.btn-normal-primary-with-shadow').eq(1).click({force: true});
        cy.wait('@post').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(1500);

        cy.get('div.vs__actions svg.vs__open-indicator').eq(1).click({force: true});
        cy.get('ul#vs2__listbox li#vs2__option-3').click({force: true});
        cy.get('div.text-right button.btn-normal-primary-with-shadow').eq(1).click({force: true});
        cy.wait('@post').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(1500);

        cy.get('div.vs__actions svg.vs__open-indicator').eq(1).click({force: true});
        cy.get('ul#vs2__listbox li#vs2__option-4').click({force: true});
        cy.get('div.text-right button.btn-normal-primary-with-shadow').eq(1).click({force: true});
        cy.wait('@post').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(1500);

        cy.get('input.mx-input').clear().type(date);
        cy.get('div.text-right button.btn-normal-primary-with-shadow').eq(1).click({force: true});
        cy.wait('@post').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(1500);

        cy.get('button.vs__clear svg path').eq(1).click();
        cy.get('div.text-right button.btn-normal-primary-with-shadow').eq(1).click({force: true});
        cy.wait('@post').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(1500);

        cy.get('div.multiselect__select').eq(0).click({force: true});
        cy.get('ul.multiselect__content li.multiselect__element span span').eq(0).click({force: true});
        cy.get('.card-footer > .text-right > .btn-normal-primary-with-shadow').click({force: true});
        cy.wait('@post').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(1500);
    },

    /**
     * A levelezésekben megnézi, hogy tartalmaz-e locale szót.
     * @param cy
     */
    localeThread(cy) {
        cy.wait(1000);
        cy.get(':nth-child(3) > :nth-child(1) > .menu-item').click();
        cy.wait(4000);
        cy.get('span.pointer').eq(1).click();
        cy.get('h3').should('not.contain', 'locale.');
        cy.get('.card-footer > .text-right > .btn-normal-primary-with-shadow').should('not.contain', 'locale.');
        cy.get('.modal-container').eq(0).should('not.contain', 'locale.');
        cy.get('.btn-tertiary-modal').eq(0).should('not.contain', 'locale.').click({force: true});
        cy.wait(1000);

        for (let i = 0; i <= 8; i++) {
            cy.get('ul li a.menu-item div span.menu-item').eq(i).click({force: true});
            cy.wait(5000);
            cy.url().should('include', '/threads');
            cy.get('.media.date-divider').should('not.contain', 'locale.');
            cy.get('button.pointer').eq(1).click({force: true});
            cy.get('.input-label').should('not.contain', 'locale.');
            cy.get('button.btn-normal-primary-with-shadow').should('not.contain', 'locale.');
            cy.get('.btn-tertiary').should('not.contain', 'locale.');
            cy.get('button.pointer').eq(1).click({force: true});
            cy.get('li.list-bg').should('not.contain', 'locale.');
            cy.wait(3000);
            cy.get('#thread-list-scroll > :nth-child(3)').find('.flex-shrink-1.w-100.ml-0.mr-auto').eq(0).click({force: true});
            cy.wait('@email').should((xhr) => {
                expect(xhr.status, 'successful POST').to.equal(200)
            })
            cy.wait(3000);
            cy.get('.title-2').should('not.contain', 'locale.');
            cy.get('.card-header').should('not.contain', 'locale.');
            cy.get('.card-body').should('not.contain', 'locale.');
            cy.get('button.btn-rounded-with-icon-2').eq(4).click({force: true});
            cy.get('h3').should('not.contain', 'locale.');
            cy.get('.info-tooltip').should('not.contain', 'locale.');
            cy.get('.input-label').should('not.contain', 'locale.');
            cy.get('.add_new_filter-label').should('not.contain', 'locale.');
            cy.get('button.btn-normal-primary-with-shadow').should('not.contain', 'locale.');
            cy.get('div.vs__actions svg path').eq(1).click({force: true});
            cy.get('ul.vs__dropdown-menu.vs__fade-enter-active.vs__fade-enter-to li').should('not.contain', 'locale.');
            cy.get('div.vs__actions svg path').eq(3).click({force: true});
            cy.get('ul.vs__dropdown-menu.vs__fade-enter-active.vs__fade-enter-to li').should('not.contain', 'locale.');
            cy.get('.btn-tertiary').should('not.contain', 'locale.').eq(0).click({force: true});
        }
    },

    /**
     * A statisztikákban megnézi, hogy tartalmaz-e locale szót.
     * @param cy
     */
    localeStatistics(cy) {
        cy.wait(2000);
        cy.get('.menu-title').should('not.contain', 'locale.');
        cy.get('.menu-item').should('not.contain', 'locale.');
        cy.get('.content-header-title').should('not.contain', 'locale.');
        cy.get('.toast-messages').should('not.contain', 'locale.');
        cy.get('.navbar-wrapper').should('not.contain', 'locale.');
        cy.get('.synctext').should('not.contain', 'locale.');
        cy.get('.col-md-12 > .card').should('not.contain', 'locale.');

        cy.get('.card-label').should('not.contain', 'locale.');
        cy.get('.card-titles').should('not.contain', 'locale.');

        cy.get('#allTrafficChart div span svg g g g g text').should('not.contain', 'locale.');
        cy.get('#sentEmails div span svg g g g g text').should('not.contain', 'locale.');
        cy.get('#replyTimeChart div span svg g g g g text').should('not.contain', 'locale.');
        cy.get('#all-email-traffic div span svg g g g g text').first().should('not.contain', 'locale.');
        cy.get('#all-email-traffic div span svg g g g g text').eq(1).should('not.contain', 'locale.');
        cy.get('#all-email-traffic div span svg g g g g text').eq(2).should('not.contain', 'locale.');

        cy.get('table.top-list.body-2 tr td').should('not.contain', 'locale.');
        cy.get('.input-label').should('not.contain', 'locale.');
        cy.get('.btn-link').should('not.contain', 'locale.');
        cy.get('.btn-normal-primary-with-shadow').should('not.contain', 'locale.');
        cy.get('div.multiselect__select').eq(0).click({force: true});
        cy.get('ul.multiselect__content li.multiselect__element span span').should('not.contain', 'locale.');
        cy.get('div.multiselect__select').eq(0).click({force: true});
        cy.get('div.vs__actions svg.vs__open-indicator').eq(1).click({force: true});
        cy.get('ul#vs2__listbox li#vs2__option-1').should('not.contain', 'locale.');
        cy.get('ul#vs2__listbox li#vs2__option-2').should('not.contain', 'locale.');
        cy.get('ul#vs2__listbox li#vs2__option-3').should('not.contain', 'locale.');
        cy.get('ul#vs2__listbox li#vs2__option-4').should('not.contain', 'locale.');
        cy.get('ul#vs2__listbox li#vs2__option-5').should('not.contain', 'locale.');

        cy.get('.pointer > .ico-chewron-down').click({force: true});
        cy.get('.input-label').should('not.contain', 'locale.');
        cy.get('div.multiselect__select').eq(1).click({force: true});
        cy.get('ul.multiselect__content li span span').should('not.contain', 'locale.');
        cy.get('button.btn-link').eq(1).click({force: true});
        cy.get('h3').should('not.contain', 'locale.');
        cy.get('.input-label').should('not.contain', 'locale.');
        cy.get('#vs1__combobox > .vs__actions > .vs__open-indicator > path').click({force: true});
        cy.get('ul#vs1__listbox li#vs1__option-0').should('not.contain', 'locale.');
        cy.get('ul#vs1__listbox li#vs1__option-1').should('not.contain', 'locale.').click();
        cy.get('.input-label').should('not.contain', 'locale.');
        cy.get('input').should('not.contain', 'locale.');
        cy.get('button.btn-normal-primary-with-shadow').should('not.contain', 'locale.');
        cy.get('.btn-tertiary').should('not.contain', 'locale.').click({force: true});

        cy.wait(1000);

        cy.get('#opinionIndex div span svg g g g g g path[d^="M1"]').eq(0).click({force: true});
        cy.wait(1000);
        cy.get('.content-body > :nth-child(1) > .modal-wrapper > .modal-container').should('not.contain', 'locale.');
        cy.get('.content-body > :nth-child(1) > .modal-wrapper > .modal-container > .modal-header > .flex-shrink-1').should('not.contain', 'locale.');
        cy.get('#thread-list-scroll').should('not.contain', 'locale.');

        cy.wait(1000);

        cy.get('ul#main-menu-navigation li a').eq(2).click({force: true});
        cy.url().should('include', '/report/daily_email_prod');
        cy.wait('@filt').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.get('.header-label').should('not.contain', 'locale.');
        cy.get('table tr th').should('not.contain', 'locale.');
        cy.get('.card-body').should('not.contain', 'locale.');
        cy.get('.card-footer').should('not.contain', 'locale.');
        cy.get('.input-label').should('not.contain', 'locale.');
        cy.get('.btn-link').should('not.contain', 'locale.');
        cy.get('.btn-normal-primary-with-shadow').should('not.contain', 'locale.');
        cy.get('div.multiselect__select').eq(0).click({force: true});
        cy.get('ul.multiselect__content li.multiselect__element span span').should('not.contain', 'locale.');
        cy.get('div.multiselect__select').eq(0).click({force: true});
        cy.get('div.col-md-12 div span.pointer span').eq(0).click({force: true});
        cy.get('.input-label').should('not.contain', 'locale.');
        cy.get('.control').should('not.contain', 'locale.');
        cy.get('div.multiselect__select').eq(1).click({force: true});
        cy.get('ul.multiselect__content li span span').should('not.contain', 'locale.');
        cy.get('div.col-md-12 div span.pointer span').eq(1).click({force: true});
        cy.get('.control').should('not.contain', 'locale.');
        cy.get('.mx-input').should('not.contain', 'locale.');

        cy.get('button.btn-link').eq(1).click({force: true});
        cy.get('h3').should('not.contain', 'locale.');
        cy.get('.input-label').should('not.contain', 'locale.');
        cy.get('#vs1__combobox > .vs__actions > .vs__open-indicator > path').click({force: true});
        cy.get('ul#vs1__listbox li#vs1__option-0').should('not.contain', 'locale.');
        cy.get('ul#vs1__listbox li#vs1__option-1').should('not.contain', 'locale.').click();
        cy.get('.input-label').should('not.contain', 'locale.');
        cy.get('input').should('not.contain', 'locale.');
        cy.get('button.btn-normal-primary-with-shadow').should('not.contain', 'locale.');
        cy.get('.btn-tertiary').should('not.contain', 'locale.').click({force: true});
        cy.get('#downloadDropdown').should('not.contain', 'locale.').click({force: true});
        cy.get('.dropdown-item > .btn-link').should('not.contain', 'locale.');

        cy.wait(1000);

        cy.get('ul#main-menu-navigation li a').eq(3).click({force: true});
        cy.url().should('include', '/report/reaction_time');
        cy.get('h2').should('not.contain', 'locale.');
        cy.get('table tr th').should('not.contain', 'locale.');
        cy.get('.card-body').should('not.contain', 'locale.');
        cy.get('.card-footer').should('not.contain', 'locale.');

        cy.get('.input-label').should('not.contain', 'locale.');
        cy.get('.btn-link').should('not.contain', 'locale.');
        cy.get('.btn-normal-primary-with-shadow').should('not.contain', 'locale.');
        cy.get('div.multiselect__select').eq(0).click({force: true});
        cy.get('ul.multiselect__content li.multiselect__element span span').should('not.contain', 'locale.');
        cy.get('div.multiselect__select').eq(0).click({force: true});
        cy.get('div.vs__actions svg.vs__open-indicator').eq(1).click({force: true});
        cy.get('ul#vs2__listbox li#vs2__option-1').should('not.contain', 'locale.');
        cy.get('ul#vs2__listbox li#vs2__option-2').should('not.contain', 'locale.');
        cy.get('ul#vs2__listbox li#vs2__option-3').should('not.contain', 'locale.');
        cy.get('ul#vs2__listbox li#vs2__option-4').should('not.contain', 'locale.');
        cy.get('ul#vs2__listbox li#vs2__option-5').should('not.contain', 'locale.');
        cy.get('.mx-input').should('not.contain', 'locale.');

        cy.get('div.col-md-12 div span.pointer span').eq(0).click({force: true});
        cy.get('.input-label').should('not.contain', 'locale.');
        cy.get('.control').should('not.contain', 'locale.');
        cy.get('div.multiselect__select').eq(1).click({force: true});
        cy.get('ul.multiselect__content li span span').should('not.contain', 'locale.');
        cy.get('div.col-md-12 div span.pointer span').eq(1).click({force: true});
        cy.get('.control').should('not.contain', 'locale.');

        cy.get('button.btn-link').eq(1).click({force: true});
        cy.get('h3').should('not.contain', 'locale.');
        cy.get('.input-label').should('not.contain', 'locale.');
        cy.get('#vs1__combobox > .vs__actions > .vs__open-indicator > path').click({force: true});
        cy.get('ul#vs1__listbox li#vs1__option-0').should('not.contain', 'locale.');
        cy.get('ul#vs1__listbox li#vs1__option-1').should('not.contain', 'locale.').click();
        cy.get('.input-label').should('not.contain', 'locale.');
        cy.get('input').should('not.contain', 'locale.');
        cy.get('button.btn-normal-primary-with-shadow').should('not.contain', 'locale.');
        cy.get('.btn-tertiary').should('not.contain', 'locale.').click({force: true});

        cy.wait(1000);

        cy.get('ul#main-menu-navigation li ul li a').eq(0).click({force: true});
        cy.url().should('include', '/opinion-analysis');
        cy.wait(4000);
        cy.get('.input-label').should('not.contain', 'locale.');
        cy.get('.btn-link').should('not.contain', 'locale.');
        cy.get('.btn-normal-primary-with-shadow').should('not.contain', 'locale.');
        cy.get('div.multiselect__select').eq(0).click({force: true});
        cy.get('ul.multiselect__content li.multiselect__element span span').should('not.contain', 'locale.');
        cy.get('div.multiselect__select').eq(0).click({force: true});
        cy.wait(1000);
        cy.get('#vs2__combobox > .vs__actions > .vs__open-indicator > path').click({force: true});
        cy.get('ul#vs2__listbox li#vs2__option-1').should('not.contain', 'locale.');
        cy.get('ul#vs2__listbox li#vs2__option-2').should('not.contain', 'locale.');
        cy.get('ul#vs2__listbox li#vs2__option-3').should('not.contain', 'locale.');
        cy.get('ul#vs2__listbox li#vs2__option-4').should('not.contain', 'locale.');
        cy.get('ul#vs2__listbox li#vs2__option-5').should('not.contain', 'locale.');
        cy.get('.mx-input').should('not.contain', 'locale.');

        cy.get('div.col-md-12 div span.pointer span').eq(0).click({force: true});
        cy.get('.input-label').should('not.contain', 'locale.');
        cy.get('.control').should('not.contain', 'locale.');
        cy.get('div.multiselect__select').eq(1).click({force: true});
        cy.get('ul.multiselect__content li span span').should('not.contain', 'locale.');

        cy.get('h3').should('not.contain', 'locale.');
        cy.get('.card').should('not.contain', 'locale.');
        cy.get('.card-titles').should('not.contain', 'locale.');
        cy.get('.sort-btn').should('not.contain', 'locale.');

        cy.get('#vs3__combobox > .vs__actions').should('not.contain', 'locale.').click({force: true});
        cy.get('ul#vs3__listbox li#vs3__option-0').should('not.contain', 'locale.');
        cy.get('ul#vs3__listbox li#vs3__option-1').should('not.contain', 'locale.');
        cy.get('#vs5__combobox > .vs__actions').should('not.contain', 'locale.').click({force: true});
        cy.get('ul#vs5__listbox li#vs5__option-0').should('not.contain', 'locale.');
        cy.get('ul#vs5__listbox li#vs5__option-1').should('not.contain', 'locale.');
        cy.get('#vs7__combobox > .vs__actions').should('not.contain', 'locale.').click({force: true});
        cy.get('ul#vs7__listbox li#vs7__option-0').should('not.contain', 'locale.');
        cy.get('ul#vs7__listbox li#vs7__option-1').should('not.contain', 'locale.');

        cy.get('.text-right > .btn-link').click({force: true});
        cy.get('h3').should('not.contain', 'locale.');
        cy.get('.input-label').should('not.contain', 'locale.');
        cy.get('#vs1__combobox > .vs__actions > .vs__open-indicator > path').click({force: true});
        cy.get('ul#vs1__listbox li#vs1__option-0').should('not.contain', 'locale.');
        cy.get('ul#vs1__listbox li#vs1__option-1').should('not.contain', 'locale.').click();
        cy.get('.input-label').should('not.contain', 'locale.');
        cy.get('input').should('not.contain', 'locale.');
        cy.get('button.btn-normal-primary-with-shadow').should('not.contain', 'locale.');
        cy.get('.btn-tertiary').should('not.contain', 'locale.').click({force: true});

        cy.get('#opinionIndex div span svg g g g g text').should('not.contain', 'locale.');
        cy.get('#opinionIndex div span svg g g g g g path[d^="M1"]').eq(0).click({force: true});
        cy.wait(1000);
        cy.get('.content-body > :nth-child(1) > .modal-wrapper > .modal-container').should('not.contain', 'locale.');
        cy.get('.content-body > :nth-child(1) > .modal-wrapper > .modal-container > .modal-header > .flex-shrink-1').should('not.contain', 'locale.');
        cy.get('#thread-list-scroll').should('not.contain', 'locale.');

        cy.wait(1000);

        cy.get('ul#main-menu-navigation li ul li a').eq(1).click({force: true});
        cy.url().should('include', '/emotion-analysis');
        cy.wait(4000);
        cy.get('.input-label').should('not.contain', 'locale.');
        cy.get('.btn-link').should('not.contain', 'locale.');
        cy.get('.btn-normal-primary-with-shadow').should('not.contain', 'locale.');
        cy.get('div.multiselect__select').eq(0).click({force: true});
        cy.get('ul.multiselect__content li.multiselect__element span span').should('not.contain', 'locale.');
        cy.get('div.multiselect__select').eq(0).click({force: true});
        cy.wait(1000);
        cy.get('#vs2__combobox > .vs__actions > .vs__open-indicator > path').click({force: true});
        cy.get('ul#vs2__listbox li#vs2__option-1').should('not.contain', 'locale.');
        cy.get('ul#vs2__listbox li#vs2__option-2').should('not.contain', 'locale.');
        cy.get('ul#vs2__listbox li#vs2__option-3').should('not.contain', 'locale.');
        cy.get('ul#vs2__listbox li#vs2__option-4').should('not.contain', 'locale.');
        cy.get('ul#vs2__listbox li#vs2__option-5').should('not.contain', 'locale.');
        cy.get('.mx-input').should('not.contain', 'locale.');

        cy.get('div.col-md-12 div span.pointer span').eq(0).click({force: true});
        cy.get('.input-label').should('not.contain', 'locale.');
        cy.get('.control').should('not.contain', 'locale.');
        cy.get('div.multiselect__select').eq(1).click({force: true});
        cy.get('ul.multiselect__content li span span').should('not.contain', 'locale.');

        cy.get('h1').should('not.contain', 'locale.');
        cy.get('.card-titles').should('not.contain', 'locale.');
        cy.get('.sort-btn').should('not.contain', 'locale.');
        cy.get('#emotionRadarChart text').should('not.contain', 'locale.');
        cy.get('.small').should('not.contain', 'locale.');
        cy.get('.empty-thread-list-label').should('not.contain', 'locale.');
        cy.get('#vs4__combobox > .vs__actions').should('not.contain', 'locale.').click({force: true});
        cy.get('ul#vs4__listbox li#vs4__option-0').should('not.contain', 'locale.');
        cy.get('ul#vs4__listbox li#vs4__option-1').should('not.contain', 'locale.');

        cy.get(':nth-child(2) > .dropdown > #navbarDropdown').click({force: true});
        cy.get('.dropdown-item > .btn-link').should('not.contain', 'locale.');

        cy.get('.text-right > .btn-link').click({force: true});
        cy.get('h3').should('not.contain', 'locale.');
        cy.get('.input-label').should('not.contain', 'locale.');
        cy.get('#vs1__combobox > .vs__actions > .vs__open-indicator > path').click({force: true});
        cy.get('ul#vs1__listbox li#vs1__option-0').should('not.contain', 'locale.');
        cy.get('ul#vs1__listbox li#vs1__option-1').should('not.contain', 'locale.').click();
        cy.get('.input-label').should('not.contain', 'locale.');
        cy.get('input').should('not.contain', 'locale.');
        cy.get('button.btn-normal-primary-with-shadow').should('not.contain', 'locale.');
        cy.get('.btn-tertiary').should('not.contain', 'locale.').click({force: true});
    },

    /**
     * Megnézi, hogy a beállításokban van-e locale-szó.
     * @param cy
     */
    localeSet(cy) {
        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.get('a.dropdown-item').should('not.contain', 'locale.');
        cy.get('a.dropdown-item.dusk-profile').click();
        cy.get('h2').should('not.contain', 'locale.');
        cy.get('.btn-warning-tertiary.m-auto').should('not.contain', 'locale.').click();
        cy.get('h3').should('not.contain', 'locale.');
        cy.get('.flex-row.modal-delete').should('not.contain', 'locale.');
        cy.get('.btn-warning-2').should('not.contain', 'locale.');
        cy.get('.btn-tertiary-modal').should('not.contain', 'locale.').eq(0).click();
        cy.get('div.content-body span.primary.pointer').eq(0).click();
        cy.get('.title-2').should('not.contain', 'locale.');
        cy.get('.input-label').should('not.contain', 'locale.');
        cy.get('.control').should('not.contain', 'locale.');
        cy.get('input').should('not.contain', 'locale.');
        cy.get('div.vs__actions svg path').eq(1).click({force: true});
        cy.get('ul#vs1__listbox li').should('not.contain', 'locale.');
        cy.get('div.vs__actions svg path').eq(1).click({force: true});
        cy.get('.btn-tertiary').should('not.contain', 'locale.');
        cy.get('button.btn-normal-primary-with-shadow').should('not.contain', 'locale.');
        cy.get('div.content-body span.primary.pointer').eq(0).click();

        cy.get('div.content-body span.primary.pointer').eq(1).click();
        cy.get('.title-2').should('not.contain', 'locale.');
        cy.get('.form-group.body-2.separator').should('not.contain', 'locale.');
        cy.get('.input-label').should('not.contain', 'locale.');
        cy.get('.control').should('not.contain', 'locale.');
        cy.get('span.text').should('not.contain', 'locale.');
        cy.get('div.vs__actions svg path').eq(3).click({force: true});
        cy.get('#vs2__option-0').should('not.contain', 'locale.');
        cy.get('#vs2__option-1').should('not.contain', 'locale.');
        cy.get('div.vs__actions svg path').eq(3).click({force: true});
        cy.get('div.vs__actions svg path').eq(5).click({force: true});
        cy.get('ul#vs3__listbox li').should('not.contain', 'locale.');
        cy.get('.btn-tertiary').should('not.contain', 'locale.');
        cy.get('button.btn-normal-primary-with-shadow').should('not.contain', 'locale.');
        cy.get('div.content-body span.primary.pointer').eq(1).click();

        cy.get('div.content-body span.primary.pointer').eq(2).click();
        cy.get('.title-2').should('not.contain', 'locale.');
        cy.get('.input-label').should('not.contain', 'locale.');
        cy.get('.btn-tertiary').should('not.contain', 'locale.');
        cy.get('button.btn-normal-primary-with-shadow').should('not.contain', 'locale.');
        cy.get('div.content-body span.primary.pointer').eq(2).click();

        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.get('a.dropdown-item').should('not.contain', 'locale.');
        cy.get('a.dropdown-item.dusk-domain-settings').click({force: true});
        cy.get('h2').should('not.contain', 'locale.');
        cy.get('li.nav-item').should('not.contain', 'locale.');
        cy.get('.input-label').should('not.contain', 'locale.');
        cy.get('.form-group.input-label.upper.separator').should('not.contain', 'locale.');
        cy.get('input').should('not.contain', 'locale.');
        cy.get('.ico-delete-warning').eq(0).click();
        cy.get('h3').should('not.contain', 'locale.');
        cy.get('.flex-row.modal-delete').should('not.contain', 'locale.');
        cy.get('.btn-warning-2').should('not.contain', 'locale.');
        cy.get('.btn-tertiary-modal').should('not.contain', 'locale.').click();
        cy.get('button.btn-normal-primary-with-shadow').should('not.contain', 'locale.');
        cy.wait(1000);
        cy.get('div.vs__actions svg.vs__open-indicator').eq(0).click();
        cy.get('ul#vs1__listbox li').should('not.contain', 'locale.');
        cy.get('div.vs__actions svg.vs__open-indicator').eq(0).click();
        cy.get('div.vs__actions svg.vs__open-indicator').eq(1).click({force: true});
        cy.get('ul#vs2__listbox li').should('not.contain', 'locale.');
        cy.get('div.vs__actions svg.vs__open-indicator').eq(1).click({force: true});
        cy.get('div.vs__actions svg.vs__open-indicator').eq(2).click({force: true});
        cy.get('ul#vs3__listbox li').should('not.contain', 'locale.');
        cy.get('div.vs__actions svg.vs__open-indicator').eq(2).click({force: true});

        cy.get('span.ico-plus').click({force: true});
        cy.get('h3').should('not.contain', 'locale.');
        cy.get('.input-label').should('not.contain', 'locale.');
        cy.get('.form-group.input-label.upper.separator').should('not.contain', 'locale.');
        cy.get('span.question.pull-right').should('not.contain', 'locale.');
        cy.get('div.vs__actions svg path').eq(1).click({force: true});
        cy.get('ul.vs__dropdown-menu li').should('not.contain', 'locale.');
        cy.get('div.vs__actions svg path').eq(1).click({force: true});
        cy.get('div.vs__actions svg path').eq(3).click({force: true});
        cy.get('ul.vs__dropdown-menu li').should('not.contain', 'locale.');
        cy.get('div.vs__actions svg path').eq(3).click({force: true});
        cy.get('div.vs__actions svg path').eq(5).click({force: true});
        cy.get('ul.vs__dropdown-menu li').should('not.contain', 'locale.');
        cy.get('div.vs__actions svg path').eq(5).click({force: true});
        cy.get('button.btn-normal-primary-with-shadow').should('not.contain', 'locale.');
        cy.get('.ico-x.small.close').eq(0).click();

        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.get('a.dropdown-item').should('not.contain', 'locale.');
        cy.get('a.dropdown-item.dusk-report-config').click();
        cy.get('h2').should('not.contain', 'locale.');
        cy.get('span.title-2').should('not.contain', 'locale.');
        cy.get('.input-label').should('not.contain', 'locale.');
        cy.get('ul.list-group').should('not.contain', 'locale.');
        cy.get('div.vs__actions svg path').eq(1).click({force: true});
        cy.get('ul#vs1__listbox li').should('not.contain', 'locale.');
        cy.get('div.vs__actions svg path').eq(1).click({force: true});
        cy.get('.form-group.body-2.separator').should('not.contain', 'locale.');
        cy.get('table tr th').should('not.contain', 'locale.');
        cy.get('table tr td').should('not.contain', 'locale.');

        cy.get('.btn-link').should('not.contain', 'locale.');
        cy.get('span.ico-plus').eq(1).click({force: true});
        cy.get('h3').should('not.contain', 'locale.');
        cy.get('.input-label').should('not.contain', 'locale.');
        cy.get('button.btn-normal-primary-with-shadow').should('not.contain', 'locale.');
        cy.get('.btn-link').should('not.contain', 'locale.');
        cy.get('.btn-tertiary').should('not.contain', 'locale.').eq(0).click();

        cy.get('img.rectangle-gray').eq(0).click();
        cy.get('h3').should('not.contain', 'locale.');
        cy.get('.input-label').should('not.contain', 'locale.');
        cy.get('.form-control').should('not.contain', 'locale.');
        cy.get('button.btn-normal-primary-with-shadow').should('not.contain', 'locale.');
        cy.get('.btn-tertiary').should('not.contain', 'locale.').eq(0).click();

        cy.get('img.rectangle-gray').eq(1).click();
        cy.get('h3').should('not.contain', 'locale.');
        cy.get('.flex-row.modal-delete').should('not.contain', 'locale.');
        cy.get('.btn-warning-2').should('not.contain', 'locale.');
        cy.get('.btn-tertiary-modal').should('not.contain', 'locale.').eq(0).click();

        cy.get('.primary.mr-1.ico-chewron-down').eq(0).click();

        cy.get('span.ico-plus').eq(0).click();
        cy.get('h3').should('not.contain', 'locale.');
        cy.get('.input-label').should('not.contain', 'locale.');
        cy.get('button.btn-normal-primary-with-shadow').should('not.contain', 'locale.');
        cy.get('.btn-tertiary').should('not.contain', 'locale.').eq(0).click();

        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.get('a.dropdown-item').eq(4).click();
        cy.get('h2').should('not.contain', 'locale.');
        cy.get('h3').should('not.contain', 'locale.');
        cy.get('.body-3').should('not.contain', 'locale.');
        cy.get('a#account-tab').should('not.contain', 'locale.');
        cy.get('table thead').should('not.contain', 'locale.');
        cy.get('table tbody').should('not.contain', 'locale.');
        cy.get('a#account-wo-tab').should('not.contain', 'locale.').click();
        cy.get('table tbody').should('not.contain', 'locale.');
        cy.get('table tbody').should('not.contain', 'locale.');

        cy.get('button.btn-link').eq(0).should('not.contain', 'locale.').click();
        cy.wait(1000);
        cy.get('h3').should('not.contain', 'locale.');
        cy.get('.input-label').should('not.contain', 'locale.');
        cy.get('.editor__content').should('not.contain', 'locale.');
        cy.get('div.vs__actions svg.vs__open-indicator').eq(0).click({force: true});
        cy.get('ul#vs1__listbox li').should('not.contain', 'locale.');
        cy.get('div.vs__actions svg.vs__open-indicator').eq(0).click({force: true});
        cy.get('div.vs__actions svg.vs__open-indicator').eq(1).click({force: true});
        cy.get('ul#vs2__listbox li').should('not.contain', 'locale.');
        cy.get('div.vs__actions svg.vs__open-indicator').eq(1).click({force: true});
        cy.get('button.btn-normal-primary-with-shadow').should('not.contain', 'locale.');
        cy.get('.btn-tertiary').should('not.contain', 'locale.').eq(0).click();
    },

    /**
     * Ellenőrzi, hogy a beállításokban a spam szűrőknél van-e locale szó.
     * @param cy
     */
    localeMess(cy) {
        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.get('a.dropdown-item').should('not.contain', 'locale.');
        cy.get('a.dropdown-item.dusk-filter').eq(0).click();
        // cy.wait('@messenger').should((xhr) => {
        //     expect(xhr.status, 'successful POST').to.equal(200)
        // })
        cy.wait(3000);
        cy.get('h2').should('not.contain', 'locale.');
        cy.get('table tr th').should('not.contain', 'locale.');
        cy.get('table tr td').should('not.contain', 'locale.');
        // cy.get('.btn-warning-tertiary').should('not.contain', 'locale.');
        cy.get('button.btn-link').should('not.contain', 'locale.');
    },

    localeMessWoC(cy) {
        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.get('a.dropdown-item').should('not.contain', 'locale.');
        cy.get('a.dropdown-item.dusk-filter').eq(0).click();
        cy.wait('@messenger').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(3000);
        cy.get('h3').should('not.contain', 'locale.');
        cy.get('.messenger-why-use').should('not.contain', 'locale.');
        cy.get('.messenger-how-work').should('not.contain', 'locale.');
        cy.get('.btn-normal-primary').should('not.contain', 'locale.');
    },

    /**
     * Bejelentkezés előtti locale szó létezésének vizsgálata.
     * @param cy
     * @param url
     */
    localeText(cy, url) {
        cy.visit(url);
        cy.url().should('include', '/login');
        cy.get('.input-label').should('not.contain', 'locale.');
        cy.get('.card-header').should('not.contain', 'locale.');
        cy.get('.card-body').should('not.contain', 'locale.');
        cy.get('.card-footer').should('not.contain', 'locale.');
        cy.get('.navbar-wrapper').should('not.contain', 'locale.');
        cy.get('.btn-big-primary-with-shadow-modal').should('not.contain', 'locale.');
        cy.get('.my-1 > .btn-link').should('not.contain', 'locale.');
        cy.get('.dropdown-toggle').should('not.contain', 'locale.').click();
        cy.get('.dropdown-menu').should('not.contain', 'locale.');
        cy.get('.col-sm-12 > .btn-link').should('not.contain', 'locale.').click();
        cy.wait(2000);

        cy.get('.input-label').should('not.contain', 'locale.');
        cy.get('.card-header').should('not.contain', 'locale.');
        cy.get('.card-body').should('not.contain', 'locale.');
        cy.get('.card-footer').should('not.contain', 'locale.');
        cy.get('.navbar-wrapper').should('not.contain', 'locale.');
        cy.get('.toast-messages').should('not.contain', 'locale.');
        cy.get('div.sub-card-content').should('not.contain', 'locale.');
        cy.get('.control-group').should('not.contain', 'locale.');
        cy.get('.btn-big-primary-with-shadow').should('not.contain', 'locale.');
        cy.get('.dropdown-toggle').should('not.contain', 'locale.').click();
        cy.get('.dropdown-menu').should('not.contain', 'locale.');
        cy.get('.btn-link.underline').should('not.contain', 'locale.').click();
        cy.wait(2000);

        cy.get('.my-1 > .btn-link').click();
        cy.wait(2000);

        cy.get('.input-label').should('not.contain', 'locale.');
        cy.get('.card-header').should('not.contain', 'locale.');
        cy.get('.card-body').should('not.contain', 'locale.');
        cy.get('.card-footer').should('not.contain', 'locale.');
        cy.get('.navbar-wrapper').should('not.contain', 'locale.');
        cy.get('.btn-big-primary-with-shadow').should('not.eq', 'locale.');
        cy.get('label').should('not.eq', 'locale.');
        cy.get('.dropdown-toggle').should('not.contain', 'locale.').click();
        cy.get('.dropdown-menu').should('not.contain', 'locale.');
        cy.get('.btn-link.underline').should('not.contain', 'locale.').click();
    },

    /**
     * Profil beállításokban az ország módosítása után a valuta helyes változásának ellenőrzése az előfizetés oldalon.
     * @param cy
     * @param value1
     * @param value2
     * @param value3
     * @param value4
     * @param country1
     * @param country2
     * @param country3
     * @param country4
     */
    eurohuf(cy, value1, value2, value3, value4, country1, country2, country3, country4) {
        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.get('a.dropdown-item.dusk-subscription').click();

        cy.get('#currency').select('HUF');
        cy.get('.sub-label').eq(1).should('not.contain', value1);
        cy.get('#currency').select('EUR');
        cy.get('.sub-label').eq(1).should('not.contain', value2);

        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.get('a.dropdown-item.dusk-profile').click();
        cy.get('div.content-body span.primary.pointer').eq(0).click();
        cy.get('div.vs__actions svg path').eq(1).click({force: true});
        cy.get('input.vs__search').eq(0).type(country1);
        cy.get('ul#vs1__listbox li#vs1__option-0').click({force: true});
        cy.get('div.card-footer button.btn-normal-primary-with-shadow').eq(0).click();
        cy.wait('@update2').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(5000);

        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.get('a.dropdown-item.dusk-subscription').click();

        cy.get('#currency').select('HUF');
        cy.get('.sub-label').eq(1).should('contain', value1);
        cy.get('#currency').select('EUR');
        cy.get('.sub-label').eq(1).should('contain', value2);

        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.get('a.dropdown-item.dusk-profile').click();
        cy.get('div.content-body span.primary.pointer').eq(0).click();
        cy.get('div.vs__actions svg path').eq(1).click({force: true});
        cy.get('input.vs__search').eq(0).type(country2);
        cy.get('ul#vs1__listbox li#vs1__option-0').click({force: true});
        cy.get('div.card-footer button.btn-normal-primary-with-shadow').eq(0).click();
        cy.wait('@update2').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(5000);

        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.get('a.dropdown-item.dusk-subscription').click();

        cy.get('#currency').select('HUF');
        cy.get('.sub-label').eq(1).should('contain', value1);
        cy.get('#currency').select('EUR');
        cy.get('.sub-label').eq(1).should('contain', value2);

        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.get('a.dropdown-item.dusk-profile').click();
        cy.get('div.content-body span.primary.pointer').eq(0).click();
        cy.get('div.vs__actions svg path').eq(1).click({force: true});
        cy.get('input.vs__search').eq(0).type(country3);
        cy.get('ul#vs1__listbox li#vs1__option-0').click({force: true});
        cy.get('div.card-footer button.btn-normal-primary-with-shadow').eq(0).click();
        cy.wait('@update2').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(5000);

        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.get('a.dropdown-item.dusk-subscription').click();

        cy.get('#currency').select('HUF');
        cy.get('.sub-label').eq(1).should('contain', value3);
        cy.get('#currency').select('EUR');
        cy.get('.sub-label').eq(1).should('contain', value4);

        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.get('a.dropdown-item.dusk-profile').click();
        cy.get('div.content-body span.primary.pointer').eq(0).click();
        cy.get('div.vs__actions svg path').eq(1).click({force: true});
        cy.get('input.vs__search').eq(0).type(country4);
        cy.get('ul#vs1__listbox li#vs1__option-0').click({force: true});
        cy.get('div.card-footer button.btn-normal-primary-with-shadow').eq(0).click();
        cy.wait('@update2').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
    },

    /**
     * Profil törlése.
     * @param cy
     * @param btnText
     */
    deleteProfile(cy, btnText = 'Végleges törlés') {
        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.wait(3000);
        cy.get('a.dropdown-item.dusk-profile').click();
        cy.wait(3000);
        cy.get('.btn-warning-tertiary.m-auto').click();
        cy.wait(3000);
        cy.get('.btn-warning-2').contains(btnText).click();
        cy.wait(3000);
    },

    /**
     * Profil regisztrálása
     * @param cy
     * @param uName
     * @param fName
     * @param email
     * @param passw
     */
    register(cy, uName, fName, email, passw) {
        cy.get('#user_name').clear();
        cy.get('#user_name').type(uName);
        cy.get('#full_name').clear();
        cy.get('#full_name').type(fName);
        cy.get('#email').clear();
        cy.get('#email').type(email);
        cy.get('#password').clear();
        cy.get('#password').type(passw);
        cy.get('#password-confirm').clear();
        cy.get('#password-confirm').type(passw);
        cy.get('#reg_math').clear();
        cy.get('#reg_math[data-value]').then(($inp) => {
            const cls = $inp.attr('data-value');
            cy.wrap($inp);
            cy.get('#reg_math').type(cls);
        })
        cy.get('[type="checkbox"]#custom_agree').check({force: true});
        cy.get('div.card-body.login-card-body form').submit();
    },

    /**
     * Statisztikák menü részen megnézi, hogy az elemek helyesen látszódnak-e.
     * @param cy
     */
    statisticWoN(cy) {
        cy.get('button.btn-link').eq(1).click({force: true});
        cy.get('button.btn-normal-primary-with-shadow').eq(0).should('be.visible').should('not.be.disabled');
        cy.get('.btn-tertiary').should('be.visible').should('not.be.disabled').click();
        cy.get('.input-label > .pointer').click();
        cy.wait(1000);
        cy.get('.card-footer > .text-right > :nth-child(2)').eq(0).should('be.visible').should('not.be.disabled');
        cy.get('.card-footer > .text-right > .btn-normal-primary-with-shadow').should('be.visible').should('not.be.disabled').click({force: true});

        cy.get('ul#main-menu-navigation li a').eq(2).click({force: true});
        cy.wait('@filt').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(1000);
        cy.get('button.btn-link').eq(1).click({force: true});
        cy.get('button.btn-normal-primary-with-shadow').eq(0).should('be.visible').should('not.be.disabled');
        cy.get('.btn-tertiary').should('be.visible').should('not.be.disabled').click();

        cy.get('ul#main-menu-navigation li a').eq(3).click({force: true});
        cy.wait(1000);
        cy.get('button.btn-link').eq(1).click({force: true});
        cy.get('button.btn-normal-primary-with-shadow').eq(0).should('be.visible').should('not.be.disabled');
        cy.get('.btn-tertiary').should('be.visible').should('not.be.disabled').click();
    },

    /**
     * Beállításokon belül megnézi, hogy az elemek helyesen látszódnak-e.
     * @param cy
     */
    viewSettings(cy) {
        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.get('a.dropdown-item.dusk-profile').click();
        cy.wait(4000);
        cy.get('.btn-warning-tertiary.m-auto').click();
        cy.get('.btn-warning-2').should('be.visible').should('not.be.disabled');
        cy.get('.btn-tertiary-modal').eq(0).should('be.visible').should('not.be.disabled').click({force: true});

        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.get('a.dropdown-item.dusk-domain-settings').click({force: true});
        cy.get('.pointer.ico-delete-warning').eq(0).click();
        cy.get('.btn-warning-2').should('be.visible').should('not.be.disabled');
        cy.get('.btn-tertiary-modal').should('be.visible').eq(0).should('not.be.disabled').click({force: true});

        cy.get('span.ico-plus').click({force: true});
        cy.get('div.modal-footer.btn-pt button.btn-normal-primary-with-shadow').scrollIntoView();
        cy.get('div.modal-footer.btn-pt button.btn-normal-primary-with-shadow').should('be.visible').should('not.be.disabled');
        cy.get('.ico-x.small.close.pointer').click({force: true});

        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.get('a.dropdown-item.dusk-email-accounts').click();
        cy.wait(7000);
        cy.get('span.ico-plus').eq(0).click({force: true});
        cy.wait(1000);
        cy.get('button.btn-normal-primary-with-shadow-modal').scrollIntoView().should('be.visible').should('not.be.disabled');
        cy.get('.btn-tertiary-modal').should('be.visible').should('not.be.disabled').eq(0).click();
        cy.get('span.ico-plus').eq(1).click({force: true});
        cy.wait(1000);
        cy.get('button.btn-normal-primary-with-shadow-modal').eq(0).scrollIntoView().should('be.visible').should('not.be.disabled');
        cy.get('.btn-tertiary-modal').should('be.visible').should('not.be.disabled').eq(0).click();
        cy.get('span.ico-plus').eq(2).click({force: true});
        cy.wait(1000);
        cy.get('button.btn-normal-primary-with-shadow-modal').eq(0).scrollIntoView().should('be.visible').should('not.be.disabled');
        cy.get('.ico-x.small.close.pointer').scrollIntoView().should('be.visible').should('not.be.disabled').eq(0).click();
        cy.get('#navbarDropdown img').eq(0).click();
        cy.get('.dropdown-item.more-menu-item button.btn-link').eq(0).click({force: true});
        cy.wait(30000);
        cy.get('button.btn-normal-primary-with-shadow-modal').scrollIntoView().should('be.visible').should('not.be.disabled');
        cy.get('.btn-tertiary-modal').should('be.visible').should('not.be.disabled').eq(0).click();
        cy.wait(2000);

        cy.get('#navbarDropdown img').eq(0).click({force: true});
        cy.get('.dropdown-item.more-menu-item button.btn-link-warning').eq(0).click({force: true});
        cy.get('.btn-warning-2').scrollIntoView().should('be.visible').should('not.be.disabled');
        cy.get('.btn-tertiary-modal').eq(0).should('be.visible').should('not.be.disabled').click({force: true});

        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.get('a.dropdown-item.dusk-report-config').click();
        cy.wait(30000);
        cy.get('span.ico-plus').eq(0).click();
        cy.get('button.btn-normal-primary-with-shadow').scrollIntoView().should('be.visible').should('not.be.disabled');
        cy.get('.btn-tertiary').eq(0).should('be.visible').should('not.be.disabled').click();

        cy.get('span.pointer img.rectangle-gray').eq(0).click();
        cy.get('button.btn-normal-primary-with-shadow').scrollIntoView().should('be.visible').should('not.be.disabled');
        cy.get('.btn-tertiary').eq(0).should('be.visible').should('not.be.disabled').click();

        cy.get('span.pointer img.rectangle-gray').eq(1).click();
        cy.get('.btn-warning-2').should('be.visible').scrollIntoView().should('not.be.disabled');
        cy.get('.btn-tertiary-modal').eq(0).should('be.visible').should('not.be.disabled').click({force: true});

        cy.get('span.ico-plus').eq(1).click({force: true});
        cy.get('button.btn-normal-primary-with-shadow').scrollIntoView().should('be.visible').should('not.be.disabled');
        cy.get('.btn-tertiary').eq(0).should('be.visible').should('not.be.disabled').click();

        cy.get('span.ico-plus').eq(1).click({force: true});
        cy.get('button.btn-normal-primary-with-shadow').scrollIntoView().should('be.visible').should('not.be.disabled');
        cy.wait(2000).then(() => {
            cy.get('.btn-tertiary').eq(0).scrollIntoView().should('be.visible').should('not.be.disabled').click();
        });

        cy.get('table td span.pointer').eq(0).click();
        cy.get('button.btn-normal-primary-with-shadow').scrollIntoView().should('be.visible').should('not.be.disabled');
        cy.get('.btn-tertiary').eq(0).should('be.visible').should('not.be.disabled').click();

        cy.get('table td span.pointer').eq(1).click();
        cy.get('.btn-warning-2').scrollIntoView().should('be.visible').should('not.be.disabled');
        cy.get('.btn-tertiary-modal').eq(0).should('be.visible').should('not.be.disabled').click({force: true});

        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.get('a.dropdown-item').eq(4).click();
        cy.wait('@perm').should((xhr) => {
            expect(xhr.status, 'successful GET').to.equal(200)
        })
        cy.wait(2000);
        cy.get('button.btn-link').eq(0).click();
        cy.get('button.btn-normal-primary-with-shadow').scrollIntoView().should('be.visible').should('not.be.disabled');
        cy.get('.btn-tertiary').eq(0).should('be.visible').should('not.be.disabled').click();

        cy.get('span.ico-more-primary').eq(0).scrollIntoView().should('be.visible').should('not.be.disabled').click();
        cy.get('.btn-link').eq(0).should('be.visible').should('not.be.disabled').click();
        cy.get('button.btn-normal-primary-with-shadow').scrollIntoView().should('be.visible').should('not.be.disabled');
        cy.get('.btn-tertiary').eq(0).should('be.visible').should('not.be.disabled').click();

        cy.get('span.ico-more-primary').eq(0).should('be.visible').should('not.be.disabled').click();
        cy.get('.btn-link-warning').eq(0).scrollIntoView().should('be.visible').should('not.be.disabled').click();
        cy.get('.btn-warning-2').should('be.visible').should('not.be.disabled');
        cy.get('.btn-tertiary-modal').eq(0).should('be.visible').should('not.be.disabled').click({force: true});
        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.get('a.dropdown-item.dusk-filter').click();
        cy.wait('@getFilt').should((xhr) => {
            expect(xhr.status, 'successful GET').to.equal(200)
        })
        cy.wait(2000);
        cy.get('#btn-add').click({force: true});
        this.addNewFilter(cy);
        cy.wait(1000);

        cy.get('span.primary.pointer img').eq(0).click();
        this.addNewFilter(cy);
        cy.wait(1000);

        cy.get('span.danger.pointer img').eq(0).click();
        cy.get('.btn-warning-2').should('be.visible').should('not.be.disabled');
        cy.get('.btn-tertiary-modal').eq(0).should('be.visible').should('not.be.disabled').click({force: true});
    },

    /**
     * Új szűrő hozzáadásánál az elemek helyes láthatóságának ellenőrzése.
     * @param cy
     */
    addNewFilter(cy) {
        cy.get('span.add_new_filter-label').should('be.visible').click({force: true});
        cy.get('span.add_new_filter-label').scrollIntoView();
        cy.get('span.add_new_filter-label').should('be.visible').click({force: true});
        cy.get('span.add_new_filter-label').scrollIntoView();
        cy.get('span.add_new_filter-label').should('be.visible').click({force: true});
        cy.get('button.btn-normal-primary-with-shadow').eq(0).scrollIntoView();
        cy.get('button.btn-normal-primary-with-shadow').eq(0).should('be.visible').should('not.be.disabled');
        cy.get('.btn-tertiary').eq(0).should('be.visible').should('not.be.disabled').click({force: true});
    },

    /**
     * Levelezéseken belül elemek helyes láthatóságának ellenőrzése.
     * @param cy
     */
    viewThread(cy) {
        cy.get('ul li a.menu-item div span.menu-item').eq(0).click({force: true});
        cy.wait('@threads').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(3000);
        cy.get('#thread-list-scroll > :nth-child(3)').find('.flex-shrink-1.w-100.ml-0.mr-auto').eq(0).click({force: true});
        cy.wait('@email').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(2000);
        cy.get('button.btn-rounded-with-icon-2').eq(4).click();
        this.addNewFilter(cy);

        cy.get('ul li a.menu-item div span.menu-item').eq(1).click({force: true});
        cy.wait('@threads').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(3000);
        cy.get('#thread-list-scroll > :nth-child(3)').find('.flex-shrink-1.w-100.ml-0.mr-auto').eq(0).click({force: true});
        cy.wait('@email').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(2000);
        cy.get('button.btn-rounded-with-icon-2').eq(4).click();
        this.addNewFilter(cy);

        cy.get('ul li a.menu-item div span.menu-item').eq(2).click({force: true});
        cy.wait('@threads').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(3000);
        cy.get('#thread-list-scroll > :nth-child(3)').find('.flex-shrink-1.w-100.ml-0.mr-auto').eq(0).click({force: true});
        cy.wait('@email').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(2000);
        cy.get('button.btn-rounded-with-icon-2').eq(4).click();
        this.addNewFilter(cy);

        cy.get('ul li a.menu-item div span.menu-item').eq(3).click({force: true});
        cy.wait('@threads').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(3000);
        cy.get('#thread-list-scroll > :nth-child(3)').find('.flex-shrink-1.w-100.ml-0.mr-auto').eq(0).click({force: true});
        cy.wait('@email').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(2000);
        cy.get('button.btn-rounded-with-icon-2').eq(4).click();
        this.addNewFilter(cy);

        cy.get('ul li a.menu-item div span.menu-item').eq(4).click({force: true});
        cy.wait('@threads').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(3000);
        cy.get('#thread-list-scroll > :nth-child(3)').find('.flex-shrink-1.w-100.ml-0.mr-auto').eq(0).click({force: true});
        cy.wait('@email').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(2000);
        cy.get('button.btn-rounded-with-icon-2').eq(4).click();
        this.addNewFilter(cy);

        cy.get('ul li a.menu-item div span.menu-item').eq(5).click({force: true});
        cy.wait('@threads').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(3000);
        cy.get('#thread-list-scroll > :nth-child(3)').find('.flex-shrink-1.w-100.ml-0.mr-auto').eq(0).click({force: true});
        cy.wait('@email').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(2000);
        cy.get('button.btn-rounded-with-icon-2').eq(4).click();
        this.addNewFilter(cy);

        cy.get('ul li a.menu-item div span.menu-item').eq(6).click({force: true});
        cy.wait('@threads').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(3000);
        cy.get('#thread-list-scroll > :nth-child(3)').find('.flex-shrink-1.w-100.ml-0.mr-auto').eq(0).click({force: true});
        cy.wait('@email').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(2000);
        cy.get('button.btn-rounded-with-icon-2').eq(4).click();
        this.addNewFilter(cy);

        cy.get('ul li a.menu-item div span.menu-item').eq(7).click({force: true});
        cy.wait('@threads').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(3000);
        cy.get('#thread-list-scroll > :nth-child(3)').find('.flex-shrink-1.w-100.ml-0.mr-auto').eq(0).click({force: true});
        cy.wait('@email').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(2000);
        cy.get('button.btn-rounded-with-icon-2').eq(4).click();
        this.addNewFilter(cy);

        cy.get('ul li a.menu-item div span.menu-item').eq(8).click({force: true});
        cy.wait('@threads').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(3000);
        cy.get('#thread-list-scroll > :nth-child(3)').find('.flex-shrink-1.w-100.ml-0.mr-auto').eq(0).click({force: true});
        cy.wait('@email').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(2000);
        cy.get('button.btn-rounded-with-icon-2').eq(4).click();
        this.addNewFilter(cy);
    },

    /**
     * Regisztráció utáni Onboarding automatikus kitöltése, majd regisztráció befejezése.
     * @param cy
     * @param email
     */
    onboarding(cy, email) {
        cy.url().should('include', '/onboarding');
        cy.wait(2000);
        cy.get('#step1 > .card > .card-content > .card-body > :nth-child(1) > .step-wrapper > .wizard-item-container > .d-flex > :nth-child(1)').click();
        cy.wait(2000);
        cy.get('#step1 > .card > .card-content > .card-body > :nth-child(1) > .step-wrapper > .wizard-btn > .btn-normal-primary-with-shadow').click();
        cy.wait(2000);
        cy.get('.wizard-item-container > :nth-child(1) > .wizard-card-row').click();
        cy.wait(2000);
        cy.get('.card-body > :nth-child(1) > .wizard-btn > .btn-normal-primary-with-shadow').click();
        cy.wait(2000);
        cy.get('.flex-shrink-1 > .row > :nth-child(1) > .form-group > .form-control').type(email);
        cy.wait(2000);
        cy.get('#step3 > .card > .card-content > .card-body > :nth-child(1) > .step-wrapper > .wizard-btn > .btn-normal-primary-with-shadow').click();
        cy.wait(2000);
        cy.get('.flex-shrink-1 > .btn-normal-primary-with-shadow').click();
        cy.wait(2000);
        cy.get('.flex-shrink-1 > .btn-normal-primary-with-shadow').click();
        cy.wait(2000);
        cy.get('#step6 > .card > .card-content > .card-body > :nth-child(1) > .step-wrapper > .wizard-btn > .btn-normal-primary-with-shadow').click();
        cy.wait(2000);
        cy.get('.card-body > .wizard-btn > .btn-normal-primary-with-shadow').click();
    },

    onboardingWithErrors(cy, email) {
        cy.url().should('include', '/onboarding');
        cy.wait('@initData').should((xhr) => {
            expect(xhr.status, 'successful GET').to.equal(200);
        });
        cy.get('#step1 > .card > .card-content > .card-body > :nth-child(1) > .step-wrapper > .wizard-item-container > .d-flex > :nth-child(1)').eq(0).click();
        cy.wait(2000);
        cy.get('#step1 > .card > .card-content > .card-body > :nth-child(1) > .step-wrapper > .wizard-item-container > .wizard-item-details > .wizard-btn > .btn-normal-primary-with-shadow').click();
        cy.wait(2000);
        cy.get('.wizard-item-container > :nth-child(1) > .wizard-card-row').click();
        cy.wait(2000);
        cy.get('.col-md-10 > .form-group > .form-control').type(email);
        cy.wait(2000);
        cy.get('#step3 > .card > .card-content > .card-body > :nth-child(1) > .step-wrapper > .wizard-btn > .btn-normal-primary-with-shadow').click();
        cy.wait(2000);
        cy.get('.d-flex > :nth-child(1) > .btn-normal-primary-with-shadow').click();
        cy.wait(2000);
        cy.get('#step5 > .card > .card-content > .card-body > :nth-child(1) > .step-wrapper > .wizard-item-container > .wizard-btn > .btn-normal-primary-with-shadow').click();
        cy.wait(2000);
        cy.get('.wizard-btn > .btn-link').click();
        cy.wait(2000);
        cy.get('.card-body > .wizard-btn > .btn-normal-primary-with-shadow').click();
    }

}