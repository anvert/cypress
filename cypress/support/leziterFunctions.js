function separator(value) {
    let array = value.split(' ');
    array.shift();
    return array.join(' ');
}

module.exports = {

    /**
     * Teszteli, hogy a paraméterben átadott url post folyamat sikeres volt-e, illetve megvárja annak betöltését.
     * @param cy
     * @param url - post url
     * @param name - tetszőleges elnevezése
     */
    checksSuccessfulPost(cy, url, name, extraTimeWaiting = 0) {
        cy.intercept({
            method: 'POST',
            url: url,
        }).as(name);
        cy.wait(extraTimeWaiting);
        cy.wait('@' + name).its('response.statusCode').should('eq', 200);
    },

    /**
     * Bejelentkezik
     * @param cy
     * @param psw jelszó
     * @param email e-mail cím
     * @param postUrl bejelentkezés utáni post url betöltésének megvárása (boss-ra alapértelmezett)
     */
    login(cy, psw, email, postUrl = '/dashboard/get-email-traffic', extraTimeWaiting = 0) {
        const urlName = Cypress.env('app_url');

        cy.viewport(1920, 1080);
        cy.visit(urlName);

        cy.wait(3000).then(() => {
            cy.url().should('include', '/login');
        });

        cy.get('#email').clear();
        cy.get('#email').type(email);
        cy.get('#password').clear();
        cy.get('#password').type(psw);

        if (postUrl !== null) {
            cy.get('form').eq(0).submit().then(() => {
                this.checksSuccessfulPost(cy, postUrl, 'homepage', extraTimeWaiting);
            });
        } else {
            cy.get('form').eq(0).submit();
        }
    },

    /**
     * Ha nincs delegálatlan levél, akkor lefut a függvény és automatikusan készít egyet.
     * @param cy
     * @param exists van-e levél a Delegálatlan boxban (true = igen, false = nem)
     */
    createsUndelegatedLetter(cy, exists = false) {
        if (!exists) {
            cy.log('__Letter undelegation is processing...__');
            cy.get('.card.rounded-xxl').eq(1).within(() => {
                cy.get('#navbarDropdown').first().click();
                cy.get('.dropdown-content').find('.dropdown').eq(1).click();
                cy.get('.dropdown-content').find('.dropdown').eq(0).click();
                cy.get('.dropdown-content').find('.dropdown').eq(0).click();
                cy.get('.btn-small-assign').contains('Mentés').click();
            });
        }
    },

    /**
     * A legelső Delegálatlan levelet delegálja rá a listában a legelső személyre. Ezután megnézi, hogy
     * a Delegált, vagy az Elakadt levelezés boxban van-e. Ezt cy.log-ban ki is írja.
     * @param cy
     */
    delegates(cy) {
        cy.get('.card.rounded-xxl').first().within(() => {
            // eltárolja alias-ban az adott tárgyú levelet
            cy.get('.subject-truncate.pointer').first().find('span').eq(0).invoke('text').as('text');
        });

        // elvégzi a delegálást
        cy.get('.ico-assign-gray').first().click();
        cy.get('.dropdown-content').find('.dropdown').first().click();
        cy.get('.dropdown-content').find('.form-control').eq(0).type('Generated text');
        cy.get('.btn-small-assign').contains('Mentés').click();

        /*// megnézi, hogy a Delegáltak vagy az Elakadtak között van-e
        cy.get('@text').then(value => {
            let title = separator(value);
            cy.contains('.card.rounded-xxl', title).within(() => {
                cy.get('.card-titles').invoke('text').as('cardTitle');
            });
            cy.get('@cardTitle').then(value => {
                // FIXME: Nem Delegált értéke van a value-nak
                if (value.trim() === 'Delegált') {
                    cy.log('__This letter is in the delegated section.__');
                }
                if (value.trim() === 'Elakadt levelezés') {
                    cy.log('__This letter is in the stucked section.__');
                }
                cy.log(value);
            });
        });*/
    },

    /**
     * Ellenőrzi, hogy a fent lévő 3 box milyen szám értékkel rendelkezik. A vele megegyező listában megnézi, hogy
     * van-e abban a kategóriában (delegált/ delegálatlan/ elakadt) '.empty-thread-list-label' class-ú elem.<br>
     * Aminél átmegy a teszt:<br>
     * - <u>Ha 0 és VAN benne a '.empty-thread-list-label' class-ú elem</u>, tehát tényleg nincs egy
     * levél sem az adott kategóriában.<br>
     * - <u>Ha NEM 0 és NINCS benne a '.empty-thread-list-label' class-ú elem</u>, tehát tényleg van
     * levél az adott kategóriában.<br>
     * Aminél Assertion error léphet fel:<br>
     * - <u>Ha 0 és NINCS a '.empty-thread-list-label' class-ú elem</u>, tehát 0-t számolt, de mégis van benne levél.<br>
     * - <u>Ha NEM 0 és VAN benne a '.empty-thread-list-label' class-ú elem</u>, tehát NEM 0-t számolt, de mégsincs
     * benne levél.<br>
     * @param cy
     */
    checksNumbersOfDelegationBoxes(cy) {

        cy.get('.main-number').eq(0).invoke('text').as('undelegatedNumber');
        cy.get('@undelegatedNumber').then(value => {
            if (value === 0 || value === '0') {
                cy.get('.card.rounded-xxl').eq(0).within(() => {
                    cy.get('.empty-thread-list-label').should('exist');
                    cy.log('There is no undelegated letter in the list ___✓___');
                });
            } else {
                cy.get('.card.rounded-xxl').eq(0).within(() => {
                    cy.get('.empty-thread-list-label').should('not.exist');
                    cy.log('There are some undelegated letters in the list ___✓___');
                });
            }
        });

        cy.get('.main-number').eq(1).invoke('text').as('delegatedNumber');
        cy.get('@delegatedNumber').then(value => {
            if (value === 0 || value === '0') {
                cy.get('.card.rounded-xxl').eq(2).within(() => {
                    cy.get('.empty-thread-list-label').should('exist');
                    cy.log('There is no delegated letter in the list ___✓___');
                });
            } else {
                cy.get('.card.rounded-xxl').eq(2).within(() => {
                    cy.get('.empty-thread-list-label').should('not.exist');
                    cy.log('There are some delegated letters in the list ___✓___');
                });
            }
        });

        cy.get('.main-number').eq(2).invoke('text').as('stuckedNumber');
        cy.get('@stuckedNumber').then(value => {
            if (value === 0 || value === '0') {
                cy.get('.card.rounded-xxl').eq(1).within(() => {
                    cy.get('.empty-thread-list-label').should('exist');
                    cy.log('There is no stucked letter in the list ___✓___');
                });
            } else {
                cy.get('.card.rounded-xxl').eq(1).within(() => {
                    cy.get('.empty-thread-list-label').should('not.exist');
                    cy.log('There are some stucked letters in the list ___✓___');
                });
            }
        });
    },

    /**
     * Levélírásnál az összes paramétert kitölti
     * @param cy
     * @param sendTo címzett
     * @param sendEmail küldje el a megírt e-mailt, vagy ne (true = küldje, false = mentse piszkozatként)
     */
    compose(cy, sendTo, sendEmail) {
        cy.contains('Levélírás').click();
        cy.get('.ti-new-tag-input-wrapper > #input-small').clear();
        cy.get('.ti-new-tag-input-wrapper > #input-small').type(sendTo);
        cy.get(':nth-child(1) > .form-group > .form-control').clear();
        cy.wait(2000);
        cy.get(':nth-child(1) > .form-group > .form-control').type('Cypress_test_subject');
        cy.wait(2000);

        cy.get('.ProseMirror').type('This is a text from Cypress.{selectAll}', {force: true});
        // Kövér betűstílus beállítása
        cy.get('.editor-menubar > :nth-child(1)').click();

        if (sendEmail) {
            this.sendEmail(cy);
        }
        if (!sendEmail) {
            this.makeDraft(cy);
        }
    },

    /**
     * Elküldi a levelet, és megvizsgálja sikeres volt-e a művelet
     * @param cy
     */
    sendEmail(cy) {
        cy.get('.btn-normal-send-primary.mt-1.ml-1').click();

        cy.intercept({
            method: 'POST',
            url: '/anvert-sent-emails/send-email',
        }).as('emailSent');

        cy.wait('@emailSent').its('response.statusCode').should('eq', 200);
    },

    makeDraft(cy) {
        cy.get('.btn-tertiary-send-modal.mt-1').click();
        this.checksSuccessfulPost(cy, '/threads/thread-emails-after-draft-action', 'draftSaved');
    },

    tryingComposingFunctions(cy) {
        /*cy.contains('Levélírás').click();
        cy.get('.ti-new-tag-input-wrapper > #input-small').clear();
        cy.get('.ti-new-tag-input-wrapper > #input-small').type('demobartaemese@outlook.com');
        cy.get(':nth-child(1) > .form-group > .form-control').clear();
        cy.get(':nth-child(1) > .form-group > .form-control').type('Cypress_test_subject');

        cy.get('.editor-menubar > :nth-child(1)').click();
        cy.get('.ProseMirror').type('{upArrow}This is a text in BOLD style.{enter}', {force: true});
        cy.get('.editor-menubar > :nth-child(1)').click();

        cy.get('.editor-menubar > :nth-child(2)').click();
        cy.get('.ProseMirror').type('{upArrow}This is a text in ITALIC style.{enter}', {force: true});
        cy.get('.editor-menubar > :nth-child(2)').click();

        cy.get('.ProseMirror').type('This is a text in ITALIC style.{home}{shift}{end}', {force: true});
        // Dőlt betűstílus beállítása
        cy.get('.editor-menubar > :nth-child(2)').click();

        // az első p szöveg kinyerése és ellenőrzése, hogy bold stílusú-e
        cy.get('.ProseMirror').first().invoke('text').as('testMessage1');
        cy.get('@testMessage1').then( value => {
            cy.get('.ProseMirror').find('strong').contains(value);
        });
        cy.get('.editor-menubar > :nth-child(1)').click();
        cy.get('@testMessage1').then( value => {
            cy.get('.ProseMirror').contains('strong', value).should('not.exist');
        });*/
    },

    /**
     * Megnézi, hogy a jogosultságok listájában szerepel-e az 'Ügyintéző'.
     * @param cy
     */
    visibleInList(cy) {
        cy.get('.dropdown-toggle').click();
        cy.get('[href="https://anvert.local/permissions"]').click();
        cy.get('.col-md-5 > .btn-link').click();
        cy.get('.form-group > .v-select').click();
        cy.get('.vs__dropdown-menu').within(() => {
            cy.contains('Ügyintéző');
        });
    },

    /**
     * Megvizsgálja az e-maileket az "Összes"-ben, hogy valóban csak az ügyintézőnek szántak, illetve a központi
     * levelek láthatóak-e csak neki.
     * @param cy
     * @param centralEmail - központi e-mail címe
     * @param centralUsernames - központi e-mail cím régi és új felhasználónevei tömbben
     * @param email - a jelenleg bejelentkezett ügyintéző e-mail címe
     * @param username - a jelenleg bejelentkezett ügyintéző felhasználóneve
     */
    checkAllEmailsForEmployee(cy, centralEmail, centralUsernames, email, username) {
        cy.wait(2000).then(() => {
            cy.get('.menu-item').contains('Összes').click();
        });

        cy.get('[data-v-08377e28=""][data-v-eb802ace=""]').find('.list-bg.list-overflow').then(counts => {
            const quantity = Cypress.$(counts).length;
            cy.log('___There are ' + quantity + ' letters in the folder.___');

            // ez minden egyes levélre megcsinálja
            for (let i = 0; i < quantity; i++) {
                cy.get('.list-bg.list-overflow').eq(i).within(() => {
                    cy.get('.flex-shrink-1.w-100.ml-0.mr-auto').click({force: true});
                });
                this.emailIsForEmployee(cy, centralEmail, centralUsernames, email, username);
                cy.get('.ico-back').click();
            }
        });
    },

    /**
     * Megvizsgálja egy levélről, hogy valóban csak az ügyintéző, illetve a központi e-mail a címzett.
     * @param cy
     * @param centralEmail - központi e-mail címe
     * @param centralUsernames - központi e-mail cím régi és új felhasználónevei tömbben
     * @param email - a jelenleg bejelentkezett ügyintéző e-mail címe
     * @param username - a jelenleg bejelentkezett ügyintéző felhasználóneve
     */
    emailIsForEmployee(cy, centralEmail, centralUsernames, email, username) {
        let centralOrEmployee = false;

        cy.get('.email-recipients.text-overflow.m-auto.m-r-12').then(value => {
            const addresseeQuantity = Cypress.$(value).length;

            for (let i = 0; i < addresseeQuantity; i++) {
                for (const centralUsername of centralUsernames) {
                    if (value.eq(i).text().includes('Címzett(ek): ' + centralUsername)) {
                        centralOrEmployee = true;
                        break;
                    }
                }

                if (value.eq(i).text().includes(centralEmail)) {
                    centralOrEmployee = true;
                } else if (value.eq(i).text().includes(email) || value.text().includes(username)) {
                    centralOrEmployee = true;
                }
            }

            expect(centralOrEmployee).to.equal(true, 'The employee sees his e-mails only');
        });
    },

    /**
     * Megvizsgálja az e-maileket az “Összes”-ben <u>keresésnél</u>, hogy valóban csak az ügyintézőnek szántak, illetve
     * a központi levelek láthatóak-e csak neki.
     * @param cy
     * @param textToSearchFor - keresendő szöveg
     */
    checksSearchbar(cy, textToSearchFor) {
        cy.wait(2000).then(() => {
            cy.get('.menu-item').contains('Összes').click();
        });

        this.checksSuccessfulPost(cy, '/threads/get-recommended-threads', 'loadedAll');
        cy.get('.hidden-xs > :nth-child(1) > :nth-child(1) > :nth-child(1) > .form-group > .form-control')
            .type(textToSearchFor + '{enter}');

        const newAndOldcentralUsernames = ['demofiokk', 'outlookinfo', 'info'];
        this.checkAllEmailsForEmployee(
            cy,
            'demofiokk@outlook.com',
            newAndOldcentralUsernames,
            'demokerepespeter@outlook.com',
            'demokerepespeter'
        );
    },

    /**
     * Létezik a Levélírás komponens, illetve megnyitja azt.
     * @param cy
     */
    opensLetterWriting(cy) {
        cy.get('.main-menu-content').contains('Levélírás').click();
        cy.get('.modal.fullscreen').should('exist');
    },

    /**
     * Default aláírás automatikus betöltésének ellenőrzése
     * @param cy
     */
    async analyzesDefaultSignature(cy) {
        cy.get('.dropdown-toggle').click();
        cy.get('.dusk-profile').click();
        cy.get(':nth-child(3) > .card-content > .card-header').click();
        cy.wait(2000);
        const TITLE_OF_SIGNATURE = await this.getsTextOfElement('[style=""] > .card-body');
        if (!(TITLE_OF_SIGNATURE.toString().trim().includes('Alapértelmezett'))) {
            this.createsDefaultSignature(cy, 'Default signature from Cypress');
        }

        await cy.wait(2000);
        await cy.get('.table > tbody > tr').each((el, index) => {
            if (el.text().includes('Alapértelmezett')) {
                cy.get('.table > tbody > tr').eq(index).within(() => {
                    cy.get('.pointer.has-tooltip').first().click();
                });
                cy.get('#signature').then(signature => {
                    let defSignature = signature.text();
                    cy.get('.ico-x').click();
                    this.opensLetterWriting(cy);
                    cy.get('#signature').should('exist');
                    cy.get('em > span').contains(defSignature).then(() => {
                        let signatureExists = true;
                        expect(signatureExists).to.equal(true, 'The Default signature works correctly');
                    });
                    cy.get('.ico-x').click();
                });
            }
        });
    },

    /**
     * Automatikusan létrehoz egy Alapértelmezett aláírást.
     * @param cy
     * @param signatureTitle - aláírás címe
     */
    createsDefaultSignature(cy, signatureTitle) {
        cy.wait(2000);
        cy.get('.user-name').then(username => {
            cy.get('div > .btn-link').click();
            cy.get('.col-sm-12 > .form-group > .form-control').type(signatureTitle);
            cy.wait(2000);
            cy.get('.ProseMirror').type(username.text() + '{selectAll}', {force: true});
        });
        cy.get('.ico-italic').click();
        cy.get('.ico-text-color').click();
        cy.get('[style="background-color: rgb(0, 0, 255);"]').click();
        cy.get('.btn-normal-primary-with-shadow.ml-sm-1').first().click();
        cy.wait(2000);
        cy.get('.table > tbody > tr').each((el, index) => {
            if (el.text().includes(signatureTitle)) {
                cy.get('.table > tbody > tr').eq(index).within(() => {
                    cy.get('.btn-link').click();
                });
            }
        });
    },

    /**
     * Lekéri adott elemnek a szövegét.
     * @param elementID elem azonosítója
     * @returns {Bluebird<unknown>} elem szövege
     */
    async getsTextOfElement(elementID) {
        return new Cypress.Promise((resolve) => {
            cy.get(elementID)
                .invoke('text')
                .then((txt) => resolve(txt.toString()))
        });
    },

    /**
     * Lekéri adott elem(ek) számát.
     * @param elementID elem azonosítója
     * @returns {Bluebird<unknown>}
     */
    async getsNumberOfElement(elementID) {
        return new Cypress.Promise((resolve) => {
            cy.get(elementID).then(elem => {
                resolve(Cypress.$(elem).length)
            });
        });
    },

    /**
     * Létrehoz egy aláírást.
     * @param cy
     * @param signatureTitle aláírás címe
     * @returns {Promise<void>}
     */
    async createsSignature(cy, signatureTitle) {
        await cy.wait(2000);
        cy.get('.dropdown-toggle').click();
        cy.get('.dusk-profile').click();
        cy.get(':nth-child(3) > .card-content > .card-header').click();

        await cy.wait(2000);
        const username = await this.getsTextOfElement('.user-name');

        cy.get('div > .btn-link').click();
        await cy.get('.col-sm-12 > .form-group > .form-control').type(signatureTitle);
        await cy.wait(2000);
        await cy.get('.ProseMirror').type(username + '{selectAll}', {force: true});
        cy.get('.ico-italic').click();
        cy.get('.ico-text-color').click();
        cy.get('[style="background-color: rgb(44, 101, 22);"]').click();
        await cy.get('.btn-normal-primary-with-shadow.ml-sm-1').first().click();
    },

    /**
     * Ellenőrzi, hogy 1-nél van-e több aláírás. Ha nincs, akkor készít egyet.
     * @param cy
     * @returns {Promise<void>}
     */
    async checksMoreSignature(cy) {
        cy.get('.dropdown-toggle').click();
        cy.get('.dusk-profile').click();
        cy.get(':nth-child(3) > .card-content > .card-header').click();
        cy.wait(2000);

        // counts the <tr> -s
        const counts = await this.getsNumberOfElement('.table > tbody > tr');

        const NUMBER_OF_USELESS_TR_TAGS = 1;
        if (counts <= 1 + NUMBER_OF_USELESS_TR_TAGS) {
            await this.createsSignature(cy, 'Another signature from Cypress');
        }

        await this.opensLetterWriting(cy);
        await this.switchesSignatures(cy,
            'Another signature from Cypress',
            'Kerepes Péter',
            'color: rgb(44, 101, 22)',
            'Kerepes Péter',
            'color: rgb(0, 0, 255)');
    },

    /**
     * Levélíráson belüli aláírások váltogatásának tesztelése.
     * @param cy
     * @param switchTo átváltani kívánt aláírás címe
     * @param switchedSignature átváltott aláírás
     * @param switchedSignatureStyle átváltott aláírás stílusa
     * @param defaultSignature alapértelmezett aláírás
     * @param defaultSignatureStyle alapértelmezett aláírás stílusa
     * @returns {Promise<void>}
     */
    async switchesSignatures(cy, switchTo, switchedSignature, switchedSignatureStyle, defaultSignature, defaultSignatureStyle) {
        const DEFAULT_SIGNATURE = await this.getsTextOfElement('em > span');
        expect(defaultSignature).to.equal(DEFAULT_SIGNATURE, 'The default signature appears correctly');
        await cy.get('em > span').invoke('attr', 'style').should('eq', defaultSignatureStyle);

        cy.get('.pull-left > :nth-child(2)').click();
        await cy.get('.signature_options').each((val) => {
            if (val.text().trim() === switchTo) {
                val.click();
            }
        });

        const SWITCHED_SIGNATURE = await this.getsTextOfElement('em > span');
        expect(switchedSignature).to.equal(SWITCHED_SIGNATURE, 'The switched signature appears correctly');
        await cy.get('em > span').invoke('attr', 'style').should('eq', switchedSignatureStyle);
    },

    /**
     * "Megadható legyen paraméterben (címzettek listája, tárgy szövege, levél szöveg)" ellenőrzése
     * @param cy
     */
    checksParameters(cy) {
        this.opensLetterWriting(cy);
        cy.get('#emailOptions').should('exist');
        cy.get('.ti-new-tag-input-wrapper > #input-small').should('exist');
        cy.get('.editor__content').should('exist');
    },

    /**
     * "Lehessen választani, hogy a csoport központi e-mailről, vagy a saját e-mail fiókjáról küldi a levelet."
     * @param cy
     * @param centralEmail - központi e-mail cím
     * @param employeeEmail - ügyintéző e-mail címe
     */
    checksSenderOptions(cy, centralEmail, employeeEmail) {
        cy.get('#emailOptions').click();
        cy.get('#emailOptions').within(() => {
            cy.contains(centralEmail);
            cy.contains(employeeEmail);
        });
    },

    /**
     * Amikor megnyomja a válasz gombot, akkor ellenőrzi, hogy nincs-e már válasz e-mail küldés folyamatban. Ha van akkor dob egy figyelmeztetést, hogy ezzel már foglalkozik XY.
     * @param cy
     */
    alertAppears(cy) {
        cy.wait(3000).then(() => {
            cy.get('.menu-item').contains('Összes').click();
        });

        cy.get('[data-v-08377e28=""][data-v-eb802ace=""] > .list-bg.list-overflow').each((val, index) => {
            cy.get('.flex-shrink-1.w-100.ml-0.mr-auto').eq(index).click({force: true});
            this.alertAppearsForOneLetter(cy, 'Kerepes Péter');
            cy.get('.ico-back').click();
        });
    },

    /**
     * Egy levélre megvizsgálja, hogy a figyelmeztető doboz megjelenik-e.<br>
     * Működése: ha a jelenleg bejelentkezett felhasználó neve (fullName) NEM egyezik meg a levélben esetlegesen megjelent
     * legfölső megkezdett levél szerző nevével, akkor fel kell dobnia az oldalnak a figyelmeztető ablakot.
     * @param cy
     * @param fullName felhasználó neve (jobb fölső sarokban ami látható)
     */
    alertAppearsForOneLetter(cy, fullName) {
        let alertAppears = false;

        cy.wait(1500).then(() => {
            cy.get('#thread-email-list > :nth-child(2)').then(text => {
                if (text.text().includes('Piszkozat')) {
                    cy.get('.card-header.thread-card-header.thread-email').eq(0).find('.email-from-name.text-overflow')
                        .then(emailFrom => {
                            cy.get('.ico-reply-gray').first().click();
                            if (emailFrom.text().trim() === fullName) {
                                cy.get('.modal-container').should('not.exist');
                            } else {
                                cy.get('.modal-container').should('exist');
                                alertAppears = true;
                                expect(alertAppears).to.equal(true, 'This letter has been started replying');
                            }
                            cy.get('.ico-x').click();
                        });
                } else {
                    expect(alertAppears).to.equal(false, 'This letter has not been started replying');
                }
            });
        });
    },

    /**
     * Feltölt egy fájlt, és megvizsgálja hogy szerepel-e a lenti listában.
     * @param cy
     */
    attachesAndDeletesFiles(cy) {
        cy.get('#upload-file-input').attachFile('/leziterFiles/cat2.jpg').then(() => {
            this.checksSuccessfulPost(cy, '/anvert-sent-emails/upload-file', 'successfulUpload');
            this.checksSuccessfulPost(cy, '/anvert-sent-emails/create-temp-attachment', 'createdTemp');
        });

        cy.get('.attachment-row').then(val => {
            let fileExists = false;
            if (val.text().includes('cat2.jpg')) {
                fileExists = true;
            }
            expect(fileExists).to.equal(true, 'The file appears correctly');
        }).then(() => {
            this.deletesAllAttachments(cy);
        }).then(() => {
            this.deletesAttachment(cy);
        }).then(() => {
            this.deletesAllAttachments(cy);
        }).then(() => {
            this.checksLimitOfAttachments(cy);
        }).then(() => {
            this.deletesAllAttachments(cy);
        });
    },

    /**
     * Előre megadott csatolmány listából törli az elsőt, és megvizsgálja sikeres volt-e a művelet.
     * @param cy
     */
    deletesAttachment(cy) {
        cy.get('#upload-file-input').attachFile('/leziterFiles/20db/1')
            .then(() => {
                cy.wait(2000);
            }).then(() => {
            for (let i = 1; i <= 4; i++) {
                cy.get('#upload-file-input').attachFile('/leziterFiles/20db/' + i).then(() => {
                    cy.wait(2000);
                });
            }
        });

        cy.get(':nth-child(1) > .ico-x-gray').click()
            .then(() => {
                cy.wait(2000);
            })
            .then(() => {
                cy.get('.attachment-row > .item')
                    .then(itemVal => {
                        cy.wait(2000).then(() => {
                            const quantityOfItems = Cypress.$(itemVal).length;
                            expect(quantityOfItems).to.equal(4, 'Attachment deleted succesfully');
                        });
                    });
            });

    },

    /**
     * Törli az összes feltöltött csatolmányt.
     * @param cy
     */
    deletesAllAttachments(cy) {
        cy.get('.attachment-row > .item').each(() => {
            cy.get('.attachment-row > .item').eq(0).find('.ico-x-gray').click().then(() => {
                cy.wait(2000)
            });
        }).then(() => {
            cy.get('.attachment-row').then(val => {
                let fileNotExists = false;
                if (!(val.hasClass('item'))) {
                    fileNotExists = true;
                }
                expect(fileNotExists).to.equal(true, 'The files have been deleted correctly');
            });
        });
    },

    /**
     * Ellenőrzi a feltöltendő fájlok mennyiségét.
     * @param cy
     */
    checksLimitOfAttachments(cy) {
        for (let i = 1; i <= 21; i++) {
            cy.get('#upload-file-input').attachFile('/leziterFiles/20db/' + i).then(() => {
                cy.wait(2000);
            });
        }
        cy.get('.file-alert').should('exist');
    },

    /**
     * Ellenőrzi a feltöltendő fájl méretét.
     * @param cy
     */
    checkSizeOfAttachments(cy) {
        const fileSize = 24;
        cy.get('#upload-file-input').attachFile('/leziterFiles/cats_against_logic.mp4').then(() => {
            if (fileSize >= 20) {
                cy.get('.file-alert').should('exist');
            } else {
                this.checksSuccessfulPost(cy, '/anvert-sent-emails/upload-file', 'successfulUpload');
                this.checksSuccessfulPost(cy, '/anvert-sent-emails/create-temp-attachment', 'createdTemp');
                cy.get('.file-alert').should('not.exist');
            }
        });
    },

    /**
     * Megnyit egy levelezést.
     * @param cy
     * @param textToSearchFor megnyitni kívánt levél
     */
    async openLetter(cy, textToSearchFor) {
        await cy.wait(3000);
        cy.get('.menu-item').contains('Összes').click();

        await cy.wait(4000);
        await cy.get('.hidden-xs > :nth-child(1) > :nth-child(1) > :nth-child(1) > .form-group > .form-control')
            .type(textToSearchFor + '{enter}');

        await cy.wait(2000);
        await cy.get('[data-v-08377e28=""][data-v-eb802ace=""] > .list-bg.list-overflow').first().within(() => {
            cy.get('.flex-shrink-1.w-100.ml-0.mr-auto').click({force: true});
        });
        await this.checkAttachmentLink(cy);
    },

    async checkAttachmentLink(cy) {
        await cy.get('.attachment.float-left.pointer').should('exist');
        await cy.get('.attachment.float-left.pointer').eq(0).click();
        // TODO: megvizsgálni, hogy működik-e a csatolmány link megnyitása
    }
}