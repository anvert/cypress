import actionLez from "./leziterFunctions";

const loginEmail = Cypress.env('login_email');
const loginPsw = Cypress.env('login_password');

const EMAILS = new Map([
    ["Béla", 'bela@test.com'],
    ["Ábel", 'abel@test.com'],
    ["Kata", 'kata@test.com'],
    ["Ági", 'agi@test.com'],
    ["Emma", 'emma@test.com'],
    ["János", 'janos@test.com'],
    ["Sámuel", 'samuel@test.com'],
    ["László", 'laszlo@test.com'],
    ["Virág", 'virag@test.com'],
    ["Mónika", 'monika@test.com'],
    ["András", 'andras@test.com'],
    ["Dorina", 'dorina@test.com']
]);
const DOMAINS = ["gmail.com", 'outlook.com', 'rackhost.hu', 'citromail.com', 'indamail.hu', 'yahoo.com', 'yandex.com'];

module.exports = {
    /**
     * Új partner felvétele opció tesztelése
     * @param cy
     */
    checkAddingNewPartner(cy) {
        cy.get('.ml-auto > .btn-link').click().then(() => {
            cy.get('.modal-container').should('exist');
            cy.get('#partner_name').should('exist');
            cy.get('#company_nickname').should('exist');
            cy.get('#partner_zip').should('exist');
            cy.get('#partner_city').should('exist');
            cy.get('#country').should('exist');
            cy.get('#partner_address').should('exist');
            cy.get('#vat_number').should('exist');
            // ezt a weboldalon majd javítani kell:
            cy.get(':nth-child(1) > :nth-child(1) > .form-group > .input-label').contains('Név *');
        }).then(() => {
            /**
             * Működik-e az "x" bezárás
             */
            cy.get('.ico-x').click();
            cy.get('.modal-container').should('not.exist');
        }).then(() => {
            /**
             * Vizsgálás, hogy a név kitöltése nélkül menti-e
             */
            cy.get('.ml-auto > .btn-link').click();
            cy.get('.btn-normal-primary-with-shadow').click();
            cy.get('.requirements').contains('A(z) Név mező kitöltése kötelező.');
            cy.get('#partner_name').type('Test Name');
            cy.get('.btn-normal-primary-with-shadow').click();
            cy.wait(1500);
            cy.get('tbody').contains('Test Name');
        }).then(() => {
            /**
             * Vizsgálás, hogy ugyanazzal a névvel menthető-e mégegyszer
             */
            cy.wait(4000);
            cy.get('.ml-auto > .btn-link').click();
            cy.get('#partner_name').type('Test Name');
            cy.get('.btn-normal-primary-with-shadow').click();
            cy.wait(2000);
            cy.get('.awn-toast-wrapper').should('exist');
            cy.get('.awn-toast-wrapper').contains('Ez a partner már létezik!');
            cy.wait(1000);
            cy.get('.btn-tertiary').click();
        }).then(() => {
            /**
             * Új partner hozzáadása minden mező kitöltésével
             */
            this.addNewCustomPartner(cy,
                'With Datas', 'testUsername',
                '6700', 'Szeged', 'Magyarország', 'Moszkvai körút 23.', '123456789');
        });
    },

    /**
     * Összes partner eltávolítása
     * @param cy
     */
    removePartners(cy) {
        cy.get(':nth-child(1) > .tab-list > .table > tbody').should('exist');
        cy.get(':nth-child(1) > .tab-list > .table > tbody > tr').each(() => {
            cy.get('.rectangle-gray').eq(1).click();
            cy.get('.btn-warning-2').click();
            cy.wait(3000);
        });
    },

    /**
     * Új partnert ad hozzá.
     * @param cy
     * @param partnerName Név
     * @param nickname Partner felhasználóneve
     * @param zip Irányítószám
     * @param city Város
     * @param country Ország
     * @param address Cím
     * @param vatNumber Adószám
     */
    addNewCustomPartner(cy, partnerName, nickname, zip, city, country, address, vatNumber) {
        cy.get('.ml-auto > .btn-link').click().then(() => {
            cy.get('#partner_name').type(partnerName);
            cy.get('#company_nickname').type(nickname);
            cy.get('#partner_zip').type(zip);
            cy.get('#partner_city').type(city);
            cy.get('#country').type(country);
            cy.wait(1000);
            cy.get('.vs__dropdown-option.vs__dropdown-option--highlight').click();
            cy.get('#partner_address').type(address);
            cy.get('#vat_number').type(vatNumber);
            cy.get('.btn-normal-primary-with-shadow').click();
            cy.wait(2000);
            cy.get('.awn-toast-wrapper').should('exist');
            cy.get('.awn-toast-wrapper').contains('Sikeres mentés');
            cy.wait(1500);
            cy.get('tbody').contains(partnerName);
        });
    },

    addNewPartnerWithOnlyPartnerName(cy, partnerName) {
        cy.get('.ml-auto > .btn-link').click().then(() => {
            cy.get('#partner_name').type(partnerName);
            cy.get('#company_nickname').type(partnerName + '_company_nickname');
            cy.get('#partner_zip').type(partnerName + '_partner_zip');
            cy.get('#partner_city').type(partnerName + '_partner_city');
            cy.get('#country').type('Magyarország');
            cy.wait(1000);
            cy.get('.vs__dropdown-option.vs__dropdown-option--highlight').click();
            cy.get('#partner_address').type(partnerName + '_partner_address');
            cy.get('#vat_number').type(partnerName + '_vat_number');
            cy.get('.btn-normal-primary-with-shadow').click();
            cy.wait(2000);
            cy.get('.awn-toast-wrapper').should('exist');
            cy.get('.awn-toast-wrapper').contains('Sikeres mentés');
            cy.wait(1500);
            cy.get('tbody').contains(partnerName);
        });
    },

    /**
     * 0 és az {@link #EMAILS} mérete között generál egy random számot, és annyi (random kiválasztott) email-t ad hozzá
     * a felhasználóhoz a tömbből.
     * @param cy
     * @param cardIndex hanyadik a felhasználó felülről (0-tól indulva)
     */
    addEmailToPartner(cy, cardIndex) {
        let howMany = Math.floor(Math.random() * EMAILS.size);

        for (let i = 0; i < howMany; i++) {
            cy.wait(2000).then(() => {
                let keys = Array.from(EMAILS.keys());
                let whichKey = keys[Math.floor(Math.random() * keys.length)];
                let keyHasValue = EMAILS.get(whichKey);
                cy.get('.card.card-settings.card-shadow').eq(cardIndex).find('.btn-link').eq(1).click({force: true});
                cy.get('.modal-body > .row > :nth-child(1) > .form-group > #name').type(whichKey);
                cy.get('#email').type(keyHasValue);
            }).then(() => {
                cy.get('.btn-normal-primary-with-shadow').click();
                cy.wait(3000);
                cy.get('.awn-toast-wrapper').then(text => {
                    if (text.text().toLowerCase().includes('figyelem')) {
                        cy.get('.btn-tertiary').click();
                    }
                });
            });
        }
    },

    addDomainToPartner(cy, cardIndex) {
        let howMany = Math.floor(Math.random() * DOMAINS.length);

        for (let i = 0; i < howMany; i++) {
            cy.wait(2000).then(() => {
                let domain = DOMAINS[Math.floor(Math.random() * DOMAINS.length)];
                cy.get('.card.card-settings.card-shadow').eq(cardIndex).find('.btn-link').eq(2).click({force: true});
                cy.get('.ti-new-tag-input').type(domain);
            }).then(() => {
                cy.get('.btn-normal-primary-with-shadow').click();
                cy.get('.btn-normal-primary-with-shadow').click();
                cy.wait(3000);
                cy.get('.awn-toast-wrapper').then(text => {
                    if (text.text().toLowerCase().includes('figyelem')) {
                        cy.get('.btn-tertiary').click();
                    }
                });
            });
        }
    },

    signUpNewUser(cy) {
    },

    /**
     * Végiglapozza az oldalakat az Ajánlott partnereknél
     * @param cy
     */
    checkRecommendedPartnerPaging(cy) {
        cy.wait(2000);
        cy.get('.card-footer').scrollIntoView();
        cy.get('.sort-btn').each((el, index) => {
            if (index !== 0) {
                cy.get('.sort-btn').eq(index).invoke('attr', 'class').then(className => {
                    if (!(className.includes('active-sort'))) {
                        cy.get('.sort-btn').eq(index).click();
                        cy.wait(1000);
                    }
                });
            }
        });
    },

    /**
     * Megnézi létezik-e még az utolsó jobbra mutató nyíl.
     * @param cy
     * @returns {boolean}
     */
    rightArrowExist(cy) {
        let rightArrowExists = true;

        cy.get('.sort-btn').then(el => {
            const LAST_ELEMENT = Cypress.$(el).length - 1;

            cy.get('.sort-btn').eq(LAST_ELEMENT).find('span').invoke('attr', 'class').then(lastElementClass => {
                if (lastElementClass.includes('ico-chewron-up-light')) {
                    rightArrowExists = false;
                }
            });
        });

        return rightArrowExists;
    },

    /**
     * Keresést ellenőrzi az ajánlott partnereknél.
     * @param cy
     * @param searchPhrase keresendő kifejezés
     */
    typesInSearchBar(cy, searchPhrase) {
        cy.get('.form-control').clear().type(searchPhrase);
        cy.wait(4000);
        cy.get('tbody').eq(1).find('tr').each((element, index) => {
            cy.get('tbody').eq(1).find('tr').eq(index).invoke('text').should('contain', searchPhrase);
        });
    },

    /**
     * Bejelentkezés és a partner beállítások megnyitása.
     * @param cy
     * @param urlName szerver fő url címe
     */
    loginAndOpenPartners(cy, urlName) {
        actionLez.login(cy, 'q1w2e3r4', 'jtari@csavarker.hu');
        cy.get('.dropdown-toggle').click();
        cy.get('[href="' + urlName + '/partners"]').should('exist');
        cy.get('[href="' + urlName + '/partners"]').click();
    },

    /**
     * Első sorban az admin felületen lévő felhasználói beállításokban lévő funkció gombok be-ki kapcsolásához szolgál.
     * Ha az adott feltétel teljesül, akkor végrehajtja a click() algoritmust.
     * @param cy
     * @param constantBtnName a gomb szövegének olyan része, ami sosem változik, állandó
     * @param variableBtnName feltétel, amely a gomb nevének olyan része, ami megnyomásakor változhat. Ha ezt a feliratot
     * tartalmazza a gomb, akkor a click() esemény végrehajtódik.
     */
    clickOnUserFunctionButton(cy, constantBtnName, variableBtnName) {
        cy.get('.btn-group-md > .btn').each((el, index) => {
            if (el.text().toLowerCase().includes(constantBtnName)) {

                if (el.text().toLowerCase().includes(variableBtnName)) {
                    cy.get('.btn-group-md > .btn').eq(index).click();
                    cy.wait(3000);
                }
            }
        });
    },

    /**
     * Felhasználó beállításainak megtekintése az admin felületén.
     * @param cy
     * @param name a felhasználó neve
     */
    openUserSettings(cy, name) {
        actionLez.login(cy, 'Titkos123?;', 'admin@anvert.com', null);
        cy.wait(3000);
        cy.get(':nth-child(1) > .nav-link').click();
        cy.wait(2000);
        cy.get('tbody > tr').each((el, index) => {
            if (el.text().includes(name)) {
                cy.get('tbody > tr').eq(index).find(':nth-child(2) > a').click();
            }
        });
    },

    /**
     * Ellenőrzi az Ajánlott partner-ek lapozását. Először megnézi admin-ban, hogy be van-e kapcsolva a funkció.
     * @param cy
     * @param urlName szerver fő url címe
     * @param name login_name env változó
     */
    checkPaging(cy, urlName, name) {

        const NUMBER_OF_PAGES = 24;
        const NUMBER = NUMBER_OF_PAGES / 10;

        this.openUserSettings(cy, name);
        this.clickOnUserFunctionButton(cy, 'ajánlott partner export', 'engedélyezése');

        cy.get('.btn-group-md > .btn').each(el => {
            if (el.text().toLowerCase().includes('ajánlott partner export')) {

                cy.get('.fas').click();
                this.loginAndOpenPartners(cy, urlName);
                cy.get('#domain-tab').click();
                for (let i = 0; i <= NUMBER; i++) {
                    this.checkRecommendedPartnerPaging(cy);
                }
            }
        });
    },

    /**
     * Ellenőrzi az Ajánlott partner-ek oldalon, hogy csak 1 oldal látszódik-e. Először megnézi admin-ban, hogy ki
     * van-e kapcsolva a funkció.
     * @param cy
     * @param urlName szerver fő url címe
     * @param name login_name env változó
     */
    checksOnlyOnePageIsAvailable(cy, urlName, name) {
        this.openUserSettings(cy, name);
        this.clickOnUserFunctionButton(cy, 'ajánlott partner export', 'tiltása');

        cy.get('.btn-group-md > .btn').each(el => {
            if (el.text().toLowerCase().includes('ajánlott partner export')) {

                expect(el.text().trim()).to.contain('engedélyezése');
                cy.get('.fas').click();
                this.loginAndOpenPartners(cy, urlName);
                cy.get('#domain-tab').click();
                cy.get('.active-sort').then(element => {
                    expect(element.text().trim()).to.equal('1');
                });
            }
        });
    },
}