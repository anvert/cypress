import action from '../../support/fixture';

const urlName = Cypress.env('local_app_url');
const date = Cypress.env('date');

describe('Dashboard test', function () {

    it('Dashboard function check after login (english)', function () {
        cy.viewport(1920, 1080);
        cy.server();
        cy.route('POST', '/dashboard/get-email-traffic').as('post');
        cy.route('POST', '/config/profile/update-profile-settings').as('update');
        cy.route('POST', '/threads/search-pre-threads').as('mails');

        action.login2(cy, 'q1w2e3r4', 'jtari@csavarker.hu', urlName);
        action.english(cy);
        action.save(cy, 'New fav');
        cy.get(':nth-child(2) > .col-sm-12 > :nth-child(1) > .pointer').click();
        cy.get(':nth-child(1) > .pointer > .anv-close').click();
        cy.get('button.btn-warning-2').click();
        cy.wait('@post').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        action.dashFilter(cy, date);
        cy.wait(3000);
        action.extended(cy, 0, 'iocsai@csavarker.hu', 'árajánlat', 0, 1, 2, '@post');
        cy.wait(3000);
        cy.get('button.btn-link.pull-left').scrollIntoView();
        cy.get('button.btn-link.pull-left').click();
        cy.wait('@mails').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
    })

    it('Dashboard function check after login (hungarian)', function () {
        cy.viewport(1920, 1080);
        cy.server();
        cy.route('POST', '/dashboard/get-email-traffic').as('post');
        cy.route('POST', '/config/profile/update-profile-settings').as('update');
        cy.route('POST', '/threads/search-pre-threads').as('mails');

        action.login2(cy, 'q1w2e3r4', 'jtari@csavarker.hu', urlName);
        action.hungarian(cy);
        action.save(cy, 'New fav');
        cy.get(':nth-child(2) > .col-sm-12 > :nth-child(1) > .pointer').click();
        cy.get(':nth-child(1) > .pointer > .anv-close').click();
        cy.get('button.btn-warning-2').click();
        cy.wait('@post').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        action.dashFilter(cy, date);
        cy.wait(3000);
        action.extended(cy, 0, 'iocsai@csavarker.hu', 'árajánlat', 0, 1, 2, '@post');
        cy.wait(3000);
        cy.get('button.btn-link.pull-left').scrollIntoView();
        cy.get('button.btn-link.pull-left').click();
        cy.wait('@mails').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
    })

})