import action from '../../support/fixture'

const urlName = Cypress.env('local_app_url');
const d = Cypress.env('date');

describe('Dashboard sidebar test', function () {

    it('Dashboard sidebar emotion analysis test check after login (english)', function () {
        cy.viewport(1920, 1080);
        cy.server();
        cy.route('POST', '/dashboard/get-email-traffic').as('post');
        cy.route('POST', '/report/filtering-reaction-time').as('filt');
        cy.route('POST', '/config/profile/update-profile-settings').as('update');
        cy.route('POST', '/threads/search-pre-threads').as('mails');
        cy.route('POST', '/dashboard/get-neticle-top-list').as('emotion');

        action.login2(cy, 'q1w2e3r4', 'jtari@csavarker.hu', urlName);
        action.english(cy);
        cy.get('.nav-item > .menu-content > :nth-child(2) > .menu-item').click();
        cy.url().should('include', '/emotion-analysis');
        cy.get('h1').contains('Emotion analysis').should('exist');
        action.analysis(cy, d);
        cy.wait(1000);

        cy.get('.raphael-group-89-dataset-axis > text').each((el, index) => {
            cy.get('.raphael-group-89-dataset-axis > text').eq(index).click();

            // Pleasure érzelem figyelmen kívül hagyása
            if (index !== 6) {
                cy.wait('@emotion').should((xhr) => {
                    expect(xhr.status, 'successful POST').to.equal(200)
                });
            }

            cy.get('.raphael-group-89-dataset-axis > text').eq(index).invoke('text').then(($text) => {
                var tmp = $text;
                cy.log(tmp);
                cy.get('h3.card-titles').eq(1).contains(tmp, {matchCase: false});

                if (tmp.trim() !== 'Pleasure') {
                    cy.get('td.empty-thread-list-label').should('not.exist');
                }
            });
        });

        cy.get('span.small span.pointer').click();
        cy.wait('@emotion').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        });
        action.extended(cy, 1, 'iocsai@csavarker.hu', 'árajánlat', 0, 1, 2, '@post');
    })

    it('Dashboard sidebar emotion analysis test after login (hungarian)', function () {
        cy.viewport(1920, 1080);
        cy.server();
        cy.route('POST', '/dashboard/get-email-traffic').as('post');
        cy.route('POST', '/report/filtering-reaction-time').as('filt');
        cy.route('POST', '/config/profile/update-profile-settings').as('update');
        cy.route('POST', '/threads/search-pre-threads').as('mails');
        cy.route('POST', '/dashboard/get-neticle-top-list').as('emotion');

        action.login2(cy, 'q1w2e3r4', 'jtari@csavarker.hu', urlName);
        action.hungarian(cy);
        cy.get('.nav-item > .menu-content > :nth-child(2) > .menu-item').click();
        cy.url().should('include', '/emotion-analysis');
        cy.get('h1').contains('Érzelem elemzés').should('exist');
        action.analysis(cy, d);
        cy.wait(1000);

        cy.get('.raphael-group-89-dataset-axis > text').each((el, index) => {
            cy.get('.raphael-group-89-dataset-axis > text').eq(index).click();

            // Pleasure érzelem figyelmen kívül hagyása
            if (index !== 6) {
                cy.wait('@emotion').should((xhr) => {
                    expect(xhr.status, 'successful POST').to.equal(200)
                });
            }

            cy.get('.raphael-group-89-dataset-axis > text').eq(index).invoke('text').then(($text) => {
                var tmp = $text;
                cy.log(tmp);
                cy.get('h3.card-titles').eq(1).contains(tmp, {matchCase: false});

                if (tmp.trim() !== 'Öröm') {
                    cy.get('td.empty-thread-list-label').should('not.exist');
                }
            });
        });

        cy.get('span.small span.pointer').click();
        cy.wait('@emotion').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(1000);
        action.extended(cy, 1, 'iocsai@csavarker.hu', 'árajánlat', 0, 1, 2, '@post');
    })
})