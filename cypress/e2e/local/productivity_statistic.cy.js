import action from '../../support/fixture'

const urlName = Cypress.env('local_app_url');
const d = Cypress.env('date');

describe('Dashboard sidebar test', function () {

	it('Dashboard sidebar productivity check after login (hungarian)', function (){
		cy.viewport(1920, 1080);
		cy.server();
		cy.route('POST', '/dashboard/get-email-traffic').as('post');
		cy.route('POST', '/report/filtering-email-productivity').as('filt');
		cy.route('POST', '/config/profile/update-profile-settings').as('update');
		cy.route('POST', '/threads/search-pre-threads').as('mails');

		action.login2(cy, 'q1w2e3r4', 'jtari@csavarker.hu', urlName);
		action.hungarian(cy);
    	cy.get('ul#main-menu-navigation li a').eq(2).click({force:true});
		cy.url().should('include', '/report/daily_email_prod');
		cy.get('h2.header-label.bold-header').contains('Aktivitás').should('exist');
		action.prod(cy, d);
		cy.get('span.multiselect__tag').eq(0).then(function($elem) {
			cy.log($elem.text().trim());
			cy.get('.multiselect__tag > span').should('contain', $elem.text().trim());
		})
		action.extendedForStatistic(cy, 0, 'info@csavarker.hu', 'árajánlat', 0, 1, 2, '@filt');
		cy.wait(3000);
		cy.get('button.btn-link.pull-left').click();
		cy.wait('@mails').should((xhr) => {
    		expect(xhr.status, 'successful POST').to.equal(200)
  		})
	})

	it('Dashboard sidebar productivity check after login (english)', function (){
		cy.viewport(1920, 1080);
		cy.server();
		cy.route('POST', '/dashboard/get-email-traffic').as('post');
		cy.route('POST', '/report/filtering-email-productivity').as('filt');
		cy.route('POST', '/config/profile/update-profile-settings').as('update');
		cy.route('POST', '/threads/search-pre-threads').as('mails');

		action.login2(cy, 'q1w2e3r4', 'jtari@csavarker.hu', urlName);
		action.english(cy);
		cy.get('ul#main-menu-navigation li a').eq(2).click({force:true});
		cy.url().should('include', '/report/daily_email_prod');
		cy.get('h2.header-label.bold-header').contains('Productivity').should('exist');
		action.prod(cy, d);
		cy.get('span.multiselect__tag').eq(0).then(function($elem) {
			cy.log($elem.text().trim());
			cy.get('.multiselect__tag > span').should('contain', $elem.text().trim());
		})
		action.extendedForStatistic(cy, 0, 'info@csavarker.hu', 'árajánlat', 0, 1, 2, '@filt');
		cy.wait(3000);
		cy.get('button.btn-link.pull-left').click();
		cy.wait('@mails').should((xhr) => {
    		expect(xhr.status, 'successful POST').to.equal(200)
  		})
	})

})