import action from '../../support/fixture'

const urlName = Cypress.env('local_app_url');
const d = Cypress.env('date');
const d2 = Cypress.env('date2');
const d3 = Cypress.env('date3');

describe('Dashboard sidebar test', function () {

    it('Dashboard sidebar opinion analysis test after login (english)', function () {
        cy.viewport(1920, 1080);
        cy.server();
        cy.route('POST', '/dashboard/get-email-traffic').as('post');
        cy.route('POST', '/report/filtering-reaction-time').as('filt');
        cy.route('POST', '/config/profile/update-profile-settings').as('update');
        cy.route('POST', '/threads/search-pre-threads').as('mails');

        action.login2(cy, 'q1w2e3r4', 'jtari@csavarker.hu', urlName);
        action.english(cy);
        cy.get('.nav-item > .menu-content > :nth-child(1) > .menu-item').click();
        cy.url().should('include', '/opinion-analysis');
        cy.get('h1').contains('Sentiment analysis').should('exist');
        action.analysis(cy, d);
        cy.wait(1000);
        cy.get('svg#raphael-paper-2 g g g g text').eq(2).contains(d2).should('exist');
        cy.get('svg#raphael-paper-2 g g g g text').eq(7).contains(d3).should('exist');
        cy.get('table.top-list.body-2').nextAll().should('not.eq', '');
        cy.get('a.sort-btn').eq(0).click({force: true});
        cy.get('a.sort-btn').eq(4).click({force: true});
        cy.get('a.sort-btn').eq(8).click({force: true});
        cy.get('table.top-list.body-2 tr').eq(9).contains('10.').should('exist');
        cy.get('table.top-list.body-2 tr').eq(19).contains('10.').should('exist');

        cy.get('#vs3__combobox > .vs__actions').click({force: true});
        cy.get('#vs3__option-0').contains('Most positive').should('exist');
        cy.get('#vs3__option-1').contains('Most negative').should('exist');

        cy.get('#vs5__combobox > .vs__actions').click({force: true});
        cy.get('#vs5__option-0').contains('Most positive').should('exist');
        cy.get('#vs5__option-1').contains('Most negative').should('exist');

        cy.get('#vs7__combobox > .vs__actions').click({force: true});
        cy.get('#vs7__option-0').contains('Most positive').should('exist');
        cy.get('#vs7__option-1').contains('Most negative').should('exist');
        cy.get('table.top-list.body-2').nextAll().should('not.eq', '');

        cy.get('.card.rounded-xxl').each((el, index, list) => {
            cy.get('.card.rounded-xxl').find('h3').then(h3_TEXT => {
                if (!(h3_TEXT.text().includes('Sentiment index'))) {
                    cy.get('table.top-list.body-2 tr td span').eq(0).then(($text) => {
                        var tmp = $text.attr('title');
                        cy.wrap($text);
                        cy.log(tmp);
                        cy.get('#vs3__combobox > .vs__actions').click({force: true});
                        cy.get('label.input-label.orderBy-text').eq(0).contains('Most negative').should('exist');
                        cy.get('table.top-list.body-2 tr td span').eq(0).should('not.eq', tmp);
                    })
                }
            });
        });

        action.extended(cy, 1, 'iocsai@csavarker.hu', 'árajánlat', 0, 1, 2, '@post');
    })

    it('Dashboard sidebar opinion analysis test after login (hungarian)', function () {
        cy.viewport(1920, 1080);
        cy.server();
        cy.route('POST', '/dashboard/get-email-traffic').as('post');
        cy.route('POST', '/report/filtering-reaction-time').as('filt');
        cy.route('POST', '/config/profile/update-profile-settings').as('update');
        cy.route('POST', '/threads/search-pre-threads').as('mails');

        action.login2(cy, 'q1w2e3r4', 'jtari@csavarker.hu', urlName);
        action.hungarian(cy);
        cy.get('.nav-item > .menu-content > :nth-child(1) > .menu-item').click();
        cy.url().should('include', '/opinion-analysis');
        cy.get('h1').contains('Hangulatelemzés').should('exist');
        action.analysis(cy, d);
        cy.wait(1000);
        cy.get('svg#raphael-paper-2 g g g g text').eq(2).contains(d2).should('exist');
        cy.get('svg#raphael-paper-2 g g g g text').eq(7).contains(d3).should('exist');
        cy.get('table.top-list.body-2').nextAll().should('not.eq', '');
        cy.get('a.sort-btn').eq(0).click({force: true});
        cy.get('a.sort-btn').eq(4).click({force: true});
        cy.get('a.sort-btn').eq(8).click({force: true});
        cy.get('table.top-list.body-2 tr').eq(9).contains('10.').should('exist');
        cy.get('table.top-list.body-2 tr').eq(19).contains('10.').should('exist');

        cy.get('#vs3__combobox > .vs__actions').click({force: true});
        cy.get('#vs3__option-0').contains('Legpozitívabb').should('exist');
        cy.get('#vs3__option-1').contains('Legnegatívabb').should('exist');

        cy.get('#vs5__combobox > .vs__actions').click({force: true});
        cy.get('#vs5__option-0').contains('Legpozitívabb').should('exist');
        cy.get('#vs5__option-1').contains('Legnegatívabb').should('exist');

        cy.get('#vs7__combobox > .vs__actions').click({force: true});
        cy.get('#vs7__option-0').contains('Legpozitívabb').should('exist');
        cy.get('#vs7__option-1').contains('Legnegatívabb').should('exist');
        cy.get('table.top-list.body-2').nextAll().should('not.eq', '');

        cy.get('.card.rounded-xxl').each((el, index, list) => {
            cy.get('.card.rounded-xxl').find('h3').then(h3_TEXT => {
                if (!(h3_TEXT.text().includes('Hangulatindex'))) {
                    cy.get('table.top-list.body-2 tr td span').eq(0).then(($text) => {
                        var tmp = $text.attr('title');
                        cy.wrap($text);
                        cy.log(tmp);
                        cy.get('#vs3__combobox > .vs__actions').click({force: true});
                        cy.get('label.input-label.orderBy-text').eq(0).contains('Legnegatívabb').should('exist');
                        cy.get('table.top-list.body-2 tr td span').eq(0).should('not.eq', tmp);
                    })
                }
            });
        });

        action.extended(cy, 1, 'iocsai@csavarker.hu', 'árajánlat', 0, 1, 2, '@post');
    })
})