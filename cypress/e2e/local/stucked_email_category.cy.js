import action from '../../support/fixture'

const urlName = Cypress.env('local_app_url');
const d = Cypress.env('date');
const d2 = Cypress.env('date2');
const d3 = Cypress.env('date3');
const d4 = Cypress.env('date4');

describe('Dashboard sidebar test', function () {

    it('Dashboard sidebar stucked emails check after login (english)', function (){
        cy.viewport(1920, 1080);
        cy.server();
        cy.route('POST', '/dashboard/get-email-traffic').as('post');
        cy.route('POST', '/config/profile/update-profile-settings').as('update');
        cy.route('POST', '/closed_thread/add').as('closed');
        cy.route('POST', '/deleted_thread/add').as('trash');
        cy.route('POST', '/threads/thread-emails').as('email');
        cy.route('POST', '/threads/search-threads').as('search');
        cy.route('POST', '/threads/get-threads').as('searchDelete');
        cy.route('POST', '/config/save-filter').as('filter');
        cy.route('GET', '/config/get-filters').as('filters');

        action.login2(cy, 'q1w2e3r4', 'jtari@csavarker.hu', urlName);
        action.english(cy);
        action.repl(cy, 4);
        action.searchD(cy, d, d2, d3, d4);
        action.getDate(cy, d2, d3, d4);
        cy.get('span.thread-list-name').eq(0).then(function($elem) {
            action.searchE(cy, $elem.text().trim());
            action.getEmail(cy, $elem.text().trim());
        })
        action.searchT(cy, 'megrendel');
        action.getText(cy, 'megrendel');
        action.filter(cy);
    })
    
    it('Dashboard sidebar stucked emails check after login (hungarian)', function (){
        cy.viewport(1920, 1080);
        cy.server();
        cy.route('POST', '/dashboard/get-email-traffic').as('post');
        cy.route('POST', '/config/profile/update-profile-settings').as('update');
        cy.route('POST', '/closed_thread/add').as('closed');
        cy.route('POST', '/deleted_thread/add').as('trash');
        cy.route('POST', '/threads/thread-emails').as('email');
        cy.route('POST', '/threads/search-threads').as('search');
        cy.route('POST', '/threads/get-threads').as('searchDelete');
        cy.route('POST', '/config/save-filter').as('filter');
        cy.route('GET', '/config/get-filters').as('filters');

        action.login2(cy, 'q1w2e3r4', 'jtari@csavarker.hu', urlName);
        action.hungarian(cy);
        action.repl(cy, 4);
        action.searchD(cy, d, d2, d3, d4);
        action.getDate(cy, d2, d3, d4);
        cy.get('span.thread-list-name').eq(0).then(function($elem) {
            action.searchE(cy, $elem.text().trim());
            action.getEmail(cy, $elem.text().trim());
        })
        action.searchT(cy, 'megrendel');
        action.getText(cy, 'megrendel');
        action.filter(cy);
    })
})