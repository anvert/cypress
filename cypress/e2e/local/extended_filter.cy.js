import action from '../../support/fixture'

const urlName = Cypress.env('local_app_url');

describe('Dashboard test extended filter', function () {

    it('Dashboard extended filter test after login (english)', function () {
        cy.viewport(1920, 1080);
        cy.server();
        cy.route('POST', '/dashboard/get-email-traffic').as('post');
        cy.route('POST', '/config/profile/update-profile-settings').as('update');
        cy.route('POST', '/threads/search-pre-threads').as('mails');

        action.login2(cy, 'q1w2e3r4', 'jtari@csavarker.hu', urlName);
        action.english(cy);
        action.extended(cy, 0, 'iocsai@csavarker.hu', 'árajánlat', 0, 1, 2, '@post');
    })

    it('Dashboard extended filter test after login (hungarian)', function () {
        cy.viewport(1920, 1080);
        cy.server();
        cy.route('POST', '/dashboard/get-email-traffic').as('post');
        cy.route('POST', '/config/profile/update-profile-settings').as('update');
        cy.route('POST', '/threads/search-pre-threads').as('mails');

        action.login2(cy, 'q1w2e3r4', 'jtari@csavarker.hu', urlName);
        action.hungarian(cy);
        action.extended(cy, 0, 'iocsai@csavarker.hu', 'árajánlat', 0, 1, 2, '@post');
    })
})