import action from '../../support/fixture'

const urlName = Cypress.env('app_url');

describe('Profile test', function () {

	it('Dashboard check after login (english)', function(){
		cy.viewport(1920, 1080);
		cy.server();
		cy.route('POST', '/dashboard/get-email-traffic').as('post');
		cy.route('POST', '/config/profile/update-profile-settings').as('update');
		cy.route('POST', '/config/profile/update-user-n-company-data').as('updateProf');
		action.login2(cy, 'q1w2e3r4', 'jtari@csavarker.hu', urlName);
		action.english(cy);
		action.settings(cy, '4', '11', '61', '4', '241');
	})

	it('Dashboard check after login (hungarian)', function(){
		cy.viewport(1920, 1080);
		cy.server();
		cy.route('POST', '/dashboard/get-email-traffic').as('post');
		cy.route('POST', '/config/profile/update-profile-settings').as('update');
		cy.route('POST', '/config/profile/update-user-n-company-data').as('updateProf');
		action.login2(cy, 'q1w2e3r4', 'jtari@csavarker.hu', urlName);
		action.hungarian(cy);
		action.settings(cy, '3', '10', '60', '3', '240');
	})
})