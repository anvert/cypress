import action from '../../support/fixture'

const urlName = Cypress.env('local_app_url');
const date = Cypress.env('date');

describe('Dashboard sidebar test', function () {

    it('Dashboard sidebar internal emails check after login (english)', function (){
        cy.viewport(1920, 1080);
        cy.server();
        cy.route('POST', '/dashboard/get-email-traffic').as('post');
        cy.route('POST', '/config/profile/update-profile-settings').as('update');
        cy.route('POST', '/closed_thread/add').as('closed');
        cy.route('POST', '/deleted_thread/add').as('trash');
        cy.route('POST', '/threads/thread-emails').as('email');
        cy.route('POST', '/threads/search-threads').as('search');
        cy.route('POST', '/threads/get-threads').as('searchDelete');
        cy.route('POST', '/config/save-filter').as('filter');
        cy.route('GET', '/config/get-filters').as('filters');

        action.login2(cy, 'q1w2e3r4', 'jtari@csavarker.hu', urlName);
        action.english(cy);
        action.repl(cy, 2, 'titkarsag@csavarker.hu', date, 'csavar');
        action.filter(cy);
    })
	
	it('Dashboard sidebar internal emails check after login (hungarian)', function (){
        cy.viewport(1920, 1080);
		cy.server();
        cy.route('POST', '/dashboard/get-email-traffic').as('post');
        cy.route('POST', '/config/profile/update-profile-settings').as('update');
		cy.route('POST', '/closed_thread/add').as('closed');
		cy.route('POST', '/deleted_thread/add').as('trash');
		cy.route('POST', '/threads/thread-emails').as('email');
		cy.route('POST', '/threads/search-threads').as('search');
        cy.route('POST', '/threads/get-threads').as('searchDelete');
        cy.route('POST', '/config/save-filter').as('filter');
        cy.route('GET', '/config/get-filters').as('filters');

        action.login2(cy, 'q1w2e3r4', 'jtari@csavarker.hu', urlName);
        action.hungarian(cy);
        action.repl(cy, 2, 'titkarsag@csavarker.hu', date, 'csavar');
        action.filter(cy);
	})

})