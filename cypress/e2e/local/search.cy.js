import action from '../../support/fixture'

const urlName = Cypress.env('local_app_url');
const d = Cypress.env('date');
const d2 = Cypress.env('date2');
const d3 = Cypress.env('date3');
const d4 = Cypress.env('date4');

describe('Dashboard sidebar test', function () {

    it('Search test after login (english)', function () {
        cy.viewport(1920, 1080);
        cy.server();
        cy.route('POST', '/dashboard/get-email-traffic').as('post');
        cy.route('POST', '/closed_thread/add').as('closed');
        cy.route('POST', '/deleted_thread/add').as('trash');
        cy.route('POST', '/threads/thread-emails').as('email');
        cy.route('POST', '/threads/search-threads').as('search');
        cy.route('POST', '/threads/get-threads').as('thread');
        cy.route('POST', '/config/profile/update-profile-settings').as('update');

        action.login2(cy, 'q1w2e3r4', 'jtari@csavarker.hu', urlName);
        action.english(cy);
        cy.get('ul li a.menu-item div span.menu-item').eq(3).click({force: true});
        cy.url().should('include', '/threads');
        cy.wait('@thread').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(3000);
        action.searchD(cy, d, d2, d3, d4);
        action.getDate(cy, d2, d3, d4);
        action.searchE(cy, 'JOSZERSZAM.com');
        action.getEmail(cy, 'JOSZERSZAM.com');
        action.searchT(cy, 'ajánlat');
        action.getText(cy, 'ajánlat');
    })

    it('Search test after login (hungarian)', function () {
        cy.viewport(1920, 1080);
        cy.server();
        cy.route('POST', '/dashboard/get-email-traffic').as('post');
        cy.route('POST', '/closed_thread/add').as('closed');
        cy.route('POST', '/deleted_thread/add').as('trash');
        cy.route('POST', '/threads/thread-emails').as('email');
        cy.route('POST', '/threads/search-threads').as('search');
        cy.route('POST', '/threads/get-threads').as('thread');
        cy.route('POST', '/config/profile/update-profile-settings').as('update');

        action.login2(cy, 'q1w2e3r4', 'jtari@csavarker.hu', urlName);
        action.hungarian(cy);
        cy.get('ul li a.menu-item div span.menu-item').eq(3).click({force: true});
        cy.url().should('include', '/threads');
        cy.wait('@thread').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(3000);
        action.searchD(cy, d, d2, d3, d4);
        action.getDate(cy, d2, d3, d4);
        action.searchE(cy, 'Demus Gábor');
        action.getEmail(cy, 'Demus Gábor');
        action.searchT(cy, 'ajánlat');
        action.getText(cy, 'ajánlat');
    })
})