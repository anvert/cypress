import action from '../../support/fixture'

const urlName = Cypress.env('local_app_url');
const d = Cypress.env('spamDate');
const d2 = Cypress.env('spamDate2');
const d3 = Cypress.env('spamDate3');
const d4 = Cypress.env('spamDate4');

describe('Dashboard sidebar test', function () {
  
    it('Dashboard sidebar closed emails check after login (english)', function (){
        cy.viewport(1920, 1080);
        cy.server();
        cy.route('POST', '/dashboard/get-email-traffic').as('post');
        cy.route('POST', '/config/profile/update-profile-settings').as('update');
        cy.route('POST', '/closed_thread/add').as('closed');
        cy.route('POST', '/deleted_thread/add').as('trash');
        cy.route('POST', '/threads/thread-emails').as('email');
        cy.route('POST', '/threads/search-threads').as('search');
        cy.route('POST', '/threads/get-threads').as('threads');
        cy.route('POST', '/config/save-filter').as('filter');
        cy.route('GET', '/config/get-filters').as('filters');
        cy.route('POST', '/closed_thread/remove').as('closedRemove');
        cy.route('POST', '/deleted_thread/remove').as('deletedRemove');

        action.login2(cy, 'q1w2e3r4', 'jtari@csavarker.hu', urlName);
        action.english(cy);
        action.moveLetterToClosed(cy);
        action.closed(cy, 5, 0);
        action.searchD(cy, d, d2, d3, d4);
        action.getDate(cy, d2, d3, d4);
        action.searchE(cy, 'lkoszo@csavarker.hu');
        action.getEmail(cy, 'lkoszo@csavarker.hu');
        action.searchT(cy, 'árajánlat');
        action.getText(cy, 'árajánlat');
        action.filter(cy);
        action.restoreThreads(cy, 'Trash', '@deletedRemove');
        action.restoreThreads(cy, 'Closed', '@closedRemove');
  })

    it('Dashboard sidebar closed emails check after login (hungarian)', function (){
        cy.viewport(1920, 1080);
        cy.server();
        cy.route('POST', '/dashboard/get-email-traffic').as('post');
        cy.route('POST', '/config/profile/update-profile-settings').as('update');
        cy.route('POST', '/closed_thread/add').as('closed');
        cy.route('POST', '/deleted_thread/add').as('trash');
        cy.route('POST', '/threads/thread-emails').as('email');
        cy.route('POST', '/threads/search-threads').as('search');
        cy.route('POST', '/threads/get-threads').as('threads');
        cy.route('POST', '/closed_thread/remove').as('remove');
        cy.route('POST', '/config/save-filter').as('filter');
        cy.route('GET', '/config/get-filters').as('filters');
        cy.route('POST', '/closed_thread/remove').as('closedRemove');
        cy.route('POST', '/deleted_thread/remove').as('deletedRemove');

        action.login2(cy, 'q1w2e3r4', 'jtari@csavarker.hu', urlName);
        action.hungarian(cy);
        action.moveLetterToClosed(cy);
        action.closed(cy, 5, 0);
        action.searchD(cy, d, d2, d3, d4);
        action.getDate(cy, d2, d3, d4);
        // cy.get(':nth-child(7) > a.menu-item > :nth-child(1) > .menu-item').click(); // Föntiek kihagyása esetén
        action.searchE(cy, 'www.tuzoltokeszulek.com');
        action.getEmail(cy, 'www.tuzoltokeszulek.com');
        action.searchT(cy, 'árajánlat');
        action.getText(cy, 'árajánlat');
        action.filter(cy);
        action.restoreThreads(cy, 'Törölt', '@deletedRemove');
        action.restoreThreads(cy, 'Lezárt', '@closedRemove');
  })
  
})