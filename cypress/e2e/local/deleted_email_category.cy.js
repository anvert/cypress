import action from '../../support/fixture'

const urlName = Cypress.env('local_app_url');
const d = Cypress.env('date');
const d2 = Cypress.env('date2');
const d3 = Cypress.env('date3');
const d4 = Cypress.env('date4');

describe('Dashboard sidebar test', function () {

    it('Dashboard sidebar deleted emails check after login (english)', function (){
        cy.viewport(1920, 1080);
        cy.server();
        cy.route('POST', '/dashboard/get-email-traffic').as('post');
        cy.route('POST', '/config/profile/update-profile-settings').as('update');
        cy.route('POST', '/closed_thread/add').as('closed');
        cy.route('POST', '/deleted_thread/add').as('trash');
        cy.route('POST', '/threads/thread-emails').as('email');
        cy.route('POST', '/threads/search-threads').as('search');
        cy.route('POST', '/threads/get-threads').as('threads');
        cy.route('POST', '/deleted_thread/remove').as('remove');
        cy.route('POST', '/deleted_thread/permanent').as('deleted');
        cy.route('POST', '/config/save-filter').as('filter');
        cy.route('GET', '/config/get-filters').as('filters');
        cy.route('POST', '/deleted_thread/remove').as('deletedRemove');

        action.login2(cy, 'q1w2e3r4', 'jtari@csavarker.hu', urlName);
        action.english(cy);
        action.deleteLetter(cy, 0);
        action.closed(cy, 6, 1);
        action.searchD(cy, d, d2, d3, d4);
        action.getDate(cy, d2, d3, d4);
        action.searchE(cy, 'Kiss Zoltán');
        action.getEmail(cy, 'Kiss Zoltán');
        action.searchT(cy, 'árajánlat');
        action.getText(cy, 'árajánlat');
        action.filter(cy);
        action.restoreThreads(cy, 'Trash', '@deletedRemove');
    })
  
    it.skip('Dashboard sidebar deleted emails check after login (hungarian)', function (){
        cy.viewport(1920, 1080);
        cy.server();
        cy.route('POST', '/dashboard/get-email-traffic').as('post');
        cy.route('POST', '/config/profile/update-profile-settings').as('update');
        cy.route('POST', '/closed_thread/add').as('closed');
        cy.route('POST', '/deleted_thread/add').as('trash');
        cy.route('POST', '/threads/thread-emails').as('email');
        cy.route('POST', '/threads/search-threads').as('search');
        cy.route('POST', '/threads/get-threads').as('threads');
        cy.route('POST', '/deleted_thread/remove').as('remove');
        cy.route('POST', '/deleted_thread/permanent').as('deleted');
        cy.route('POST', '/config/save-filter').as('filter');
        cy.route('GET', '/config/get-filters').as('filters');
        cy.route('POST', '/deleted_thread/remove').as('deletedRemove');

        action.login2(cy, 'q1w2e3r4', 'jtari@csavarker.hu', urlName);
        action.hungarian(cy);
        action.deleteLetter(cy, 0);
        action.closed(cy, 6, 1);
        action.searchD(cy, d, d2, d3, d4);
        action.getDate(cy, d2, d3, d4);
        action.searchE(cy, 'Szabolcs Tari');
        action.getEmail(cy, 'Szabolcs Tari');
        action.searchT(cy, 'Árajánlatkérés');
        action.getText(cy, 'Árajánlatkérés');
        action.filter(cy);
        action.restoreThreads(cy, 'Törölt', '@deletedRemove');
    })

})