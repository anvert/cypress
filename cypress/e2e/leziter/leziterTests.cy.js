import action from '../../support/leziterFunctions'
// import action2 from '../../support/fixture'

const urlName = Cypress.env('app_url');

describe.skip('Sending email, making draft', () => {
    beforeEach( () => {
        action.login(cy, 'Titkos123', 'demokerepespeter@outlook.com', '/assigned/get-dashboard-data');
    })

    it.skip('checks Letter Composing', () => {
        action.compose(cy, 'demobartaemese@outlook.com', true);
    });

    it('makes draft',() => {
        action.compose(cy, 'demobartaemese@outlook.com', false);
    });

    it.skip('checks Letter Writing', () => {
        action.tryingComposingFunctions(cy);
    });
})

describe.skip('Delegation testing', () => {
    beforeEach( () => {
        action.login(cy, 'Titkos123', 'demonagyjanos@outlook.com');

        cy.contains('Delegálás').click();
        cy.intercept({
            method: 'POST',
            url: '/assigned/get-dashboard-data',
        }).as('delegation');
        cy.wait('@delegation').its('response.statusCode').should('eq', 200);
    })

    it('creates undelegated letter', () => {
        action.createsUndelegatedLetter(cy, true);
    });

    it('delegates a letter', () => {
        action.delegates(cy);
    });

    it('checks the numbers of delegation boxes', () => {
        action.checksNumbersOfDelegationBoxes(cy);
    });
})

describe.skip('Employee roles with boss login', () => {
    beforeEach( () => {
        action.login(cy, 'Titkos123', 'demonagyjanos@outlook.com');
    })

    it('checks role visibility in list', function () {
        action.visibleInList(cy);
    });
});

describe.skip('Employee roles with employee login', () => {
    beforeEach( 'Signing in with employee', () => {
        action.login(cy, 'Titkos123', 'demokerepespeter@outlook.com', '/assigned/get-dashboard-data');
    });

    it('checks if employee sees his e-mails only', () => {
        const newAndOldcentralUsernames = ['demofiokk', 'outlookinfo', 'info'];
        action.checkAllEmailsForEmployee(
            cy,
            'demofiokk@outlook.com',
            newAndOldcentralUsernames,
            'demokerepespeter@outlook.com',
            'demokerepespeter'
        );
    });

    it('checks the searching to display only those letters that belong to employee', () => {
        action.checksSearchbar(cy, 'válasz');
    });

    it('checks if the alert box exists', () => {
        action.alertAppears(cy);
    });
});

describe('Letter writing, replying and forwarding', () => {
    beforeEach( 'Signing in with employee', () => {
        action.login(cy, 'Titkos123', 'demokerepespeter@outlook.com', '/assigned/get-dashboard-data');
    });

    it('should see letter writing modal', function () {
        action.opensLetterWriting(cy);
    });

    it('checks the default signature to load automatically', function () {
        action.analyzesDefaultSignature(cy);
        action.checksMoreSignature(cy);
    });

    it('checks letter writing parameters', function () {
        action.checksParameters(cy);
        action.checksSenderOptions(cy, 'demofiokk@outlook.com', 'demokerepespeter@outlook.com');
    });

    it('should see an alert box if somebody already has started replying', function () {
        // action.alertAppears(cy);
        // TODO: nem tudom ellenőrizni, kinek a megkezdett piszkozata van a lista tetején
    });

    it('checks file attache', function () {
        action.opensLetterWriting(cy);
        action.attachesAndDeletesFiles(cy);
        action.checkSizeOfAttachments(cy);
    });
});

describe.skip('Checking the attachements link (//TODO)', () => {
    beforeEach( 'Signing in with employee', () => {
        cy.viewport(1920, 1080);
        cy.visit(urlName);
        cy.wait(2000);

        cy.url().should('include', '/login')
        action.login(cy, 'Titkos123', 'demokerepespeter@outlook.com', '/assigned/get-dashboard-data');
    });

    it('sends and analyizes email', function () {
        action.openLetter(cy, 'Csatolmány teszt');
    });
});