import action from '../../support/fixture'

const urlName = Cypress.env('app_url');

describe('Smaller view test', function () {
	
	it('Smaller viewport test (1200x700)', function(){
		cy.viewport(1200, 700);
		cy.server();
		cy.route('POST', '/dashboard/get-email-traffic').as('post');
		cy.route('POST', '/report/filtering-email-productivity').as('filt');
		cy.route('POST', '/threads/get-threads').as('threads');
		cy.route('GET', '/permissions/init-data').as('perm');
		cy.route('GET', '/threads/get-categories').as('messenger');
		cy.route('POST', '/config/profile/update-user-n-company-data').as('updateProf');
		cy.route('GET', '/config/get-filters').as('getFilt');
		cy.route('POST', '/threads/thread-emails').as('email');

		action.login2(cy, 'q1w2e3r4', 'jtari@csavarker.hu', urlName);
		action.save(cy, 'Favourite');
		action.statisticWoN(cy);

		cy.get('ul#main-menu-navigation li ul li a').eq(0).click({force: true});
		cy.wait('@post').should((xhr) => {
			expect(xhr.status, 'successful POST').to.equal(200)
		})
		cy.get('button.btn-link').eq(0).click({force:true});
		cy.get('button.btn-normal-primary-with-shadow').eq(0).should('be.visible').should('not.be.disabled');
		cy.get('.btn-tertiary').eq(1).should('be.visible').should('not.be.disabled').click({force:true});
		cy.get('div.col-md-12 div span.pointer span').eq(0).click({force:true});
		cy.wait(1000);
		cy.get('.btn-warning-2.btn-ml').should('be.visible').should('not.be.disabled');
		cy.get('.btn-tertiary').eq(0).should('be.visible').should('not.be.disabled').click({force:true});
		cy.wait(1000);

		cy.get('ul#main-menu-navigation li ul li a').eq(1).click({force: true});
		cy.get('button.btn-link').click({force:true});
		cy.get('button.btn-normal-primary-with-shadow').eq(0).should('be.visible').should('not.be.disabled');
		cy.get('.btn-tertiary').eq(1).should('be.visible').should('not.be.disabled').click({force:true});
		cy.get('div.col-md-12 div span.pointer span').eq(0).click({force:true});
		cy.wait(1000);
		cy.get('.btn-warning-2.btn-ml').should('be.visible').should('not.be.disabled');
		cy.get('.btn-tertiary').eq(0).should('be.visible').should('not.be.disabled').click({force:true});

		action.viewThread(cy);
		action.viewSettings(cy);
	})

	it('Smaller viewport test (1200x600)', function(){
		cy.viewport(1200, 600);
		cy.server();
		cy.route('POST', '/dashboard/get-email-traffic').as('post');
		cy.route('POST', '/report/filtering-email-productivity').as('filt');
		cy.route('POST', '/threads/get-threads').as('threads');
		cy.route('GET', '/permissions/init-data').as('perm');
		cy.route('GET', '/threads/get-categories').as('messenger');
		cy.route('POST', '/config/profile/update-user-n-company-data').as('updateProf');
		cy.route('GET', '/config/get-filters').as('getFilt');
		cy.route('POST', '/threads/thread-emails').as('email');

		action.login2(cy, 'q1w2e3r4', 'jtari@csavarker.hu', urlName);
		action.save(cy, 'Favourite');
		action.statisticWoN(cy);

		cy.get('ul#main-menu-navigation li ul li a').eq(0).click({force: true});
		cy.wait('@post').should((xhr) => {
			expect(xhr.status, 'successful POST').to.equal(200)
		})
		cy.get('button.btn-link').eq(0).click({force:true});
		cy.get('button.btn-normal-primary-with-shadow').eq(0).should('be.visible').should('not.be.disabled');
		cy.get('.btn-tertiary').eq(1).should('be.visible').should('not.be.disabled').click({force:true});
		cy.get('div.col-md-12 div span.pointer span').eq(0).click({force:true});
		cy.wait(1000);
		cy.get('.btn-warning-2').should('be.visible').should('not.be.disabled');
		cy.get('.btn-tertiary').eq(0).should('be.visible').should('not.be.disabled').click({force:true});
		cy.wait(1000);

		cy.get('ul#main-menu-navigation li ul li a').eq(1).click({force: true});
		cy.get('button.btn-link').click({force:true});
		cy.get('button.btn-normal-primary-with-shadow').eq(0).should('be.visible').should('not.be.disabled');
		cy.get('.btn-tertiary').eq(1).should('be.visible').should('not.be.disabled').click({force:true});
		cy.get('div.col-md-12 div span.pointer span').eq(0).click({force:true});
		cy.wait(1000);
		cy.get('.btn-warning-2').should('be.visible').should('not.be.disabled');
		cy.get('.btn-tertiary').eq(0).should('be.visible').should('not.be.disabled').click({force:true});

		action.viewThread(cy);
		action.viewSettings(cy);
	})

})