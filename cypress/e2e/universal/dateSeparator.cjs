class DateSeparator {
    date;

    constructor(daysAgo) {
        this.date = new Date();
        this.date.setDate(this.date.getDate() - daysAgo);
    }

    printDate(separator) {
        return this.getYear() + separator + this.getMonth() + separator + this.getDay();
    }

    getDay() {
        return String(this.date.getDate()).padStart(2, '0');
    }

    getMonth() {
        return String(this.date.getMonth() + 1).padStart(2, '0'); //January is 0!
    }

    getYear() {
        return String(this.date.getFullYear());
    }
}
module.exports = DateSeparator;