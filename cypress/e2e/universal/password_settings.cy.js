import action from '../../support/fixture'

const urlName = Cypress.env('app_url');

describe('Profile test', function () {

	it('Profile password change test after login (english)', function(){
		cy.viewport(1920, 1080);
		cy.server();
		cy.route('POST', '/dashboard/get-email-traffic').as('post');
		cy.route('POST', '/config/profile/update-profile-settings').as('update');
		cy.route('POST', '/config/profile/update-password-settings').as('updatePass');
		action.login2(cy, 'q1w2e3r4', 'jtari@csavarker.hu', urlName);
		action.english(cy);
		action.password(cy, 'q1w2e3r4', 't5z6u7i8');
		action.login(cy, 't5z6u7i8', 'jtari@csavarker.hu');
	})

	it('Profile password change test after login (hungarian)', function(){
		cy.viewport(1920, 1080);
		cy.server();
		cy.route('POST', '/dashboard/get-email-traffic').as('post');
		cy.route('POST', '/config/profile/update-profile-settings').as('update');
		cy.route('POST', '/config/profile/update-password-settings').as('updatePass');
		action.login2(cy, 't5z6u7i8', 'jtari@csavarker.hu', urlName);
		action.hungarian(cy);
		action.password(cy, 't5z6u7i8', 'q1w2e3r4');
		action.login(cy, 'q1w2e3r4', 'jtari@csavarker.hu');
	})
})