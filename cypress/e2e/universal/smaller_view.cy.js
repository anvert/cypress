import action from "../../support/fixture";

const urlName = Cypress.env('app_url');
// const vat = '506718';

describe('Smaller view test', function () {

    it.skip('Smaller viewport test (1200x700)', function () {
        cy.viewport(1200, 700);
        cy.server();
        cy.route('POST', '/dashboard/get-email-traffic').as('post');
        cy.route('POST', '/report/filtering-email-productivity').as('filt');
        cy.route('POST', '/threads/get-threads').as('threads');
        cy.route('GET', '/permissions/init-data').as('perm');
        cy.route('GET', '/config/messenger/get-fb-data').as('messenger');
        cy.route('POST', '/config/profile/update-user-n-company-data').as('updateProf');
        cy.route('GET', '/config/get-filters').as('getFilt');
        cy.route('POST', '/threads/thread-emails').as('email');
        cy.route('POST', '/closed_thread/add').as('closed');
        cy.route('POST', '/deleted_thread/add').as('trash');

        action.login2(cy, 'q1w2e3r4', 'jtari@csavarker.hu', urlName);
        action.save(cy, 'Favourite');
        action.statisticWoN(cy);

        cy.get('ul#main-menu-navigation li ul li a').eq(0).click({force: true});
        cy.wait(6000);
        /*cy.wait('@post', {timeout: 20000}).should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        });*/
        cy.get('button.btn-link').eq(0).click({force: true});
        cy.get('button.btn-normal-primary-with-shadow').eq(0).should('be.visible').should('not.be.disabled');
        cy.get('.btn-tertiary').should('be.visible').should('not.be.disabled').click({force: true});
        cy.wait(6000);

        cy.get('ul#main-menu-navigation li ul li a').eq(1).click({force: true});
        cy.get('button.btn-link').click({force: true});
        cy.get('button.btn-normal-primary-with-shadow').eq(0).should('be.visible').should('not.be.disabled');
        cy.get('.btn-tertiary').should('be.visible').should('not.be.disabled').click({force: true});

        action.moveLetterToClosed(cy);
        action.moveLetterToDeleted(cy);
        action.viewThread(cy);
        action.viewSettings(cy);

        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.get('a.dropdown-item.dusk-filter').eq(0).click();

        cy.wait(2000);
        cy.get(':nth-child(1) > .text-right > .d-flex > :nth-child(2) > .danger').eq(0).click();
        cy.get('.btn-warning-2').should('not.be.disabled');
        cy.get('.btn-tertiary-modal').should('not.be.disabled').click();
    })

    it('Smaller viewport test (1200x600)', function () {
        cy.viewport(1200, 600);
        cy.server();
        cy.route('POST', '/dashboard/get-email-traffic').as('post');
        cy.route('POST', '/report/filtering-email-productivity').as('filt');
        cy.route('POST', '/threads/get-threads').as('threads');
        cy.route('GET', '/permissions/init-data').as('perm');
        cy.route('GET', '/config/messenger/get-fb-data').as('messenger');
        cy.route('POST', '/config/profile/update-user-n-company-data').as('updateProf');
        cy.route('GET', '/config/get-filters').as('getFilt');
        cy.route('POST', '/threads/thread-emails').as('email');
        cy.route('POST', '/closed_thread/remove').as('closedRemove');
        cy.route('POST', '/deleted_thread/remove').as('deletedRemove');

        action.login2(cy, 'q1w2e3r4', 'jtari@csavarker.hu', urlName);
        action.statisticWoN(cy);

        cy.get('ul#main-menu-navigation li ul li a').eq(0).click({force: true}).then(() => {
            cy.wait(3000);
        });
        cy.wait(6000);
        cy.get('button.btn-link').eq(0).click({force: true});
        cy.get('button.btn-normal-primary-with-shadow').eq(0).should('be.visible').should('not.be.disabled');
        cy.get('.btn-tertiary').should('be.visible').should('not.be.disabled').click({force: true});

        cy.get('ul#main-menu-navigation li ul li a').eq(1).click({force: true});
        cy.get('button.btn-link').click({force: true});
        cy.get('button.btn-normal-primary-with-shadow').eq(0).should('be.visible').should('not.be.disabled');
        cy.get('.btn-tertiary').should('be.visible').should('not.be.disabled').click({force: true});

        action.viewThread(cy);
        action.viewSettings(cy);

        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.get('a.dropdown-item.dusk-filter').eq(0).click();
        /*cy.wait('@messenger').should((xhr) => {
            expect(xhr.status, 'successful GET').to.equal(200)
        })*/
        cy.wait(2000);
        cy.get(':nth-child(1) > .text-right > .d-flex > :nth-child(2) > .danger').eq(0).click();
        cy.get('.btn-warning-2').should('not.be.disabled');
        cy.get('.btn-tertiary-modal').should('not.be.disabled').click();

        action.restoreThreads(cy, 'Lezárt', '@closedRemove');
        action.restoreThreads(cy, 'Törölt', '@deletedRemove');
    })

})