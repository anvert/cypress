import action from '../../support/fixture'

const urlName = Cypress.env('app_url');

const URLS = {
    activity:'https://leziter.anvert.com/report/daily_email_prod',
    reactionTime:'https://leziter.anvert.com/report/reaction_time',
    opinionAnalysis:'https://leziter.anvert.com/opinion-analysis',
    emotionAnalysis:'https://leziter.anvert.com/emotion-analysis',
    drafts:'https://leziter.anvert.com/drafts',
    assigned:'https://leziter.anvert.com/assigned',
    threads:'https://leziter.anvert.com/threads',
    profile:'https://leziter.anvert.com/config/profile',
    domains:'https://leziter.anvert.com/config/domains',
    emailAccounts:'https://leziter.anvert.com/config/email_accounts',
    report:'https://leziter.anvert.com/config/report',
    permissions:'https://leziter.anvert.com/permissions',
    filters:'https://leziter.anvert.com/config/filters',
    autoNotifications:'https://leziter.anvert.com/config/auto-notifications',
    subscription:'https://leziter.anvert.com/config/subscription'
};

describe('Admin page test', function () {

    it('Test the admin page', function () {
        cy.viewport(1920, 1080);
        cy.server();
        cy.route('POST', '/dashboard/get-email-traffic').as('post');
        cy.route('POST', '/config/profile/update-profile-settings').as('update');
        cy.route('GET', '/horizon/api/workload').as('horizon');

        cy.visit(urlName);
        cy.url().should('include', '/login');
        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span').click();
        cy.get('a.dropdown-item.dusk-profile').next().click();

        action.login(cy, 'Titkos123?;', 'admin@anvert.com');

        cy.url().should('include', '/admin');
        cy.wait(2000);
        cy.get('.nav-item .nav-link').eq(0).click();
        cy.url().should('include', '/admin/users');
        cy.wait(2000);
        cy.get('.nav-item .nav-link').eq(1).click();
        cy.url().should('include', '/admin/queue');
        cy.wait(2000);
        cy.get('.nav-item .nav-link').eq(2).click();
        cy.url().should('include', '/admin/jobs');
        cy.wait(2000);
        cy.go('back');
        cy.url().should('include', '/admin/queue');
        cy.get('.nav-item .nav-link').eq(3).click();
        cy.url().should('include', '/admin/process-mails');
        cy.wait(2000);
        cy.get('.nav-item .nav-link').eq(4).click();
        cy.url().should('include', '/admin/runjobs');
        cy.wait(2000);
        cy.get('.nav-item .nav-link').eq(5).click();
        cy.url().should('include', '/job-statistics');
        cy.wait(2000);
        cy.get('.nav-item .nav-link').eq(6).click();
        cy.url().should('include', '/schedule');
        cy.wait(2000);
        cy.go('back');
        cy.url().should('include', '/admin/job-statistics');
        cy.wait(2000);
    });

})