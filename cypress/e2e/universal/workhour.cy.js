import action from '../../support/fixture'

const urlName = Cypress.env('app_url');
const email = Cypress.env('newEmailRegister');
const name = Cypress.env('newNameRegister');
const psw = Cypress.env('newPswregister');

describe('Workhour test', function () {

    it('Default workhour test (english)', function () {
        cy.viewport(1920, 1080);
        cy.server();
        cy.route('POST', '/dashboard/get-email-traffic').as('post');
        cy.route('POST', '/report/filtering-reaction-time').as('filt');
        cy.route('POST', '/config/profile/update-profile-settings').as('update');
        cy.route('POST', '/threads/search-pre-threads').as('mails');
        cy.route('POST', '/dashboard/get-neticle-top-list').as('emotion');
        cy.route('POST', '/config/save-domain').as('domain');
        cy.route('GET', '/config/init-groups-data').as('group');
        cy.route('GET', '/onboarding/init-data').as('initData');
        cy.route('POST', '/config/save-group').as('saveGroups');

        cy.visit(urlName);
        cy.url().should('include', '/login');
        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span').click();
        cy.get('a.dropdown-item.dusk-profile').first().click();

        cy.get('a.btn-link.underline').click();
        cy.url().should('include', '/register');

        action.register(cy, name, name, email, psw);
        cy.wait(3000);
        action.onboarding(cy, 'demokovacsjulia@gmail.com');
        cy.wait(5000);

        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.get('a.dropdown-item.dusk-report-config').click();
        cy.wait('@group').should((xhr) => {
            expect(xhr.status, 'successful GET').to.equal(200)
        })

        cy.get('table tbody tr td').eq(1).should('contain', '8:00');
        cy.get('table tbody tr td').eq(2).should('contain', '17:00');
        cy.get('table tbody tr td').eq(5).should('contain', '8:00');
        cy.get('table tbody tr td').eq(6).should('contain', '17:00');
        cy.get('table tbody tr td').eq(9).should('contain', '8:00');
        cy.get('table tbody tr td').eq(10).should('contain', '17:00');
        cy.get('table tbody tr td').eq(13).should('contain', '8:00');
        cy.get('table tbody tr td').eq(14).should('contain', '17:00');
        cy.get('table tbody tr td').eq(17).should('contain', '8:00');
        cy.get('table tbody tr td').eq(18).should('contain', '17:00');

        cy.get('span.ico-plus').eq(0).click();
        cy.get('input.form-control').eq(0).type('proba');
        cy.get('button.btn-normal-primary-with-shadow').click();
        cy.wait('@saveGroups').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(2000);
        cy.get('.primary.mr-1.ico-chewron-down').eq(0).click();
        cy.get('.card-header').eq(1).click();
        cy.get('table tbody tr td').eq(1).should('contain', '8:00');
        cy.get('table tbody tr td').eq(2).should('contain', '17:00');
        cy.get('table tbody tr td').eq(5).should('contain', '8:00');
        cy.get('table tbody tr td').eq(6).should('contain', '17:00');
        cy.get('table tbody tr td').eq(9).should('contain', '8:00');
        cy.get('table tbody tr td').eq(10).should('contain', '17:00');
        cy.get('table tbody tr td').eq(13).should('contain', '8:00');
        cy.get('table tbody tr td').eq(14).should('contain', '17:00');
        cy.get('table tbody tr td').eq(17).should('contain', '8:00');
        cy.get('table tbody tr td').eq(18).should('contain', '17:00');

        action.deleteProfile(cy, 'Delete permanently');
    })

    it('Default workhour test (hungarian)', function () {
        cy.viewport(1920, 1080);
        cy.server();
        cy.route('POST', '/dashboard/get-email-traffic').as('post');
        cy.route('POST', '/report/filtering-reaction-time').as('filt');
        cy.route('POST', '/config/profile/update-profile-settings').as('update');
        cy.route('POST', '/threads/search-pre-threads').as('mails');
        cy.route('POST', '/dashboard/get-neticle-top-list').as('emotion');
        cy.route('POST', '/config/save-domain').as('domain');
        cy.route('GET', '/config/init-groups-data').as('group');
        cy.route('GET', '/onboarding/init-data').as('initData');
        cy.route('POST', '/config/save-group').as('saveGroups');


        cy.visit(urlName);
        cy.url().should('include', '/login');
        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span').click();
        cy.get('a.dropdown-item.dusk-profile').next().click();

        cy.get('a.btn-link.underline').click();
        cy.url().should('include', '/register');

        action.register(cy, name, name, email, psw);
        cy.wait(3000);
        action.onboarding(cy, 'demotothlaszlo@gmail.com');
        cy.wait(5000);

        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.get('a.dropdown-item.dusk-report-config').click();
        cy.wait('@group').should((xhr) => {
            expect(xhr.status, 'successful GET').to.equal(200)
        })

        cy.get('table tbody tr td').eq(1).should('contain', '8:00');
        cy.get('table tbody tr td').eq(2).should('contain', '17:00');
        cy.get('table tbody tr td').eq(5).should('contain', '8:00');
        cy.get('table tbody tr td').eq(6).should('contain', '17:00');
        cy.get('table tbody tr td').eq(9).should('contain', '8:00');
        cy.get('table tbody tr td').eq(10).should('contain', '17:00');
        cy.get('table tbody tr td').eq(13).should('contain', '8:00');
        cy.get('table tbody tr td').eq(14).should('contain', '17:00');
        cy.get('table tbody tr td').eq(17).should('contain', '8:00');
        cy.get('table tbody tr td').eq(18).should('contain', '17:00');

        cy.get('span.ico-plus').eq(0).click();
        cy.get('input.form-control').eq(0).type('proba');
        cy.get('button.btn-normal-primary-with-shadow').click();
        cy.wait('@saveGroups').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(2000);
        cy.get('.primary.mr-1.ico-chewron-down').eq(0).click();
        cy.get('.card-header').eq(1).click();

        cy.get('table tbody tr td').eq(1).should('contain', '8:00');
        cy.get('table tbody tr td').eq(2).should('contain', '17:00');
        cy.get('table tbody tr td').eq(5).should('contain', '8:00');
        cy.get('table tbody tr td').eq(6).should('contain', '17:00');
        cy.get('table tbody tr td').eq(9).should('contain', '8:00');
        cy.get('table tbody tr td').eq(10).should('contain', '17:00');
        cy.get('table tbody tr td').eq(13).should('contain', '8:00');
        cy.get('table tbody tr td').eq(14).should('contain', '17:00');
        cy.get('table tbody tr td').eq(17).should('contain', '8:00');
        cy.get('table tbody tr td').eq(18).should('contain', '17:00');

        action.deleteProfile(cy);
    })
})