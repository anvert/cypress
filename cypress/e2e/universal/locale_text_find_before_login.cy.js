import action from '../../support/fixture'

const urlName = Cypress.env('app_url');

describe('Check locale text test', function () {

	it('Locale. text checking (english)', function(){
		cy.viewport(1920, 1080);
		cy.server();
		cy.route('POST', '/dashboard/get-email-traffic').as('post');
		cy.route('POST', '/config/profile/update-profile-settings').as('update');

		action.localeText(cy, urlName);
	})

	it('Locale. text checking (hungarian)', function(){
		cy.viewport(1920, 1080);
		cy.server();
		cy.route('POST', '/dashboard/get-email-traffic').as('post');
		cy.route('POST', '/config/profile/update-profile-settings').as('update');

		action.localeText(cy, urlName);
	})
})