import action from '../../support/fixture'

const urlName = Cypress.env('app_url');

describe('Smaller view test', function () {
	
	it('Smaller viewport test (1200x700)', function(){
		cy.viewport(1200, 700);
		cy.server();
		cy.route('POST', '/dashboard/get-email-traffic').as('post');
		cy.route('POST', '/report/filtering-email-productivity').as('filt');
		cy.route('POST', '/threads/get-threads').as('threads');
		cy.route('GET', '/permissions/init-data').as('perm');
		cy.route('GET', '/threads/get-categories').as('messenger');
		cy.route('POST', '/config/profile/update-user-n-company-data').as('updateProf');
		cy.route('GET', '/config/get-filters').as('getFilt');
		cy.route('POST', '/threads/thread-emails').as('email');
		cy.route('POST', '/config/profile/update-profile-settings').as('update');
		cy.route('POST', '/config/profile/update-user-n-company-data').as('update2');
		cy.route('GET', '/onboarding/init-data').as('initData');

		action.login2(cy, 'q1w2e3r4', 'jtari@csavarker.hu', urlName);

		cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
		cy.get('a.dropdown-item.dusk-subscription').click();
		cy.get('.btn-small').click();
		cy.get('.btn-normal-primary-with-shadow').should('be.visible').should('not.be.disabled');
		cy.get('.btn-tertiary').should('be.visible').should('not.be.disabled').click({force:true});
	})

	it('Smaller viewport test (1200x600)', function(){
		cy.viewport(1200, 600);
		cy.server();
		cy.route('POST', '/dashboard/get-email-traffic').as('post');
		cy.route('POST', '/report/filtering-email-productivity').as('filt');
		cy.route('POST', '/threads/get-threads').as('threads');
		cy.route('GET', '/permissions/init-data').as('perm');
		cy.route('GET', '/threads/get-categories').as('messenger');
		cy.route('POST', '/config/profile/update-user-n-company-data').as('updateProf');
		cy.route('GET', '/config/get-filters').as('getFilt');
		cy.route('POST', '/threads/thread-emails').as('email');
		cy.route('POST', '/config/profile/update-profile-settings').as('update');
		cy.route('GET', '/onboarding/init-data').as('initData');
		cy.route('POST', '/config/profile/update-user-n-company-data').as('update2');

		action.login2(cy, 'q1w2e3r4', 'jtari@csavarker.hu', urlName);

		cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
		cy.get('a.dropdown-item.dusk-subscription').click();
		cy.get('.btn-small').click();
		cy.wait(2000);
		cy.get('.btn-normal-primary-with-shadow').scrollIntoView().should('be.visible').should('not.be.disabled');
		cy.get('.btn-tertiary').should('be.visible').should('not.be.disabled').click({force:true});
	})

})

		