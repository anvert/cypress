import action from '../../support/fixture';

const urlName = Cypress.env('app_url');

describe('Onboarding test', function () {
    it('Add gmail account', function () {
        cy.viewport(1920, 1080);
        cy.server();
        cy.route('GET', '/onboarding/init-data').as('initData');
        cy.route({
            url: '/dashboard/get-email-traffic',
            method: 'POST'
        }).as('emails');
        cy.visit(urlName);
        cy.url().should('include', `${urlName}/login`).then(() => {
            registration(cy);
        }).then(() => {
            chooseDomain(cy, false, 'Gmail');
        }).then(() => {
            onboardingPage(cy)
        });
        cy.url().should('include', urlName);
        cy.wait('@emails', {timeout: 20000}).should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200);
        });
        action.deleteProfile(cy);
        cy.url().should('include', `${urlName}/login`);
    })

    it('Add imap account', function () {
        cy.viewport(1920, 1080);
        cy.server();
        cy.route('GET', '/onboarding/init-data').as('initData');
        cy.visit(urlName);
        cy.url().should('include', `${urlName}/login`).then(() => {
            registration(cy);
        }).then(() => {
            chooseImapDomain(cy,
                'outlook.office365.com', 'TLS', '993',
                'smtp-mail.outlook.com', 'TLS', '587');
        }).then(() => {
            onboardingPage(cy);
        });
        cy.url().should('include', urlName);
        action.deleteProfile(cy);
        cy.url().should('include', `${urlName}/login`);
    });
});

function registration(cy) {
    cy.get('.col-sm-12 > .btn-link').click();
    cy.url().should('include', `${urlName}/register`);
    action.register(cy, 'miki', 'Móka Miki', 'mik@asd.com', 'q1w2e3r4');
}

function onboardingPage(cy) {
    // Invite type (Only invitation)
    cy.get('.wizard-item-container > :nth-child(1) > .wizard-card-row').click();
    cy.wait(2000);
    cy.get('.card-body > :nth-child(1) > .wizard-btn > .btn-normal-primary-with-shadow').click();
    cy.wait(2000);
    cy.get('.flex-shrink-1 > .row > :nth-child(1) > .form-group > .form-control').type('mik@asd.com');
    cy.wait(2000);
    cy.get('#step3 > .card > .card-content > .card-body > :nth-child(1) > .step-wrapper > .wizard-btn > .btn-normal-primary-with-shadow').click();
    cy.wait(2000);
    cy.get('.flex-shrink-1 > .btn-normal-primary-with-shadow').click();
    cy.wait(2000);
    cy.get('#step5 > .card > .card-content > .card-body > :nth-child(1) > .step-wrapper > .wizard-btn > .btn-normal-primary-with-shadow').click();
    cy.wait(2000);
    cy.get('#step6 > .card > .card-content > .card-body > :nth-child(1) > .step-wrapper > .wizard-btn > .btn-normal-primary-with-shadow').click();
    cy.wait(2000);
    cy.get('.card-body > .wizard-btn > .btn-normal-primary-with-shadow').click();
}

function chooseDomain(cy, domainIsCustom, domainName) {
    // Onboarding page
    // Select domain
    cy.wait(5000).then(() => {
        if (!domainIsCustom) {
            cy.get('.d-flex.flex-row.flex-wrap > .wizard-item.btn-tertiary-gray').each((el) => {
                if (el.text().trim().includes(domainName)) {
                    el.click();
                }
                cy.log(el.text());
            });
        }

        if (domainIsCustom) {
            cy.get('.col-4 > .v-select').click();
            cy.get('.vs__dropdown-option.vs__dropdown-option').each((el) => {
                if (el.text().trim().includes(domainName)) {
                    el.click();
                }
            });
        }
    }).then(() => {
        /*if (!successful) {
            expect(1).to.equal(2, 'The parameters of the function onboarding might be wrong.');
        }*/

        cy.wait(2000);
        cy.get('.btn-normal-primary-with-shadow').eq(1).click();
        cy.wait(2000);
    });
}

function chooseImapDomain(cy, imapServ, imapEncrypt, imapPort, smtpServ, smtpEncrypt, smtpPort,) {
    // Onboarding page
    cy.url().should('include', '/onboarding');
    cy.wait(2000);
    // Select domain
    cy.get('.wizard-item-details > .btn-link').click();
    cy.get(':nth-child(2) > :nth-child(1) > .form-group > .form-control').type(imapServ);
    cy.get(':nth-child(2) > :nth-child(2) > .row > :nth-child(1) > .form-group > .v-select').type(imapEncrypt);
    cy.get('#vs2__listbox > li').first().click();
    cy.get(':nth-child(2) > :nth-child(2) > .row > :nth-child(2) > .form-group > .form-control').type(imapPort);
    cy.get(':nth-child(4) > :nth-child(1) > .form-group > .form-control').type(smtpServ);
    cy.get(':nth-child(4) > :nth-child(2) > .row > :nth-child(1) > .form-group > .v-select').type(smtpEncrypt);
    cy.get('#vs3__listbox > li').first().click();
    cy.get(':nth-child(4) > :nth-child(2) > .row > :nth-child(2) > .form-group > .form-control').type(smtpPort);
    cy.wait(2000);
    cy.get('.btn-normal-primary-with-shadow').eq(1).click();
}