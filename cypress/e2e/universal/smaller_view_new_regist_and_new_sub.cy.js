import action from '../../support/fixture'

const urlName = Cypress.env('app_url');

describe('Smaller view test', function () {
	
	it('Smaller viewport test (1200x700)', function(){
		cy.viewport(1200, 700);
		cy.server();
		cy.route('POST', '/dashboard/get-email-traffic').as('post');
		cy.route('POST', '/report/filtering-email-productivity').as('filt');
		cy.route('POST', '/threads/get-threads').as('threads');
		cy.route('GET', '/permissions/init-data').as('perm');
		cy.route('GET', '/threads/get-categories').as('messenger');
		cy.route('POST', '/config/profile/update-user-n-company-data').as('updateProf');
		cy.route('GET', '/config/get-filters').as('getFilt');
		cy.route('POST', '/threads/thread-emails').as('email');
		cy.route('POST', '/config/profile/update-profile-settings').as('update');
		cy.route('POST', '/config/profile/update-user-n-company-data').as('update2');
		cy.route('GET', '/onboarding/init-data').as('initData');

		cy.visit(urlName);
		cy.url().should('include', '/login');
		cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span').click();	
		cy.get('a.dropdown-item.dusk-profile').next().click();

		cy.get('a.btn-link.underline').click();
		cy.url().should('include', '/register');

		action.register(cy, 'kovacsj', 'Kovács Júlia', 'demokovacsjulia@gmail.com', 't5z6u7i8');
		cy.wait(3000);
		action.onboarding(cy, 'demokovacsjulia@gmail.com');
		cy.wait(5000);

		cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
		cy.get('a.dropdown-item.dusk-profile').click({force: true});
		cy.get('div.content-body span.primary.pointer').eq(0).click();
		cy.wait(2000);
		cy.get('#company_name').clear().type('Teszt kft.');
		cy.get('#vat_number').clear().type('832528');
		cy.get('div.vs__actions svg path').eq(1).click({force:true});
		cy.get('input.vs__search').eq(0).type('Magyarország');
		cy.get('ul#vs1__listbox li#vs1__option-0').click({force:true});
		cy.get('#company_zip').clear().type('5177');
		cy.get('#company_city').clear().type('Pécs');
		cy.get('#company_address').clear().type('Virág utca 32.');
		cy.get('div[style=""] > .card-footer > .row > .col-12 > .btn-normal-primary-with-shadow').click();
		cy.wait('@update2').should((xhr) => {
			expect(xhr.status, 'successful POST').to.equal(200)
		})
		cy.wait(5000);

		cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
		cy.get('a.dropdown-item.dusk-subscription').click();
		cy.get('.btn-small.sub-btn').scrollIntoView().should('be.visible').should('not.be.disabled').click();
		cy.wait(3000);
		cy.get('.btn-normal-primary-with-shadow').scrollIntoView().should('be.visible');
		cy.get('.btn-tertiary').should('be.visible').should('not.be.disabled').click({force:true});

		action.deleteProfile(cy);
	})

	it('Smaller viewport test (1200x600)', function(){
		cy.viewport(1200, 600);
		cy.server();
		cy.route('POST', '/dashboard/get-email-traffic').as('post');
		cy.route('POST', '/report/filtering-email-productivity').as('filt');
		cy.route('POST', '/threads/get-threads').as('threads');
		cy.route('GET', '/permissions/init-data').as('perm');
		cy.route('GET', '/threads/get-categories').as('messenger');
		cy.route('POST', '/config/profile/update-user-n-company-data').as('updateProf');
		cy.route('GET', '/config/get-filters').as('getFilt');
		cy.route('POST', '/threads/thread-emails').as('email');
		cy.route('POST', '/config/profile/update-profile-settings').as('update');
		cy.route('GET', '/onboarding/init-data').as('initData');
		cy.route('POST', '/config/profile/update-user-n-company-data').as('update2');

		cy.visit(urlName);
		cy.url().should('include', '/login');
		cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span').click();	
		cy.get('a.dropdown-item.dusk-profile').next().click();

		cy.get('a.btn-link.underline').click();
		cy.url().should('include', '/register');

		action.register(cy, 'tothl', 'Tóth László', 'demotothlaszlo@gmail.com', 't5z6u7i8');
		cy.wait(3000);
		action.onboarding(cy, 'demotothlaszlo@gmail.com');
		cy.wait(5000);

		cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
		cy.get('a.dropdown-item.dusk-profile').click({force: true});
		cy.get('div.content-body span.primary.pointer').eq(0).click();
		cy.wait(2000);
		cy.get('#company_name').clear().type('Test kft.');
		cy.get('#vat_number').clear().type('832528');
		cy.get('div.vs__actions svg path').eq(1).click({force:true});
		cy.get('input.vs__search').eq(0).type('Magyarország');
		cy.get('ul#vs1__listbox li#vs1__option-0').click({force:true});
		cy.get('#company_zip').clear().type('5177');
		cy.get('#company_city').clear().type('Pécs');
		cy.get('#company_address').clear().type('Virág utca 32.');
		cy.get('div[style=""] > .card-footer > .row > .col-12 > .btn-normal-primary-with-shadow').click();
		cy.wait('@update2').should((xhr) => {
			expect(xhr.status, 'successful POST').to.equal(200)
		})
		cy.wait(5000);

		cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
		cy.get('a.dropdown-item.dusk-subscription').click();
		cy.get('.btn-small.sub-btn').scrollIntoView().should('be.visible').should('not.be.disabled').click();
		cy.wait(3000);
		cy.get('.btn-normal-primary-with-shadow').scrollIntoView().should('be.visible');
		cy.get('.btn-tertiary').should('be.visible').should('not.be.disabled').click({force:true});

		action.deleteProfile(cy);
	})

})

		