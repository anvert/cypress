import action from '../../support/fixture'

const urlName = Cypress.env('app_url');

describe('Login test', function () {

    it('Test the login page in english language with wrong and correct inputs', function () {
        cy.viewport(1920, 1080);
        cy.server();
        cy.route('POST', '/dashboard/get-email-traffic').as('post');
        cy.visit(urlName);
        cy.url().should('include', '/login');

        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span').click();
        cy.get('a.dropdown-item.dusk-profile').first().click();

        cy.get('.btn-big-primary-with-shadow-modal').click();
        for (var i = 0; i < 2; i++) {
            cy.get('.requirements').eq(i).should('exist');
        }

        action.login(cy, 'abcdefgh', 'random@random.hu')
        cy.get('.requirements').should('contain', 'Username and password pair is incorrect.');

        action.login(cy, 'abcdefgh', 'jtari@csavarker.hu');
        cy.get('.requirements').should('contain', 'Invalid account data.');

        action.login(cy, 'q1w2e3r4', 'jtari@csavarker.hu');
        cy.wait('@post').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200);
        })
    });

    it('Test the login page in hungarian language with wrong and correct inputs', function () {
        cy.viewport(1920, 1080);
        cy.server();
        cy.route('POST', '/dashboard/get-email-traffic').as('post');
        cy.visit(urlName);
        cy.url().should('include', '/login');

        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span').click();
        cy.get('a.dropdown-item.dusk-profile').next().click();

        cy.get('.btn-big-primary-with-shadow-modal').click();
        for (var i = 0; i < 2; i++) {
            cy.get('.requirements').eq(i).should('exist');
        }

        action.login(cy, 'abcdefgh', 'random@random.hu')
        cy.get('.requirements').should('contain', 'A felhasználónév és jelszó páros nem megfelelő.');

        action.login(cy, 'abcdefgh', 'jtari@csavarker.hu');
        cy.get('.requirements').should('contain', 'Hibás bejelentkezési adatok.');

        action.login(cy, 'q1w2e3r4', 'jtari@csavarker.hu');
        cy.wait('@post').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
    })
})