import action from '../../support/fixture'

const urlName = Cypress.env('app_url');

describe('Check locale text test', function () {

	it('Locale. text checking (english)', function(){
		cy.viewport(1920, 1080);
		cy.server();
		cy.route('POST', '/dashboard/get-email-traffic').as('post');
		cy.route('POST', '/config/profile/update-profile-settings').as('update');
		cy.route('POST', '/report/filtering-email-productivity').as('filt');
		cy.route('POST', '/threads/thread-emails').as('email');
		cy.route('POST', '/closed_thread/add').as('closed');
		cy.route('POST', '/deleted_thread/add').as('trash');
		cy.route('POST', '/closed_thread/remove').as('remove');
		cy.route('POST', '/threads/get-threads').as('threads');

		action.login2(cy, 'q1w2e3r4', 'jtari@csavarker.hu', urlName);
		action.english(cy);
		action.moveLetterToClosed(cy);
		action.moveLetterToDeleted(cy);
		action.localeThread(cy);
	})

	it('Locale. text checking (hungarian)', function(){
		cy.viewport(1920, 1080);
		cy.server();
		cy.route('POST', '/dashboard/get-email-traffic').as('post');
		cy.route('POST', '/config/profile/update-profile-settings').as('update');
		cy.route('POST', '/report/filtering-email-productivity').as('filt');
		cy.route('POST', '/threads/thread-emails').as('email');
		cy.route('POST', '/closed_thread/remove').as('closedRemove');
		cy.route('POST', '/deleted_thread/remove').as('deletedRemove');
		cy.route('POST', '/threads/get-threads').as('threads');

		action.login2(cy, 'q1w2e3r4', 'jtari@csavarker.hu', urlName);
		action.hungarian(cy);
		action.localeThread(cy);
		action.restoreThreads(cy, 'Lezárt', '@closedRemove');
		action.restoreThreads(cy, 'Törölt', '@deletedRemove');
	})
})