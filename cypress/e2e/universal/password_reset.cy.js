import action from '../../support/fixture'

const urlName = Cypress.env('app_url');
const text = "We can't find a user with that email address.";

describe('Password reset test', function () {

    it('Test the password reset page in english ', function () {
        cy.viewport(1920, 1080);
        cy.server();
        cy.route('POST', '/dashboard/get-email-traffic').as('post');
        cy.route('POST', '/config/profile/update-profile-settings').as('update');
        cy.route('POST', '/config/save-domain').as('domain');
        cy.route('GET', '/onboarding/init-data').as('initData');

        cy.visit(urlName);
        cy.url().should('include', '/login');
        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span').click();
        cy.get('a.dropdown-item.dusk-profile').first().click();

        cy.get('a.btn-link.underline').click();
        cy.url().should('include', '/register');

        action.register(cy, 'tothl', 'Tóth László', 'demotothlaszlo@gmail.com', 't5z6u7i8');
        cy.wait(3000);
        action.onboarding(cy, 'demotothlaszlo@gmail.com');
        cy.wait(3000);

        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.wait(3000);
        cy.get('a.dropdown-item').eq(7).click();
        cy.wait(3000);

        cy.get('.dropdown-toggle').click();
        cy.get('[href="' + urlName + '/logout"]').first().click();
        cy.wait(3000);

        cy.get('.dropdown-toggle').click();
        cy.get('[href="' + urlName + '/lang/en"]').click();

        cy.get('a.btn-link').eq(0).click();
        cy.url().should('include', '/reset');

        cy.get('.btn-big-primary-with-shadow').click();
        cy.get('.requirements').should('contain', 'The Email field is required.');

        cy.get('#email').type('random@gmail.hu');
        cy.get('form').eq(0).submit();
        cy.get('.requirements').should('contain', text);

        cy.get('#email').type('demotothlaszlo@gmail.com');
        cy.get('form').eq(0).submit();
        cy.get('h3.h3').should('contain', 'We have sent your password reset link!');

        cy.get('.btn-link.underline').click();
        cy.wait(2000);
        action.login(cy, 't5z6u7i8', 'demotothlaszlo@gmail.com');
        cy.wait(3000);
        action.deleteProfile(cy, 'Delete permanently');
    })

    it('Test the password reset page in hungarian ', function () {
        cy.viewport(1920, 1080);
        cy.server();
        cy.route('POST', '/dashboard/get-email-traffic').as('post');
        cy.route('POST', '/config/profile/update-profile-settings').as('update');
        cy.route('POST', '/config/save-domain').as('domain');
        cy.route('GET', '/onboarding/init-data').as('initData');

        cy.visit(urlName);
        cy.url().should('include', '/login');
        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span').click();
        cy.get('a.dropdown-item.dusk-profile').next().click();

        cy.get('a.btn-link.underline').click();
        cy.url().should('include', '/register');

        action.register(cy, 'kovacsj', 'Kovács Júlia', 'demokovacsjulia@gmail.com', 't5z6u7i8');
        cy.wait(3000);
        action.onboarding(cy, 'demokovacsjulia@gmail.com');
        cy.wait(3000);

        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.wait(3000);
        cy.get('a.dropdown-item').eq(7).click();
        cy.wait(3000);

        cy.get('.dropdown-toggle').click();
        cy.get('[href="' + urlName + '/logout"]').first().click();
        cy.wait(3000);

        cy.get('a.btn-link').eq(0).click();
        cy.url().should('include', '/reset');

        cy.get('.btn-big-primary-with-shadow').click();
        cy.get('.requirements').should('contain', 'A(z) Email mező kitöltése kötelező.');

        cy.get('#email').type('random@gmail.hu');
        cy.get('form').eq(0).submit();
        cy.get('.requirements').should('contain', 'Nem találunk ilyen e-mail címmel rendelkező felhasználót.');

        cy.get('#email').type('demokovacsjulia@gmail.com');
        cy.get('form').eq(0).submit();
        cy.get('h3.h3').should('contain', 'E-mailben elküldtük a jelszó visszaállító linket!');

        cy.get('.btn-link.underline').click();
        cy.wait(2000);
        action.login(cy, 't5z6u7i8', 'demokovacsjulia@gmail.com');
        cy.wait(3000);
        action.deleteProfile(cy);
    })
})