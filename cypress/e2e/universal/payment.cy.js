import action from "../../support/fixture";

const urlName = Cypress.env('app_url');

describe('Subscribe test', function () {

	it('European country and hungary gross test(english)', function (){
		cy.server();
		cy.route('POST', '/dashboard/get-email-traffic').as('post');
		cy.route('POST', '/config/profile/update-profile-settings').as('update');
		cy.route('POST', '/config/profile/update-user-n-company-data').as('update2');
		cy.route('POST', '/config/test-email-account').as('account');
		cy.route('POST', '/config/save-email-account').as('save');
		cy.route('POST', '/threads/search-pre-threads').as('mails');
		cy.route('POST', '/dashboard/get-neticle-top-list').as('emotion');

		cy.visit(urlName);
		cy.url().should('include', '/login');
		cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span').click();	
		cy.get('a.dropdown-item.dusk-profile').first().click();
		
		cy.get('.col-sm-12 > .btn-link').click();
		cy.url().should('include', '/register');

		action.register(cy, 'tothl', 'Tóth László', 'demotothlaszlo@gmail.com', 't5z6u7i8');
		cy.wait(3000);
		action.onboarding(cy, 'demotothlaszlo@gmail.com');
		cy.wait(6000);

		action.english(cy);
		cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
		cy.get('a.dropdown-item.dusk-profile').click({force: true});
		cy.wait(3000);
		cy.get('div.content-body span.primary.pointer').eq(0).click();
		cy.wait(3000);
		cy.get('#company_name').clear().type('Teszt kft.');
		cy.get('#vat_number').clear().type('123123');
		cy.get('div.vs__actions svg path').eq(1).click({force: true});
		cy.get('input.vs__search').eq(0).type('Hungary');
		cy.get('ul#vs1__listbox li#vs1__option-0').click({force: true});
		cy.get('#company_zip').clear().type('1234');
		cy.get('#company_city').clear().type('Szeged');
		cy.get('#company_address').clear().type('Virág utca 32.');
		cy.get('[style=""] > .card-footer > .row > .col-12 > .btn-normal-primary-with-shadow').click();
		cy.wait('@update2').should((xhr) => {
			expect(xhr.status, 'successful POST').to.equal(200)
		})
		cy.wait(5000);

		action.deleteProfile(cy, 'Delete permanently');
	})

	it('European country and hungary gross test(hungarian)', function (){
		cy.server();
		cy.route('POST', '/dashboard/get-email-traffic').as('post');
		cy.route('POST', '/config/profile/update-profile-settings').as('update');
		cy.route('POST', '/config/profile/update-user-n-company-data').as('update2');
		cy.route('POST', '/config/test-email-account').as('account');
		cy.route('POST', '/config/save-email-account').as('save');
		cy.route('POST', '/threads/search-pre-threads').as('mails');
		cy.route('POST', '/dashboard/get-neticle-top-list').as('emotion');

		cy.visit(urlName);
		cy.url().should('include', '/login');
		cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span').click();	
		cy.get('a.dropdown-item.dusk-profile').first().click();

		cy.get('.col-sm-12 > .btn-link').click();
		cy.url().should('include', '/register');

		action.register(cy, 'kovacsj', 'Kovács Júlia', 'demokovacsjulia@gmail.com', 't5z6u7i8');
		cy.wait(3000);
		action.onboarding(cy, 'demokovacsjulia@gmail.com');
		cy.wait(6000);

		action.hungarian(cy);
		cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
		cy.get('a.dropdown-item.dusk-profile').click({force: true});
		cy.wait(3000);
		cy.get('div.content-body span.primary.pointer').eq(0).click();
		cy.wait(3000);
		cy.get('#company_name').clear().type('Teszt kft.');
		cy.get('#vat_number').clear().type('123123');
		cy.get('div.vs__actions svg path').eq(1).click({force: true});
		cy.get('input.vs__search').eq(0).type('Magyarország');
		cy.get('ul#vs1__listbox li#vs1__option-0').click({force: true});
		cy.get('#company_zip').clear().type('1234');
		cy.get('#company_city').clear().type('Szeged');
		cy.get('#company_address').clear().type('Virág utca 32.');
		cy.get('div[style=""] > .card-footer > .row > .col-12 > .btn-normal-primary-with-shadow').click();
		cy.wait('@update2').should((xhr) => {
			expect(xhr.status, 'successful POST').to.equal(200)
		})
		cy.wait(5000);

		action.deleteProfile(cy);
	})
})