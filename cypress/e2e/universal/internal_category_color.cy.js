import action from '../../support/fixture';

const urlName = Cypress.env('app_url');
const name = 'iocsai';
const filepathU = 'uploads/test_upload_internal_category2.xlsx';

describe('InternalCategory test', function () {

    it('Test the internal category correct working (hungarian)', function () {
        cy.viewport(1920, 1080);
        cy.server();
        cy.route('POST', '/dashboard/get-email-traffic').as('post');
        cy.route('POST', '/config/profile/update-profile-settings').as('update');
        cy.route('POST', '/closed_thread/add').as('closed');
        cy.route('POST', '/deleted_thread/add').as('trash');
        cy.route('POST', '/threads/thread-emails').as('email');
        cy.route('POST', '/threads/search-threads').as('search');
        cy.route('POST', '/threads/get-threads').as('searchDelete');
        cy.route('POST', '/config/save-filter').as('filter');
        cy.route('GET', '/config/init-email-accounts-data').as('acc');

        action.login2(cy, 'q1w2e3r4', 'jtari@csavarker.hu', urlName);
        cy.get('i.fa.fa-refresh').click();
        action.hungarian(cy);
        cy.wait(3000);

        // SZÍN NÉZÉS AMIKOR MÉG LÉTEZIK A FIÓK
        cy.get('ul li a.menu-item div span.menu-item').eq(2).click({force: true});
        cy.wait(10000);
        cy.get('.hidden-xs > :nth-child(1) > :nth-child(1) > :nth-child(1) > .form-group > .input-group-append').click();
        cy.get('.hidden-xs > :nth-child(1) > :nth-child(1) > :nth-child(2) > .advanced-search-options > :nth-child(1) > :nth-child(2) > .form-group > .form-control')
            .clear().type(name);
        cy.get('button.btn-normal-primary-with-shadow').eq(1).click();
        cy.wait('@search').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(4000);
        cy.get('#thread-list-scroll > :nth-child(3)')
            .find('.media-object.rounded-circle.text-circle.email-initial')
            .eq(0).invoke('attr', 'style')
            .should('eq', 'background-color: rgb(186, 191, 199);');

        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.get('a.dropdown-item.dusk-email-accounts').click();
        cy.wait('@acc').should((xhr) => {
            expect(xhr.status, 'successful GET').to.equal(200)
        })
        cy.wait(1000);
        cy.get('div.has-search input.has-search').eq(0).type('iocsai');
        cy.wait(4000);
        cy.get('#navbarDropdown img').eq(0).click({force: true});
        cy.get('.dropdown-item.more-menu-item button.btn-link-warning').eq(0).click({force: true});
        cy.wait(4000);
        cy.get('.btn-warning-2').click({force: true});
        cy.wait(10000);

        // SZÍN NÉZÉS A FIÓK TÖRLÉSE UTÁN
        cy.get('ul li a.menu-item div span.menu-item').eq(2).click({force: true});
        cy.wait(4000);
        cy.get('.hidden-xs > :nth-child(1) > :nth-child(1) > :nth-child(1) > .form-group > .input-group-append').click();
        cy.get('.hidden-xs > :nth-child(1) > :nth-child(1) > :nth-child(2) > .advanced-search-options > :nth-child(1) > :nth-child(2) > .form-group > .form-control')
            .clear().type(name);
        cy.get('button.btn-normal-primary-with-shadow').eq(1).click();
        cy.wait('@search').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(4000);
        cy.get('#thread-list-scroll > :nth-child(2) > .list-bg.list-overflow').should('not.exist');

        // FIÓK FELTÖLTÉSE
        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.get('a.dropdown-item.dusk-email-accounts').click();
        cy.wait(4000);
        cy.wait('@acc').should((xhr) => {
            expect(xhr.status, 'successful GET').to.equal(200)
        })
        cy.wait(1000);
        cy.get('button.btn-link').eq(1).click();
        cy.wait(4000);
        cy.get('div.vs__actions svg path').eq(1).click({force: true});
        cy.get('#vs1__option-0').click();
        cy.get('div.vs__actions svg path').eq(3).click({force: true});
        cy.get('#vs2__option-0').click();
        cy.get('[type="text"]').eq(0).type('INBOX');
        cy.get('[type="text"]').eq(1).type('Sent');
        cy.get('[type="text"]').eq(2).type('Trash');
        cy.get('[type="checkbox"]').eq(0).check({force: true});

        cy.get('input[type="file"]').attachFile(filepathU);
        cy.get('button.btn-normal-primary-with-shadow-modal').eq(1).click({force: true});
        /*cy.wait('@multi').should((xhr) => {
            expect(xhr.status, 'successful GET').to.equal(200)
        })*/
        cy.wait(20000);

        // SZÍN NÉZÉS AMIKOR ÚJRA LÉTEZIK A FIÓK
        cy.get('ul li a.menu-item div span.menu-item').eq(2).click({force: true});
        cy.wait(4000);
        cy.get('.hidden-xs > :nth-child(1) > :nth-child(1) > :nth-child(1) > .form-group > .input-group-append').click();
        cy.get('.hidden-xs > :nth-child(1) > :nth-child(1) > :nth-child(2) > .advanced-search-options > :nth-child(1) > :nth-child(2) > .form-group > .form-control')
            .clear().type(name);
        cy.get('button.btn-normal-primary-with-shadow').eq(1).click();
        cy.wait('@search').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.wait(4000);
        cy.get('#thread-list-scroll > :nth-child(3)')
            .find('.media-object.rounded-circle.text-circle.email-initial')
            .eq(0).invoke('attr', 'style')
            .should('eq', 'background-color: rgb(186, 191, 199);');
    })
})