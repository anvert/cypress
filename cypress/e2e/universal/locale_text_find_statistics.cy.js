import action from '../../support/fixture'

const urlName = Cypress.env('app_url');

describe('Check locale text test', function () {

	it('Locale. text checking (english)', function(){
		cy.viewport(1920, 1080);
		cy.server();
		cy.route('POST', '/dashboard/get-email-traffic').as('post');
		cy.route('POST', '/config/profile/update-profile-settings').as('update');
		cy.route('POST', '/report/filtering-email-productivity').as('filt');

		action.login2(cy, 'q1w2e3r4', 'jtari@csavarker.hu', urlName);
		action.english(cy);
		action.localeStatistics(cy);
	})

	it('Locale. text checking (hungarian)', function(){
		cy.viewport(1920, 1080);
		cy.server();
		cy.route('POST', '/dashboard/get-email-traffic').as('post');
		cy.route('POST', '/config/profile/update-profile-settings').as('update');
		cy.route('POST', '/report/filtering-email-productivity').as('filt');

		action.login2(cy, 'q1w2e3r4', 'jtari@csavarker.hu', urlName);
		action.hungarian(cy);
		action.localeStatistics(cy);
	})
})