import action from '../../support/fixture';
import dayjs from 'dayjs';

const urlName = Cypress.env('app_url');

describe('Login test', function () {


    it.skip('Loading and refreshing test in english', function () {
        cy.viewport(1920, 1080);
        cy.server();
        cy.route('POST', '/dashboard/get-email-traffic').as('post');
        cy.route('POST', '/report/filtering-email-productivity').as('filt');
        cy.route('POST', '/report/filtering-reaction-time').as('react');
        cy.route('POST', '/threads/get-threads').as('threads');
        cy.route('POST', '/config/subscription/subscription-status').as('subs');
        cy.route('GET', '/config/profile/show').as('prof');
        cy.route('GET', '/config/init-domain-data').as('domain');
        cy.route('GET', '/config/init-email-accounts-data').as('acc');
        cy.route('GET', '/config/init-groups-data').as('group');
        cy.route('GET', '/config/messenger/get-fb-data').as('messenger');
        cy.route('GET', '/config/get-filters').as('filters');
        cy.route('GET', '/permissions/init-data').as('perm');
        cy.route('POST', '/config/profile/update-profile-settings').as('update');

        cy.visit(urlName);
        cy.url().should('include', '/login');

        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span').click();
        cy.get('a.dropdown-item.dusk-profile').first().click();

        cy.get('.btn-big-primary-with-shadow-modal').click();
        for (var i = 0; i < 2; i++) {
            cy.get('.requirements').eq(i).should('exist');
        }

        action.login(cy, 'abcdefgh', 'random@random.hu');
        cy.get('.requirements').should('exist');

        action.login(cy, 'abcdefgh', 'jtari@csavarker.hu');
        cy.get('.requirements').should('exist');

        action.login(cy, 'q1w2e3r4', 'jtari@csavarker.hu');
        cy.wait('@post').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })

        cy.wait(3000);

        cy.get('i.fa.fa-refresh').click();
        cy.wait(3000);
        action.english(cy);
        cy.url().should('include', '');
        action.dash2(cy, 'E-mail count', 'Reply time', 'Sent', 'Received', 'All', 'Dashboard', 'Productivity', 'Reaction time', 'Not replied', 'Replied', 'Internal emails', 'Idle', 'Closed', 'Trash', 'Spam', 'Promotions', 'Message count', 'Text analysis', 'Sentiment analysis', 'Emotion analysis', 'Sentiment index');

        cy.get('ul#main-menu-navigation li a').eq(2).should('exist').click({force: true});
        cy.wait('@filt').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.url().should('include', '/report/daily_email_prod');
        cy.get('h2').contains('Productivity').should('exist');

        cy.get('ul#main-menu-navigation li a').eq(3).should('exist').click({force: true});
        cy.wait('@react').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.url().should('include', '/report/reaction_time');
        cy.get('h2').contains('Reaction time').should('exist');
        const todaysDate = dayjs().format('YYYY.MM.DD');
        cy.log(todaysDate);
        cy.get('div.email-productivity-datetime.text-center').eq(0).should('contain', todaysDate);

        cy.get('ul#main-menu-navigation li a').eq(4).should('exist');
        cy.get('ul#main-menu-navigation li ul li a').eq(0).should('exist').click({force: true});
        cy.wait('@post').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.url().should('include', '/opinion-analysis');
        cy.get('h1').contains('Sentiment analysis').should('exist');

        cy.get('ul#main-menu-navigation li ul li a').eq(1).should('exist').click({force: true});
        cy.wait('@post').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.url().should('include', '/emotion-analysis');
        cy.get('h1').contains('Emotion analysis').should('exist');

        cy.get('ul li a.menu-item div span.menu-item').eq(0).should('exist').click({force: true});
        cy.wait(2000);
        cy.wait('@threads').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.url().should('include', '/threads');

        cy.get('ul li a.menu-item div span.menu-item').eq(1).should('exist').click({force: true});
        cy.wait(2000);
        cy.wait('@threads').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.url().should('include', '/threads');

        cy.get('ul li a.menu-item div span.menu-item').eq(2).should('exist').click({force: true});
        cy.wait(2000);
        cy.wait('@threads').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.url().should('include', '/threads');

        cy.get('ul li a.menu-item div span.menu-item').eq(3).should('exist').click({force: true});
        cy.wait(2000);
        cy.wait('@threads').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.url().should('include', '/threads');

        cy.get('ul li a.menu-item div span.menu-item').eq(4).should('exist').click({force: true});
        cy.wait(2000);
        cy.wait('@threads').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.url().should('include', '/threads');

        cy.get('ul li a.menu-item div span.menu-item').eq(5).should('exist').click({force: true});
        cy.wait(2000);
        cy.wait('@threads').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.url().should('include', '/threads');

        cy.get('ul li a.menu-item div span.menu-item').eq(6).should('exist').click({force: true});
        cy.wait(2000);
        cy.wait('@threads').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.url().should('include', '/threads');

        cy.get('ul li a.menu-item div span.menu-item').eq(7).should('exist').click({force: true});
        cy.wait(2000);
        cy.wait('@threads').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.url().should('include', '/threads');

        cy.get('ul li a.menu-item div span.menu-item').eq(8).should('exist').click({force: true});
        cy.wait(2000);
        cy.wait('@threads').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.url().should('include', '/threads');

        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.get('a.dropdown-item.dusk-profile').click();
        cy.wait('@prof').should((xhr) => {
            expect(xhr.status, 'successful GET').to.equal(200)
        })

        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.get('a.dropdown-item.dusk-domain-settings').click({force: true});
        cy.wait('@domain').should((xhr) => {
            expect(xhr.status, 'successful GET').to.equal(200)
        })

        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.get('a.dropdown-item.dusk-email-accounts').click({force: true});
        cy.wait('@acc').should((xhr) => {
            expect(xhr.status, 'successful GET').to.equal(200)
        })

        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.get('a.dropdown-item.dusk-report-config').click({force: true});
        cy.wait('@group').should((xhr) => {
            expect(xhr.status, 'successful GET').to.equal(200)
        })

        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.get('a.dropdown-item').eq(4).click();
        cy.wait('@perm').should((xhr) => {
            expect(xhr.status, 'successful GET').to.equal(200)
        })

        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.get('a.dropdown-item.dusk-filter').eq(0).click();
        /*cy.wait('@messenger').should((xhr) => {
            expect(xhr.status, 'successful GET').to.equal(200)
        })*/
        cy.wait(3000);

        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.get('.dusk-filter').click();
        cy.wait('@filters').should((xhr) => {
            expect(xhr.status, 'successful GET').to.equal(200)
        })

        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.get('a.dropdown-item.dusk-subscription').click();
        cy.wait('@subs').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })

        cy.get('a.menu-item.mb-3').click({force: true}).then(link => {
            cy.request(link.prop('href')).its('status').should('eq', 200);
        })

        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.get('[href="' + urlName + '/logout"]').click();
        cy.url().should('include', '/login');

    })

    it('Loading and refreshing test in hungarian', function () {
        cy.viewport(1920, 1080);
        cy.server();
        cy.route('POST', '/dashboard/get-email-traffic').as('post');
        cy.route('POST', '/report/filtering-email-productivity').as('filt');
        cy.route('POST', '/report/filtering-reaction-time').as('react');
        cy.route('POST', '/threads/get-threads').as('threads');
        cy.route('POST', '/config/subscription/subscription-status').as('subs');
        cy.route('GET', '/config/profile/show').as('prof');
        cy.route('GET', '/config/init-domain-data').as('domain');
        cy.route('GET', '/config/init-email-accounts-data').as('acc');
        cy.route('GET', '/config/init-groups-data').as('group');
        cy.route('GET', '/config/messenger/get-fb-data').as('messenger');
        cy.route('GET', '/config/get-filters').as('filters');
        cy.route('GET', '/permissions/init-data').as('perm');
        cy.route('POST', '/config/profile/update-profile-settings').as('update');

        cy.visit(urlName);
        cy.url().should('include', '/login');

        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span').click();
        cy.get('a.dropdown-item.dusk-profile').next().click();

        cy.get('.btn-big-primary-with-shadow-modal').click();
        for (var i = 0; i < 2; i++) {
            cy.get('.requirements').eq(i).should('exist');
        }

        action.login(cy, 'abcdefgh', 'random@random.hu');
        cy.get('.requirements').should('exist');

        action.login(cy, 'abcdefgh', 'jtari@csavarker.hu');
        cy.get('.requirements').should('exist');

        action.login(cy, 'q1w2e3r4', 'jtari@csavarker.hu');
        cy.wait('@post').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })

        cy.wait(3000);

        cy.get('i.fa.fa-refresh').click();
        cy.wait(3000);
        action.hungarian(cy);
        cy.url().should('include', '');

        action.dash2(cy, 'E-mailek száma (db)', 'Válaszidő', 'Elküldött', 'Fogadott', 'Összes', 'Áttekintés', 'Aktivitás', 'Válaszidő', 'Megválaszolatlan', 'Megválaszolt', 'Belső levelezés', 'Elakadt', 'Lezárt', 'Törölt', 'Spam', 'Promóciók', 'Üzenetek száma (db)', 'Szövegelemzés', 'Hangulatelemzés', 'Érzelem elemzés', 'Hangulatindex');

        cy.get('ul#main-menu-navigation li a').eq(2).should('exist').click({force: true});
        cy.wait('@filt').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.url().should('include', '/report/daily_email_prod');
        cy.get('h2').contains('Aktivitás').should('exist');

        cy.get('ul#main-menu-navigation li a').eq(3).should('exist').click({force: true});
        cy.wait('@react').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.url().should('include', '/report/reaction_time');
        cy.get('h2').contains('Válaszidő').should('exist');
        const todaysDate = dayjs().format('YYYY.MM.DD');
        cy.log(todaysDate);
        cy.get('div.email-productivity-datetime.text-center').eq(0).should('contain', todaysDate);

        cy.get('ul#main-menu-navigation li a').eq(4).should('exist');
        cy.get('ul#main-menu-navigation li ul li a').eq(0).should('exist').click({force: true});
        cy.wait('@post').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.url().should('include', '/opinion-analysis');
        cy.get('h1').contains('Hangulatelemzés').should('exist');

        cy.get('ul#main-menu-navigation li ul li a').eq(1).should('exist').click({force: true});
        cy.wait('@post').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.url().should('include', '/emotion-analysis');
        cy.get('h1').contains('Érzelem elemzés').should('exist');

        cy.get('ul li a.menu-item div span.menu-item').eq(0).should('exist').click({force: true});
        cy.wait(2000);
        cy.wait('@threads').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.url().should('include', '/threads');

        cy.get('ul li a.menu-item div span.menu-item').eq(1).should('exist').click({force: true});
        cy.wait(2000);
        cy.wait('@threads').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.url().should('include', '/threads');

        cy.get('ul li a.menu-item div span.menu-item').eq(2).should('exist').click({force: true});
        cy.wait(2000);
        cy.wait('@threads').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.url().should('include', '/threads');

        cy.get('ul li a.menu-item div span.menu-item').eq(3).should('exist').click({force: true});
        cy.wait(2000);
        cy.wait('@threads').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.url().should('include', '/threads');

        cy.get('ul li a.menu-item div span.menu-item').eq(4).should('exist').click({force: true});
        cy.wait(2000);
        cy.wait('@threads').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.url().should('include', '/threads');

        cy.get('ul li a.menu-item div span.menu-item').eq(5).should('exist').click({force: true});
        cy.wait(2000);
        cy.wait('@threads').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.url().should('include', '/threads');

        cy.get('ul li a.menu-item div span.menu-item').eq(6).should('exist').click({force: true});
        cy.wait(2000);
        cy.wait('@threads').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.url().should('include', '/threads');

        cy.get('ul li a.menu-item div span.menu-item').eq(7).should('exist').click({force: true});
        cy.wait(2000);
        cy.wait('@threads').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.url().should('include', '/threads');

        cy.get('ul li a.menu-item div span.menu-item').eq(8).should('exist').click({force: true});
        cy.wait(2000);
        cy.wait('@threads').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
        cy.url().should('include', '/threads');

        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.get('a.dropdown-item.dusk-profile').click();
        cy.wait('@prof').should((xhr) => {
            expect(xhr.status, 'successful GET').to.equal(200)
        })

        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.get('a.dropdown-item.dusk-domain-settings').click({force: true});
        cy.wait('@domain').should((xhr) => {
            expect(xhr.status, 'successful GET').to.equal(200)
        })

        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.get('a.dropdown-item.dusk-email-accounts').click({force: true});
        cy.wait('@acc').should((xhr) => {
            expect(xhr.status, 'successful GET').to.equal(200)
        })

        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.get('a.dropdown-item.dusk-report-config').click({force: true});
        cy.wait('@group').should((xhr) => {
            expect(xhr.status, 'successful GET').to.equal(200)
        })

        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.get('a.dropdown-item').eq(4).click();
        cy.wait('@perm').should((xhr) => {
            expect(xhr.status, 'successful GET').to.equal(200)
        })

        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.get('a.dropdown-item.dusk-filter').eq(0).click();
        /*cy.wait('@messenger').should((xhr) => {
            expect(xhr.status, 'successful GET').to.equal(200)
        })*/
        cy.wait(3000);

        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.get('.dusk-filter').click();
        cy.wait('@filters').should((xhr) => {
            expect(xhr.status, 'successful GET').to.equal(200)
        })

        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.get('a.dropdown-item.dusk-subscription').click();
        cy.wait('@subs').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })

        cy.get('a.menu-item.mb-3').click({force: true}).then(link => {
            cy.request(link.prop('href')).its('status').should('eq', 200);
        })

        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.get('[href="' + urlName + '/logout"]').click();
        cy.url().should('include', '/login');
    })
})