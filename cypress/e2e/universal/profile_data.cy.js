import action from '../../support/fixture'

const urlName = Cypress.env('app_url');

describe('Profile test', function () {

	it('Profile test after login (english)', function(){
		cy.viewport(1920, 1080);
		cy.server();
		cy.route('POST', '/dashboard/get-email-traffic').as('post');
		cy.route('POST', '/config/profile/update-profile-settings').as('update');
		cy.route('POST', '/config/profile/update-user-n-company-data').as('updateProf');
		action.login2(cy, 'q1w2e3r4', 'jtari@csavarker.hu', urlName);
		action.english(cy);
		action.profile(cy, 'Kiss András', '85108190', 'Csavarker Kft', '5600', 'Békéscsaba', 'Andrássy út 23.', 'Germany');
	})

	it('Profile test after login (hungarian)', function(){
		cy.viewport(1920, 1080);
		cy.server();
		cy.route('POST', '/dashboard/get-email-traffic').as('post');
		cy.route('POST', '/config/profile/update-profile-settings').as('update');
		cy.route('POST', '/config/profile/update-user-n-company-data').as('updateProf');
		action.login2(cy, 'q1w2e3r4', 'jtari@csavarker.hu', urlName);
		action.hungarian(cy);
		action.profile(cy, 'Tari Jenő', '123132', 'Csavarker Plusz Kft', '6722', 'Szeged', 'Moszkvai krt 23. III. 15', 'Magyarország');
	})
})