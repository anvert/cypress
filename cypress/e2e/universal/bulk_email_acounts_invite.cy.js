import action from '../../support/fixture'

const urlName = Cypress.env('app_url');
const filepath = 'uploads/test_invite2.xlsx';

describe('Bulk email account invite test', function () {

    it('Test Bulk email account invite (hungarian)', function(){
        cy.viewport(1920, 1080);
        cy.server();
        cy.route('POST', '/dashboard/get-email-traffic').as('post');
        cy.route('POST', '/config/profile/update-profile-settings').as('update');
        cy.route('POST', '/closed_thread/add').as('closed');
        cy.route('POST', '/deleted_thread/add').as('trash');
        cy.route('POST', '/threads/thread-emails').as('email');
        cy.route('POST', '/threads/search-threads').as('search');
        cy.route('POST', '/threads/get-threads').as('searchDelete');
        cy.route('POST', '/config/save-filter').as('filter');
        cy.route('GET', '/config/init-email-accounts-data').as('acc');
        cy.route('GET', '/config/import-multiple-email-account-notification').as('multi');

        action.login2(cy, 'q1w2e3r4', 'jtari@csavarker.hu', urlName);
        action.hungarian(cy);
        cy.wait(1000);

        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.get('a.dropdown-item.dusk-email-accounts').click();
        cy.wait('@acc').should((xhr) => {
            expect(xhr.status, 'successful GET').to.equal(200)
        })
        cy.wait(1000);
        cy.get('div.has-search input.has-search').eq(0).type('eercsenyi');
        cy.wait(4000);
        cy.get('#navbarDropdown img').eq(0).click({force: true});
        cy.get('.dropdown-item.more-menu-item button.btn-link-warning').eq(0).click({force:true});
        cy.wait(4000);
        cy.get('.btn-warning-2').click({force:true});
        cy.wait(2000);
        cy.get('div.has-search input.has-search').eq(0).clear().type('elado');
        cy.wait(4000);
        cy.get('#navbarDropdown img').eq(0).click({force: true});
        cy.get('.dropdown-item.more-menu-item button.btn-link-warning').eq(0).click({force:true});
        cy.wait(4000);
        cy.get('.btn-warning-2').click({force:true});
        cy.wait(2000);
        cy.reload();

        cy.wait(5000);

        cy.get('button.btn-link').eq(1).click();
        cy.wait(4000);
        cy.get('div.vs__actions svg path').eq(1).click({force:true});
        cy.get('#vs1__option-0').click();
        cy.get('div.vs__actions svg path').eq(3).click({force:true});
        cy.get('#vs2__option-0').click();
        cy.get('[type="text"]').eq(0).type('INBOX');
        cy.get('[type="text"]').eq(1).type('Sent');
        cy.get('[type="text"]').eq(2).type('Trash');
        cy.get('[type="checkbox"]').eq(0).check({force: true});
        
        cy.get('input[type="file"]').attachFile(filepath);
        cy.get('button.btn-normal-primary-with-shadow-modal').eq(1).click();
        cy.wait('@multi').should((xhr) => {
            expect(xhr.status, 'successful GET').to.equal(200)
        })
    
        cy.wait(5000);

        cy.get('a.toast-link.text-left').click({force: true});
        cy.wait(2000);
        cy.get('div.has-search input.has-search').eq(0).type('eercsenyi');
        cy.wait(4000);
        cy.get('tbody tr td').eq(2).should('contain', 'eercsenyi@csavarker.hu');
        cy.wait(2000);
        cy.get('div.has-search input.has-search').eq(0).clear().type('elado');
        cy.wait(4000);
        cy.get('tbody tr td').eq(2).should('contain', 'elado@csavarker.hu');
        cy.get('div.has-search input.has-search').eq(0).clear();
        cy.wait('@multi').should((xhr) => {
            expect(xhr.status, 'successful POST').to.equal(200)
        })
    })
})