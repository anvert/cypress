import action from '../../support/fixture'

const urlName = Cypress.env('app_url');

describe('Dashboard test', function () {

    it('Dashboard check after login (english)', function () {
        cy.viewport(1920, 1080);
        cy.server();
        cy.route('POST', '/dashboard/get-email-traffic').as('post');
        cy.route('POST', '/config/profile/update-profile-settings').as('update');

        action.login2(cy, 'q1w2e3r4', 'jtari@csavarker.hu', urlName);
        action.english(cy);
        action.dash2(cy, 'E-mail count', 'Reply time', 'Sent', 'Received', 'All', 'Dashboard', 'Productivity', 'Reaction time', 'Not replied', 'Replied', 'Internal emails', 'Idle', 'Closed', 'Trash', 'Spam', 'Promotions', 'Message count', 'Text analysis', 'Sentiment analysis', 'Emotion analysis', 'Sentiment index');
    })

    it('Dashboard check after login (hungarian)', function () {
        cy.viewport(1920, 1080);
        cy.server();
        cy.route('POST', '/dashboard/get-email-traffic').as('post');
        cy.route('POST', '/config/profile/update-profile-settings').as('update');
        action.login2(cy, 'q1w2e3r4', 'jtari@csavarker.hu', urlName);
        action.hungarian(cy);
        action.dash2(cy, 'E-mailek száma (db)', 'Válaszidő', 'Elküldött', 'Fogadott', 'Összes', 'Áttekintés', 'Aktivitás', 'Válaszidő', 'Megválaszolatlan', 'Megválaszolt', 'Belső levelezés', 'Elakadt', 'Lezárt', 'Törölt', 'Spam', 'Promóciók', 'Üzenetek száma (db)', 'Szövegelemzés', 'Hangulatelemzés', 'Érzelem elemzés', 'Hangulatindex');
    })
})