import action from '../../support/fixture'

const urlName = Cypress.env('app_url');
const email = Cypress.env('newEmailRegister');
const name = Cypress.env('newNameRegister');
const psw = Cypress.env('newPswregister');

describe('Register test', function () {

    it('Test the register page in english ', function () {
        cy.viewport(1920, 1080);
        cy.server();
        cy.route('POST', '/dashboard/get-email-traffic').as('post');
        cy.route('POST', '/config/profile/update-profile-settings').as('update');
        cy.route('POST', '/config/save-domain').as('domain');
        cy.route('GET', '/onboarding/init-data').as('initData');

        cy.visit(urlName);
        cy.url().should('include', '/login');
        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span').click();
        cy.get('a.dropdown-item.dusk-profile').first().click();

        cy.get('a.btn-link.underline').click();
        cy.url().should('include', '/register');

        cy.get('button.btn-big-primary-with-shadow').eq(1).click({force: true});
        cy.get('.requirements');
        for (let i = 0; i < 7; i++) {
            cy.get('.requirements').eq(i).should('exist');
        }

        cy.get('#user_name').type(name);
        cy.get('#full_name').type(name);
        cy.get('#email').type(email);
        cy.get('#password').type(psw);
        cy.get('#password-confirm').type(psw);
        cy.get('#reg_math[data-value]').then(($inp) => {
            const cls = $inp.attr('data-value');
            cy.wrap($inp);
            cy.get('#reg_math').type(cls);
        })
        cy.get('div.card-body.login-card-body form').submit();
        cy.get('.requirements').should('contain', 'Accept the terms of use.');

        cy.get('#user_name').clear().type(name);
        cy.get('#full_name').clear().type(name);
        cy.get('#email').clear().type(email);
        cy.get('#password').clear().type(psw);
        cy.get('#password-confirm').clear().type(psw);
        cy.get('#reg_math').clear();
        cy.get('#reg_math[data-value]').then(($inp) => {
            const cls = $inp.attr('data-value');
            cy.wrap($inp);
            cy.get('#reg_math').type(cls - 1);
        })
        cy.get('[type="checkbox"]#custom_agree').check({force: true});
        cy.get('[type="checkbox"]#gmail_user').check({force: true});
        cy.get('div.card-body.login-card-body form').submit();
        cy.get('.requirements').should('contain', 'Incorrect total:');

        action.register(cy, name, name, email, psw);
        cy.wait(3000);
        action.onboarding(cy, 'demokovacsjulia@gmail.com');
        cy.wait(3000);
        action.deleteProfile(cy, 'Delete permanently');
    })

    it('Test the register page in hungarian ', function () {
        cy.viewport(1920, 1080);
        cy.server();
        cy.route('POST', '/dashboard/get-email-traffic').as('post');
        cy.route('POST', '/config/profile/update-profile-settings').as('update');
        cy.route('POST', '/config/save-domain').as('domain');
        cy.route('GET', '/onboarding/init-data').as('initData');

        cy.visit(urlName);
        cy.url().should('include', '/login');
        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span').click();
        cy.get('a.dropdown-item.dusk-profile').next().click();

        cy.get('a.btn-link.underline').click();
        cy.url().should('include', '/register');

        cy.get('button.btn-big-primary-with-shadow').eq(1).click({force: true});
        for (var i = 0; i < 7; i++) {
            cy.get('.requirements').eq(i).should('exist');
        }

        cy.get('#user_name').type(name);
        cy.get('#full_name').type(name);
        cy.get('#email').type(email);
        cy.get('#password').type(psw);
        cy.get('#password-confirm').type(psw);
        cy.get('#reg_math[data-value]').then(($inp) => {
            const cls = $inp.attr('data-value');
            cy.wrap($inp);
            cy.get('#reg_math').type(cls);
        })
        cy.get('div.card-body.login-card-body form').submit();
        cy.get('.requirements').should('contain', 'Fogadd el a felhasználási feltételeket.');

        cy.get('#user_name').clear().type(name);
        cy.get('#full_name').clear().type(name);
        cy.get('#email').clear().type(email);
        cy.get('#password').clear().type(psw);
        cy.get('#password-confirm').clear().type(psw);
        cy.get('#reg_math').clear();
        cy.get('#reg_math[data-value]').then(($inp) => {
            const cls = $inp.attr('data-value');
            cy.wrap($inp);
            cy.get('#reg_math').type(cls - 1);
        })
        cy.get('[type="checkbox"]#custom_agree').check({force: true});
        cy.get('[type="checkbox"]#gmail_user').check({force: true});
        cy.get('div.card-body.login-card-body form').submit();
        cy.get('.requirements').should('contain', 'Hibás összeg:');

        action.register(cy, name, name, email, psw);
        cy.wait(3000);
        action.onboarding(cy, 'demotothlaszlo@gmail.com');
        cy.wait(3000);
        action.deleteProfile(cy);
    })
})