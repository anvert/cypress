import action from '../../support/fixture'

const urlName = Cypress.env('app_url');

describe('Check locale text test', function () {

	it('Locale. text checking (english)', function(){
		cy.viewport(1920, 1080);
		cy.server();
		cy.route('POST', '/dashboard/get-email-traffic').as('post');
		cy.route('POST', '/config/profile/update-profile-settings').as('update');
		cy.route('POST', '/config/profile/update-user-n-company-data').as('update2');
		cy.visit(urlName);
		cy.url().should('include', '/login');
		cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span').click();
		cy.get('a.dropdown-item.dusk-profile').first().click();

		cy.get('.col-sm-12 > .btn-link').click();
		cy.url().should('include', '/register');
		action.register(cy, 'kovacsj', 'Kovács Júlia', 'demokovacsjulia@gmail.com', 't5z6u7i8');
		cy.wait(3000);
		action.onboarding(cy, 'demokovacsjulia@gmail.com');
  		cy.wait(5000);
		cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
		cy.get('a.dropdown-item.dusk-profile').click({force: true});
		cy.get('div.content-body span.primary.pointer').eq(0).click();
		cy.wait(2000);
		cy.get('#company_name').clear().type('Teszt kft.');
		cy.get('#vat_number').clear().type('123123');
		cy.get('#country').eq(0).type('Hungary');
		cy.get('ul#vs1__listbox li#vs1__option-0').click();
		cy.get('#company_zip').clear().type('1234');
		cy.get('#company_city').clear().type('Szeged');
		cy.get('#company_address').clear().type('Virág utca 32.');
		cy.get('div[style=""] > .card-footer > .row > .col-12 > .btn-normal-primary-with-shadow').click();
		cy.wait('@update2').should((xhr) => {
			expect(xhr.status, 'successful POST').to.equal(200)
		})
		cy.wait(5000);
  		
		cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
		cy.get('a.dropdown-item').should('not.contain', 'locale.');
		cy.get('a.dropdown-item.dusk-subscription').click();
		cy.get('h2').should('not.contain', 'locale.');
		cy.get('h3').should('not.contain', 'locale.');
		cy.get('.sub-label-title').should('not.contain', 'locale.');
		cy.get('.sub-label').should('not.contain', 'locale.');
		cy.get('.alert').should('not.contain', 'locale.');
		cy.get('h3.sub-card-title').should('not.contain', 'locale.');
		cy.get('table thead tr th').should('not.contain', 'locale.');
		cy.get('table tbody').should('not.contain', 'locale.');
		cy.get('#currency').should('not.contain', 'locale.').select('EUR');
		cy.get('#currency').should('not.contain', 'locale.').select('HUF');
		cy.get('#payment-method').should('not.contain', 'locale.').select('Wire transfer');
		cy.get('#payment-method').should('not.contain', 'locale.').select('Credit card');
		
		cy.get('.btn-small.sub-btn').should('not.contain', 'locale.').click();
		cy.wait(3000);
		cy.get('h3').should('not.contain', 'locale.');
		cy.get('.modal-container.modal-container-mobil.licence_modal').eq(1).find('span').should('not.contain', 'locale.');
		cy.get('.btn-tertiary').should('not.contain', 'locale.');
		cy.get('.btn-normal-primary-with-shadow').should('not.contain', 'locale.').click();
		cy.wait(5000);
		cy.get('h1').should('not.contain', 'locale.');
		cy.get('.col-md-8').find('span').should('not.contain', 'locale.');
		cy.get('#pay-btn').should('not.contain', 'locale.');
		action.deleteProfile(cy, 'Delete permanently');
	})

	it('Locale. text checking (hungarian)', function(){
		cy.viewport(1920, 1080);
		cy.server();
		cy.route('POST', '/dashboard/get-email-traffic').as('post');
		cy.route('POST', '/config/profile/update-profile-settings').as('update');
		cy.route('POST', '/config/profile/update-user-n-company-data').as('update2');
		cy.visit(urlName);
		cy.url().should('include', '/login');
		cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span').click();
		cy.get('a.dropdown-item.dusk-profile').first().click();

		cy.get('.col-sm-12 > .btn-link').click();
		cy.url().should('include', '/register');
		action.register(cy, 'tothl', 'Tóth László', 'demotothlaszlo@gmail.com', 't5z6u7i8');
		cy.wait(3000);
		action.onboarding(cy, 'demotothlaszlo@gmail.com');
		cy.wait(5000);

		action.hungarian(cy);
		cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
		cy.get('a.dropdown-item.dusk-profile').click();
		cy.get('div.content-body span.primary.pointer').eq(0).click();
		cy.wait(2000);
		cy.get('#company_name').clear().type('Test kft.');
		cy.get('#vat_number').clear().type('832528');
		cy.get('div.vs__actions svg path').eq(1).click({force: true});
		cy.get('input.vs__search').eq(0).type('Magyarország');
		cy.get('ul#vs1__listbox li#vs1__option-0').click({force: true});
		cy.get('#company_zip').clear().type('5177');
		cy.get('#company_city').clear().type('Pécs');
		cy.get('#company_address').clear().type('Virág utca 32.');
		cy.get('div[style=""] > .card-footer > .row > .col-12 > .btn-normal-primary-with-shadow').click();
		cy.wait('@update2').should((xhr) => {
			expect(xhr.status, 'successful POST').to.equal(200)
		})
		cy.wait(5000);

		cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
		cy.get('a.dropdown-item').should('not.contain', 'locale.');
		cy.get('a.dropdown-item.dusk-subscription').click();
		cy.get('h2').should('not.contain', 'locale.');
		cy.get('h3').should('not.contain', 'locale.');
		cy.get('.sub-label-title').should('not.contain', 'locale.');
		cy.get('.sub-label').should('not.contain', 'locale.');
		cy.get('.alert').should('not.contain', 'locale.');
		cy.get('h3.sub-card-title').should('not.contain', 'locale.');
		cy.get('table thead tr th').should('not.contain', 'locale.');
		cy.get('table tbody').should('not.contain', 'locale.');
		cy.get('#currency').should('not.contain', 'locale.').select('EUR');
		cy.get('#currency').should('not.contain', 'locale.').select('HUF');
		cy.get('#payment-method').should('not.contain', 'locale.').select('Banki átutalás');
		cy.get('#payment-method').should('not.contain', 'locale.').select('Bankkártya');

		cy.get('.btn-small.sub-btn').should('not.contain', 'locale.').click();
		cy.wait(3000);
		cy.get('h3').should('not.contain', 'locale.');
		cy.get('.modal-container.modal-container-mobil.licence_modal').eq(1).find('span').should('not.contain', 'locale.');
		cy.get('.btn-tertiary').should('not.contain', 'locale.');
		cy.get('.btn-normal-primary-with-shadow').should('not.contain', 'locale.').click();
		cy.wait(5000);
		cy.get('h1').should('not.contain', 'locale.');
		cy.get('.col-md-8').find('span').should('not.contain', 'locale.');
		cy.get('#pay-btn').should('not.contain', 'locale.');
		action.deleteProfile(cy);
	})
})