import action from '../../support/emailMiningFunctions'
import actionLez from '../../support/leziterFunctions'

// Spying the console errors
import failOnConsoleError, {consoleType} from "cypress-fail-on-console-error";
failOnConsoleError();

const selectedName = Cypress.env('login_name');
const urlName = Cypress.env('app_url');

describe('Sending email, making draft', () => {
    before('If Partner Settings are disabled, then turns ON all of them', () => {
        action.openUserSettings(cy, selectedName);
        action.clickOnUserFunctionButton(cy, 'partner kezelés', 'engedélyezése');
        action.clickOnUserFunctionButton(cy, 'ajánlott partner export', 'engedélyezése');
    });

    it('adds new partner', () => {
        action.loginAndOpenPartners(cy, urlName);
        cy.url('/partners');
        action.checkAddingNewPartner(cy);
        action.removePartners(cy);
    });

    it('signs up a new user with Gmail domain', () => {
        action.signUpNewUser(cy);
    });

    it('adds multiple new partners and randomly edits their datas', () => {
        action.loginAndOpenPartners(cy, urlName);
        cy.url('/partners').then(() => {
            for (let i = 1; i <= 4; i++) {
                action.addNewPartnerWithOnlyPartnerName(cy, i+'Test');
            }
        }).then(() => {
            cy.get(':nth-child(1) > .tab-list > .table > tbody > tr').each((el, ind) => {
                cy.wait(1000).then(() => {
                    let shouldAddEmail = Math.random() < 0.5;
                    let shouldAddDomain = Math.random() < 0.5;
                    cy.get(':nth-child(1) > .tab-list > .table > tbody > tr').eq(ind).find('.rectangle-gray').eq(0).click();
                    cy.wait(3000);

                    if (shouldAddEmail) {
                        action.addEmailToPartner(cy, ind);
                    }

                    if (shouldAddDomain) {
                        action.addDomainToPartner(cy, ind);
                    }
                }).then(() => {
                    cy.get('.ico-back').click();
                });
            });
        });
    });

    it('types in search bar', () => {
        action.loginAndOpenPartners(cy, urlName);
        cy.get('#domain-tab').click();
        cy.wait(4000);
        action.typesInSearchBar(cy, '3e');
        action.typesInSearchBar(cy, 'rendeles');
    });

    it('checks paging', () => {
        action.checkPaging(cy, urlName, selectedName);
    });

    it('checks that only one page exists', () => {
        action.checksOnlyOnePageIsAvailable(cy, urlName, selectedName);
    });

    after('If Partner Settings are enabled, then turns OFF all of them', () => {
        action.openUserSettings(cy, selectedName);
        action.clickOnUserFunctionButton(cy, 'partner kezelés', 'tiltása');
        action.clickOnUserFunctionButton(cy, 'ajánlott partner export', 'tiltása');
    });

})