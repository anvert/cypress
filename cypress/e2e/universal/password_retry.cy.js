import action from '../../support/fixture'

const urlName = Cypress.env('app_url');

describe('Check locale text test', function () {

    it('Password retry restriction test (english)', function () {
        cy.viewport(1920, 1080);
        cy.server();
        cy.route('POST', '/dashboard/get-email-traffic').as('post');
        cy.route('POST', '/config/profile/update-profile-settings').as('update');
        cy.route('POST', '/config/save-domain').as('domain');

        cy.visit(urlName);
        cy.url().should('include', '/login');
        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span').click();
        cy.get('a.dropdown-item.dusk-profile').first().click();

        cy.get('a.btn-link.underline').click();
        cy.url().should('include', '/register');

        action.register(cy, 'tothl', 'Tóth László', 'demotothlaszlo@gmail.com', 't5z6u7i8');
        cy.wait(3000);
        action.onboarding(cy, 'demotothlaszlo@gmail.com');
        cy.wait(3000);

        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.wait(3000);
        cy.get('a.dropdown-item').eq(8).click();
        cy.wait(3000);

        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span').click();
        cy.get('a.dropdown-item.dusk-profile').first().click();

        action.login(cy, 'abcdefgh', 'demotothlaszlo@gmail.com')
        cy.get('.requirements').should('contain', 'Invalid account data.');

        action.login(cy, 'kjhgfdsa', 'demotothlaszlo@gmail.com');
        cy.get('.requirements').should('contain', 'Invalid account data.');

        action.login(cy, 'asdfghjk', 'demotothlaszlo@gmail.com');
        cy.get('.requirements').should('contain', 'Invalid account data.');

        action.login(cy, 'o9i8u7z6', 'demotothlaszlo@gmail.com');
        cy.get('.requirements').should('contain', 'Invalid account data.');

        action.login(cy, 'q1e3r4t5', 'demotothlaszlo@gmail.com');
        cy.get('.requirements').should('contain', 'Invalid account data.');

        action.login(cy, 'abcdefgh', 'demotothlaszlo@gmail.com');
        cy.get('.requirements').should('contain', 'Too many login attempts. Please try again in 5 minutes.');

        action.login(cy, 'Titkos123?;', 'admin@anvert.com');
        cy.get('td.col-sm-auto a').eq(0).should('contain', 'Tóth László').click();
        cy.get('a.btn.btn-primary').eq(2).click();
        cy.get('div.wrapper-full-page').should('contain', 'Successful user login attempts unlocked');
        cy.get('a.nav-item span i.fas').click();

        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span').click();
        cy.get('a.dropdown-item.dusk-profile').first().click();

        action.login(cy, 'e3r4t5z6', 'demotothlaszlo@gmail.com');
        cy.get('.requirements').should('contain', 'Invalid account data.');

        action.login(cy, 't5z6u7i8', 'demotothlaszlo@gmail.com');
        cy.wait(7000);
        action.deleteProfile(cy, 'Delete permanently');
    })

    it('Password retry restriction test (hungarian)', function () {
        cy.viewport(1920, 1080);
        cy.server();
        cy.route('POST', '/dashboard/get-email-traffic').as('post');
        cy.route('POST', '/config/profile/update-profile-settings').as('update');
        cy.route('POST', '/config/save-domain').as('domain');

        cy.visit(urlName);
        cy.url().should('include', '/login');
        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span').click();
        cy.get('a.dropdown-item.dusk-profile').next().click();

        cy.get('a.btn-link.underline').click();
        cy.url().should('include', '/register');

        action.register(cy, 'kovacsj', 'Kovács Júlia', 'demokovacsjulia@gmail.com', 't5z6u7i8');
        cy.wait(3000);
        action.onboarding(cy, 'demokovacsjulia@gmail.com');
        cy.wait(7000);

        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span.media-object.circle-mb.text-circle.email-initial').click({force: true});
        cy.wait(3000);
        cy.get('a.dropdown-item').eq(8).click();
        cy.wait(3000);

        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span').click();
        cy.get('a.dropdown-item.dusk-profile').next().click();

        action.login(cy, 'abcdefgh', 'demokovacsjulia@gmail.com')
        cy.get('.requirements').should('contain', 'Hibás bejelentkezési adatok.');

        action.login(cy, 'kjhgfdsa', 'demokovacsjulia@gmail.com');
        cy.get('.requirements').should('contain', 'Hibás bejelentkezési adatok.');

        action.login(cy, 'asdfghjk', 'demokovacsjulia@gmail.com');
        cy.get('.requirements').should('contain', 'Hibás bejelentkezési adatok.');

        action.login(cy, 'o9i8u7z6', 'demokovacsjulia@gmail.com');
        cy.get('.requirements').should('contain', 'Hibás bejelentkezési adatok.');

        action.login(cy, 'q1e3r4t5', 'demokovacsjulia@gmail.com');
        cy.get('.requirements').should('contain', 'Hibás bejelentkezési adatok.');

        action.login(cy, 'abcdefgh', 'demokovacsjulia@gmail.com');
        cy.get('.requirements').should('contain', 'Túl sok bejelentkezési kísérlet. Kérjük, próbálkozzon újra 5 perc múlva.');

        action.login(cy, 'Titkos123?;', 'admin@anvert.com');
        cy.get('td.col-sm-auto a').eq(0).should('contain', 'Kovács Júlia').click();
        cy.get('a.btn.btn-primary').eq(2).click();
        cy.get('div.wrapper-full-page').should('contain', 'Successful user login attempts unlocked');
        cy.get('a.nav-item span i.fas').click();

        cy.get('a.dropdown-toggle.dropdown-user-link.top-nav-item span').click();
        cy.get('a.dropdown-item.dusk-profile').next().click();

        action.login(cy, 'e3r4t5z6', 'demokovacsjulia@gmail.com');
        cy.get('.requirements').should('contain', 'Hibás bejelentkezési adatok.');

        action.login(cy, 't5z6u7i8', 'demokovacsjulia@gmail.com');
        cy.wait(7000);
        action.deleteProfile(cy);
    })
})