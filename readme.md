
Minden teszt esetén egy konstansban van eltárolva az, hogy melyik szervert látogassa meg a teszt futtatásakor, lehetőség van változtatni ezt az elérést a cypress.env.json fájlban, ha az "app_url"-hez tartozó értéket a kívánt szerver címére változtatjuk (pl. anvert.local, testv2.anvert.com). Ezalól kivételt jelent az éles szerverre készített teszt amit az loading.js fájl tartalmazza, ennek konstans url elérése az app.anvert.com.


## Bejelentkezés teszt:
#### login.js

Leteszteltem magyar illetve angol nyelven a bejelentkezés felületét. A teszt először üres mezővel történő bejelentkezést vizsgálja, utána  helytelen jelszóval történő bejelentkezési kísérletet. Végül helyes adatokkal történő belépés következik.


## Regisztrációs teszt:
#### register.js

Ez a tesz is magyar illetve angol nyelvre is elkészült. Hasonlóan a bejelentkezéshez a teszt először üres mezővel történő regisztrációt vizsgál, ellenőrzi, hogy a megfelelő alertek megjelennek e. Utána rosszul kitöltött űrlappal történő regisztrációs teszteset következik. A rosszul történő űrlap kitöltés alatt a feltételek elfogadásának hiánya, illetve rossz ellenőrző kód beírását értem. Ezután pedig megfelelő adatokkal történő sikeres regisztráció következik.


## Password reset teszt:
#### password_reset.js

A bejelentkezési felületről navigálunk a jelszó visszaállítása oldalra. Angol és magyar nyelven is leteszteltem az oldal működését. Először itt is üresen hagyott mezőre ellenőriztem, hogy a hibaüzenetek megjelennek e az input mező alatt. Ezután hibás felhasználónevet megadva teszteltem, hogy a megfelelő hibaüzenet megjelenik e. És végül helyes felhasználónevet (emailt) megadva sikeres jelszó visszaállítás következik.

## Profil beállítások teszt: 
#### profile_data.js | password_settings.js | profile_settings.js

Megfelelő adatokkal belépve a profil beállítások menüben először magyar utána pedig angol nyelven tesztelem le a beállításokat. A beállítások alatt 3 rész van mindegyikre külön tesztet írtam. Egyikben történik a név, cím, cégnév beállításainak megváltoztatása. Névváltoztatás esetén ellenőrzöm, hogy a főoldalon kiírt név változik e. Ugyanezt ellenőrzöm cégnév változtatása esetén is. A másik rész a nyelvi beállítások, illetve spam elkülönítése, belső levelezésben keresés beállítása, illetve órák, napok átállítása. Végül pedig a jelszó módosítás rész tesztje következik. Angol illetve magyar nyelvre is kiterjed, úgy hogy lényegében a magyar nyelv esetén módosítja, ezt ellenőrzöm is , hogy be tud -e lépni az új jelszóval, illetve utána angol nyelvnél pedig visszaállítja azt. Itt is tesztelem, hogy be tud e lépni a módosított jelszóval utána.


## Dashboard tesztek:
#### dashboard.js | dashboard_functions.js | productivity_statistic.js | replying_time_statistic.js | emotion_analysis.js | test_opanalysis.js

Belépés után angol illetve magyar nyelvre is készítettem tesztet. Egyik tesztben ellenőrzésre kerül , hogy a bejelentkezés utáni felület elemei sikeresen betöltenek -e illetve nyelvenként a megfelelő szöveg jelenik -e meg. Tehát a grafikonok címei megjelennek a megfelelő szöveggel, illetve az oldalsáv menüpontjai is a megfelelő szöveggel betöltenek és rálépve a megfelelő oldalra navigálnak. Egy tesztet készítettem a dashboard (áttekintés) szűrőire, bővített szűrőre, szűrő mentésre, illetve, azok sikeres (200) status code-al térnek vissza.
A dashboard részhez tartozik az aktivitás, illetve válaszidő ezekre is külön tesztet készítettem. Különböző szűrésekre ellenőrzöm, hogy megfelelő status code-al térnek vissza illetve Válaszidő esetén a napot megváltoztatva mint viszonyítás ellenőrzöm, hogy a táblázat fejléc cellája az új napszámot tartalmazza -e. Hangulatelemzés illetve Érzelem elemzésnél is a szűréseket ellenőriztem, illetve a hangulat elemzésnél ha változtatom a grafikonok, kimutatásoknál azt, hogy legpozitívabb, legnegatívabb, változnak -e az adatok. Érzelem elemzés esetén pedig megnézem, hogy a grafikon elemeire kattintva értékek megjelennek -e, változnak -e.


## Oldalsáv (menüpont) tesztek:
#### all_email_category.js | deleted_email_category.js | deleted_email_category.js | closed_email_category.js | internal_email_category.js | promotion_email_category.js | replied_email_category.js | not_replied_email_category.js | stucked_email_category.js

Ezek a tesztek is angol illetve magyar nyelvre is készültek. Minden menüpontra készült egy teszt ahol a megfelelő menüpontra nézve szűri az emaileket illetve hogy azok sikeres status code-al térnek e vissza. A törölt illetve lezárt levelezés tesztjei előtt az összes emailek közül törlök illetve lezárok 10-et 10-et utána lépek csak az oldalra. Mindegyik menüpontnál egy levélt megnyitva szűrőt hozzáadunk, és azt le is ellenőrzi, hogy a profil menüpontban a szűrők között a megfelelő email cím megjelenik -e. A keresősávban megadott adatokkal leszűri az emaileket és ellenőrzi azt. Erre készült külön teszt.


## Kereső teszt:
#### search.js

A menüpontokban a keresősávba beírt dátum, feladó, szöveg alapján leszűrjük az emaileket. Ezeket ellenőrzi, hogy a megfelelő dátum szerepel e az oldalon, illetve megfelelő feladó.Ezen felül az emaileket megnyitva is ellenőrzi, hogy a megfelelő dátum jelenik meg megfelelő email illetve, hogy a megadott szöveget tartalmazza e az email, legalább a tárgyban.


## Locale. Felirat kereső teszt:
#### locale_text_find_statistics.js | locale_text_find_threads.js  | locale_text_find_in_settings.js | locale_text_find_in_settings_new_user.js | locale_text_find_in_settings_wo_con.js

Először a dashboardot és a diagramokat nézi (diagram feliratok), hogy tartalmazza -e a locale. feliratot. Ezután a menüpontokban végigmegy az összesen, a felugró ablakokat is megnyitja és a locale. feliratot nézi, hogy valahol megtalálható e. Majd a profil beállításokat nézi, hogy megtalálható -e. Sorrendben: domain beállítások, email fiók beállítások, messenger fiók beállítások, szűrők és előfizetések. Itt is a felugró ablakokba is megnézi, hogy tartalmazza -e a locale. Feliratot, a gombok tartalmazzák -e, illetve maga a felület. 


## Viewport teszt:
#### smaller_view.js | smaller_view_new_regist_and_new_sub.js | smaller_view_exist_sub.js

Az oldalon található felugró ablakokat ellenőrzi, hogy minden látható -e, vagy elérhető kisebb méret esetén is. 1200x600 és 1200x700 pixeles méretre tesztel, és megvizsgálja, hogy esetlegesen a felugró menükben, ami törlésnél stb. jelenik meg, gördíthető e az ablak, hogy a gombok és a további feliratok láthatóak -e ezáltal.


## Workhour teszt:
#### workhour.js

Az oldalon egy új felhasználót beregisztrálva ellenőrzi, hogy miután a megfelelő domain-t beállította a csoportbeállításoknál megfelelően működik az alap munkaidő létrehozása, vagyis ellenőrzi, hogy az alap munkaidő ami megjelenik, az 8:00 kezdő és 17:00 befejező időpont -e. Ezután pedig új csoport hozzádásánál is ellenérzi, hogy az alap munkaidő szerepel -e vagyis 8:00 - 17:000 a munkaidő naponta.

## Kibővített szűrő teszt:
#### extended_filter.js

Külön teszt van a bővített szűrő tesztelésére, de több oldalon is ahol inegráva van ellenőrzsre kerül ennek megfelelő működése. Az Áttekints (Dashboard) oldalon teszteli a szűrőt különböző értékeke megadva és azok keresésének helyét változtatva vizsgálja, hogy megfelelő status code-al tér vissza, végül pedig a checkboxot bepipálva is ellenőrzi, hogy nem fut le hibával.


## Euro-Forint váltás fizetéskor teszt:
#### payment_euro_huf_calculate.js 

Az oldalon egy felhasználót beregisztrálva, vagy egy már meglévő felhasználó esetén ellenérzi az előfizetés értékének helyességét. Ezt úgy ellenőrzi a teszt, hogy a cégadatoknál különböző országokat megadva (európai illetve nem európai és magyarország) a bruttó ár euróban illetve forintban is megfelelő -e. Európai országokban egyező a nettóval, míg azon kívül magasabb a bruttó ár értéke.


## Éles szerver teszt:
#### loading.js  

Az éles szerverre (app.anvert.com) készített teszt a bejelentkezési felületet teszteli először, hogy helytelen adatokkal hibaüzenet megjelenik -e, illetve helyes adatokat megadva sikerül e bejelentkezni. Ezután ellenőrzi, hogy a levelek és statisztikák a legfrissebbek -e, ezt pedig úgy ellenőrzi, hogy a válaszidőre menve a mai dátum jelenik -e meg. Majd végighalad a menüpontokon és ellenőrzi, hogy azok a megfelelő status code-al térnek e vissza, illetve a megfelelő oldal töltődik -e be.  

## Loading teszt:
#### loading.js  

A készített teszt a bejelentkezési felületet teszteli először, hogy helytelen adatokkal hibaüzenet megjelenik -e, illetve helyes adatokat megadva sikerül e bejelentkezni. Ezután ellenőrzi, hogy a levelek és statisztikák a legfrissebbek -e, ezt pedig úgy ellenőrzi, hogy a válaszidőre menve a mai dátum jelenik -e meg. Majd végighalad a menüpontokon és ellenőrzi, hogy azok a megfelelő status code-al térnek e vissza, illetve a megfelelő oldal töltődik -e be.

## Admin felület teszt:
#### admin.js
A tesztben az admin felület oldalán a különböző menüpontokat keresi fel, hogy az egyes oldalak megfelelően betöltődnek -e, illetve jó url átirányítás van. 

## Csoportos email fiók feltöltés teszt:
#### test_group_email_accounts_invite.js | test_group_email_accounts_upload.js


## Fizetés teszt:
#### payment.js

# Onboarding teszt:
#### onboarding.js
