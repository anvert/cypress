const { defineConfig } = require('cypress')
const DateSeparator = require('./cypress/e2e/universal/dateSeparator.cjs')

let twoDaysAgo = new DateSeparator(2);
let oneDayAgo = new DateSeparator(1);
let today = new DateSeparator(0);

module.exports = defineConfig({
  e2e: {
    // We've imported your old cypress plugins here.
    // You may want to clean this up later by importing these.
    setupNodeEvents(on, config) {
      return require('./cypress/plugins/index.js')(on, config)
    },
    env: {
      // "app_url": "https://testv3.anvert.com",
      "app_url": "https://anvert.local",
      "local_app_url": "https://anvert.local/",
      "login_name": "Tari Jenő",
      "login_email": "jtari@csavarker.hu",
      "login_password": "q1w2e3r4",
      "date": twoDaysAgo.printDate('-') + " ~ " + today.printDate('-'),
      "date2": twoDaysAgo.getMonth() + '.' + twoDaysAgo.getDay(),
      "date3": oneDayAgo.getMonth() + '.' + oneDayAgo.getDay(),
      "date4": today.getMonth() + '.' + today.getDay(),
      "spamDate": twoDaysAgo.printDate('-') + " ~ " + today.printDate('-'),
      "spamDate2": twoDaysAgo.getMonth() + '.' + twoDaysAgo.getDay(),
      "spamDate3": oneDayAgo.getMonth() + '.' + oneDayAgo.getDay(),
      "spamDate4": today.getMonth() + '.' + today.getDay(),
      "newEmailRegister": "nagyemil@gmail.com",
      "newPswregister": "asd123456",
      "newNameRegister": "tesztemil",
    },
    watchForFileChanges: false
    // experimentalSessionAndOrigin: true
  },
  projectId: "mm4ess"
})
